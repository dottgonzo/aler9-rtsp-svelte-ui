#!/bin/bash

VERSION=$1
REGISTRY_URI=registry.gitlab.com/dottgonzo/aler9-rtsp-svelte-ui


if [ -z "$VERSION" ]
then
docker buildx build --platform linux/amd64,linux/arm64 -t $REGISTRY_URI:master --push .
echo building $REGISTRY_URI with version: \"master\"

    else
    echo building $REGISTRY_URI with version: \"master\" and \"$VERSION\"

docker buildx build --platform linux/amd64,linux/arm64 -t $REGISTRY_URI:master -t $REGISTRY_URI:$VERSION --push .

fi
