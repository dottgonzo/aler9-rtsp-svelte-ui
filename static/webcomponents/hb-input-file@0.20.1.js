var release = (function () {
	'use strict';
	function e() {}
	function t(e) {
		return e();
	}
	function i() {
		return Object.create(null);
	}
	function o(e) {
		e.forEach(t);
	}
	function a(e) {
		return 'function' == typeof e;
	}
	function n(e, t) {
		return e != e ? t == t : e !== t || (e && 'object' == typeof e) || 'function' == typeof e;
	}
	let r, l;
	function s(e, t) {
		return r || (r = document.createElement('a')), (r.href = t), e === r.href;
	}
	function c(e, t) {
		e.appendChild(t);
	}
	function d(e, t, i) {
		e.insertBefore(t, i || null);
	}
	function p(e) {
		e.parentNode && e.parentNode.removeChild(e);
	}
	function m(e) {
		return document.createElement(e);
	}
	function u(e) {
		return document.createTextNode(e);
	}
	function f() {
		return u(' ');
	}
	function g(e, t, i, o) {
		return e.addEventListener(t, i, o), () => e.removeEventListener(t, i, o);
	}
	function b(e, t, i) {
		null == i ? e.removeAttribute(t) : e.getAttribute(t) !== i && e.setAttribute(t, i);
	}
	function h(e, t) {
		(t = '' + t), e.data !== t && (e.data = t);
	}
	function x(e, t, i, o) {
		null == i ? e.style.removeProperty(t) : e.style.setProperty(t, i, o ? 'important' : '');
	}
	function v(e) {
		const t = {};
		for (const i of e) t[i.name] = i.value;
		return t;
	}
	function w(e) {
		l = e;
	}
	function y() {
		if (!l) throw new Error('Function called outside component initialization');
		return l;
	}
	function k() {
		const e = y();
		return (t, i, { cancelable: o = !1 } = {}) => {
			const a = e.$$.callbacks[t];
			if (a) {
				const n = (function (e, t, { bubbles: i = !1, cancelable: o = !1 } = {}) {
					const a = document.createEvent('CustomEvent');
					return a.initCustomEvent(e, i, o, t), a;
				})(t, i, { cancelable: o });
				return (
					a.slice().forEach((t) => {
						t.call(e, n);
					}),
					!n.defaultPrevented
				);
			}
			return !0;
		};
	}
	const $ = [],
		_ = [];
	let j = [];
	const z = [],
		E = Promise.resolve();
	let I = !1;
	function H(e) {
		j.push(e);
	}
	const q = new Set();
	let C = 0;
	function S() {
		if (0 !== C) return;
		const e = l;
		do {
			try {
				for (; C < $.length; ) {
					const e = $[C];
					C++, w(e), L(e.$$);
				}
			} catch (e) {
				throw (($.length = 0), (C = 0), e);
			}
			for (w(null), $.length = 0, C = 0; _.length; ) _.pop()();
			for (let e = 0; e < j.length; e += 1) {
				const t = j[e];
				q.has(t) || (q.add(t), t());
			}
			j.length = 0;
		} while ($.length);
		for (; z.length; ) z.pop()();
		(I = !1), q.clear(), w(e);
	}
	function L(e) {
		if (null !== e.fragment) {
			e.update(), o(e.before_update);
			const t = e.dirty;
			(e.dirty = [-1]), e.fragment && e.fragment.p(e.ctx, t), e.after_update.forEach(H);
		}
	}
	const N = new Set();
	function M(e, t) {
		const i = e.$$;
		null !== i.fragment &&
			(!(function (e) {
				const t = [],
					i = [];
				j.forEach((o) => (-1 === e.indexOf(o) ? t.push(o) : i.push(o))),
					i.forEach((e) => e()),
					(j = t);
			})(i.after_update),
			o(i.on_destroy),
			i.fragment && i.fragment.d(t),
			(i.on_destroy = i.fragment = null),
			(i.ctx = []));
	}
	function R(e, t) {
		-1 === e.$$.dirty[0] && ($.push(e), I || ((I = !0), E.then(S)), e.$$.dirty.fill(0)),
			(e.$$.dirty[(t / 31) | 0] |= 1 << t % 31);
	}
	function O(n, r, s, c, d, m, u, f = [-1]) {
		const g = l;
		w(n);
		const b = (n.$$ = {
			fragment: null,
			ctx: [],
			props: m,
			update: e,
			not_equal: d,
			bound: i(),
			on_mount: [],
			on_destroy: [],
			on_disconnect: [],
			before_update: [],
			after_update: [],
			context: new Map(r.context || (g ? g.$$.context : [])),
			callbacks: i(),
			dirty: f,
			skip_bound: !1,
			root: r.target || g.$$.root
		});
		u && u(b.root);
		let h = !1;
		if (
			((b.ctx = s
				? s(n, r.props || {}, (e, t, ...i) => {
						const o = i.length ? i[0] : t;
						return (
							b.ctx &&
								d(b.ctx[e], (b.ctx[e] = o)) &&
								(!b.skip_bound && b.bound[e] && b.bound[e](o), h && R(n, e)),
							t
						);
				  })
				: []),
			b.update(),
			(h = !0),
			o(b.before_update),
			(b.fragment = !!c && c(b.ctx)),
			r.target)
		) {
			if (r.hydrate) {
				const e = (function (e) {
					return Array.from(e.childNodes);
				})(r.target);
				b.fragment && b.fragment.l(e), e.forEach(p);
			} else b.fragment && b.fragment.c();
			r.intro && (x = n.$$.fragment) && x.i && (N.delete(x), x.i(v)),
				(function (e, i, n, r) {
					const { fragment: l, after_update: s } = e.$$;
					l && l.m(i, n),
						r ||
							H(() => {
								const i = e.$$.on_mount.map(t).filter(a);
								e.$$.on_destroy ? e.$$.on_destroy.push(...i) : o(i), (e.$$.on_mount = []);
							}),
						s.forEach(H);
				})(n, r.target, r.anchor, r.customElement),
				S();
		}
		var x, v;
		w(g);
	}
	let T;
	function A(e) {
		let t, i, o, a, n, r, l, s;
		function u(e, t) {
			return 'image/jpeg' === e[6] ||
				'image/png' === e[6] ||
				'image/gif' === e[6] ||
				'image/svg+xml' === e[6]
				? K
				: 'video/mp4' === e[6] || 'video/ogg' === e[6] || 'video/webm' === e[6]
				? G
				: 'audio/mpeg' === e[6] || 'audio/ogg' === e[6] || 'audio/wav' === e[6]
				? D
				: 'application/pdf' === e[6]
				? J
				: 'application/vnd.ms-excel' === e[6] ||
				  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' === e[6]
				? V
				: 'application/msword' === e[6] ||
				  'application/vnd.openxmlformats-officedocument.wordprocessingml.document' === e[6]
				? F
				: 'application/vnd.ms-powerpoint' === e[6] ||
				  'application/vnd.openxmlformats-officedocument.presentationml.presentation' === e[6]
				? B
				: 'application/zip' === e[6] ||
				  'application/x-7z-compressed' === e[6] ||
				  'application/x-rar-compressed' === e[6]
				? U
				: P;
		}
		let h = u(e),
			x = h(e),
			v = !e[1]?.params?.placeHolderImage?.src && Q(e);
		return {
			c() {
				(t = m('span')),
					x.c(),
					(i = f()),
					v && v.c(),
					(o = f()),
					(a = m('button')),
					(n = m('i')),
					b(n, 'class', 'bi bi-x-lg'),
					b(
						a,
						'style',
						(r = e[1]?.params?.placeHolderImage ? 'position: absolute;right: 0px;' : '')
					),
					b(a, 'class', 'btn btn-outline'),
					b(t, 'id', 'preview');
			},
			m(r, p) {
				d(r, t, p),
					x.m(t, null),
					c(t, i),
					v && v.m(t, null),
					c(t, o),
					c(t, a),
					c(a, n),
					l || ((s = g(a, 'click', e[17])), (l = !0));
			},
			p(e, n) {
				h === (h = u(e)) && x ? x.p(e, n) : (x.d(1), (x = h(e)), x && (x.c(), x.m(t, i))),
					e[1]?.params?.placeHolderImage?.src
						? v && (v.d(1), (v = null))
						: v
						? v.p(e, n)
						: ((v = Q(e)), v.c(), v.m(t, o)),
					2 & n &&
						r !== (r = e[1]?.params?.placeHolderImage ? 'position: absolute;right: 0px;' : '') &&
						b(a, 'style', r);
			},
			d(e) {
				e && p(t), x.d(), v && v.d(), (l = !1), s();
			}
		};
	}
	function P(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function U(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-zip');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function B(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-ppt');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function F(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-word');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function V(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-spreadsheet');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function J(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-pdf');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function D(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-music');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function G(t) {
		let i;
		return {
			c() {
				(i = m('i')),
					x(i, 'font-size', '1.3em'),
					x(i, 'vertical-align', 'middle'),
					b(i, 'class', 'bi bi-file-earmark-play');
			},
			m(e, t) {
				d(e, i, t);
			},
			p: e,
			d(e) {
				e && p(i);
			}
		};
	}
	function K(e) {
		let t, i;
		return {
			c() {
				(t = m('img')),
					x(t, 'max-height', e[1]?.params?.placeHolderImage?.height || '38'),
					x(t, 'max-width', (e[1]?.params?.placeHolderImage?.width || '60') + 'px'),
					s(t.src, (i = e[8])) || b(t, 'src', i);
			},
			m(e, i) {
				d(e, t, i);
			},
			p(e, o) {
				2 & o && x(t, 'max-height', e[1]?.params?.placeHolderImage?.height || '38'),
					2 & o && x(t, 'max-width', (e[1]?.params?.placeHolderImage?.width || '60') + 'px'),
					256 & o && !s(t.src, (i = e[8])) && b(t, 'src', i);
			},
			d(e) {
				e && p(t);
			}
		};
	}
	function Q(e) {
		let t,
			i,
			o = e[5].name + '';
		return {
			c() {
				(t = m('span')), (i = u(o)), b(t, 'id', 'file-name');
			},
			m(e, o) {
				d(e, t, o), c(t, i);
			},
			p(e, t) {
				32 & t && o !== (o = e[5].name + '') && h(i, o);
			},
			d(e) {
				e && p(t);
			}
		};
	}
	function W(e) {
		let t,
			i,
			o,
			a,
			n,
			r,
			l,
			s,
			c,
			u = e[1]?.params?.placeHolderImage?.src && X(e);
		return {
			c() {
				u && u.c(),
					(t = f()),
					(i = m('input')),
					b(
						i,
						'style',
						(o =
							'max-width: 270px;' +
							(e[1]?.params?.placeHolderImage?.src
								? 'opacity: 0;position: absolute;z-index: -1;'
								: ''))
					),
					b(i, 'type', 'file'),
					b(i, 'id', (a = e[1]?.id)),
					(i.required = n = e[1]?.required),
					b(i, 'placeholder', (r = e[1]?.placeholder)),
					(i.readOnly = l = e[1]?.readonly),
					b(i, 'accept', e[7]);
			},
			m(o, a) {
				u && u.m(o, a), d(o, t, a), d(o, i, a), s || ((c = g(i, 'change', e[18])), (s = !0));
			},
			p(e, s) {
				e[1]?.params?.placeHolderImage?.src
					? u
						? u.p(e, s)
						: ((u = X(e)), u.c(), u.m(t.parentNode, t))
					: u && (u.d(1), (u = null)),
					2 & s &&
						o !==
							(o =
								'max-width: 270px;' +
								(e[1]?.params?.placeHolderImage?.src
									? 'opacity: 0;position: absolute;z-index: -1;'
									: '')) &&
						b(i, 'style', o),
					2 & s && a !== (a = e[1]?.id) && b(i, 'id', a),
					2 & s && n !== (n = e[1]?.required) && (i.required = n),
					2 & s && r !== (r = e[1]?.placeholder) && b(i, 'placeholder', r),
					2 & s && l !== (l = e[1]?.readonly) && (i.readOnly = l),
					128 & s && b(i, 'accept', e[7]);
			},
			d(e) {
				u && u.d(e), e && p(t), e && p(i), (s = !1), c();
			}
		};
	}
	function X(e) {
		let t, i, o, a, n, r;
		return {
			c() {
				(t = m('span')),
					(i = m('img')),
					(a = u('\n\t\t\t\tSelect image')),
					x(i, 'max-height', (e[1].params.placeHolderImage.height || '38') + 'px'),
					x(i, 'max-width', (e[1].params.placeHolderImage.width || '60') + 'px'),
					s(i.src, (o = e[1]?.params?.placeHolderImage?.src)) || b(i, 'src', o),
					x(t, 'cursor', 'pointer');
			},
			m(o, l) {
				d(o, t, l), c(t, i), c(t, a), n || ((r = g(t, 'click', e[9])), (n = !0));
			},
			p(e, t) {
				2 & t && x(i, 'max-height', (e[1].params.placeHolderImage.height || '38') + 'px'),
					2 & t && x(i, 'max-width', (e[1].params.placeHolderImage.width || '60') + 'px'),
					2 & t && !s(i.src, (o = e[1]?.params?.placeHolderImage?.src)) && b(i, 'src', o);
			},
			d(e) {
				e && p(t), (n = !1), r();
			}
		};
	}
	function Y(e) {
		let t,
			i,
			o = e[1].validationTip + '';
		return {
			c() {
				(t = m('div')),
					(i = u(o)),
					b(t, 'part', 'invalid-feedback'),
					b(t, 'class', 'invalid-feedback mb-1');
			},
			m(e, o) {
				d(e, t, o), c(t, i);
			},
			p(e, t) {
				2 & t && o !== (o = e[1].validationTip + '') && h(i, o);
			},
			d(e) {
				e && p(t);
			}
		};
	}
	function Z(t) {
		let i,
			o,
			a,
			n,
			r,
			l,
			s,
			g = t[5]?.name && A(t),
			h = !t[5]?.name && W(t),
			x = t[1]?.validationTip && 'yes' === t[0] && Y(t);
		return {
			c() {
				(i = m('link')),
					(o = f()),
					(a = m('span')),
					g && g.c(),
					(n = f()),
					h && h.c(),
					(l = f()),
					x && x.c(),
					(s = u('')),
					(this.c = e),
					b(i, 'rel', 'stylesheet'),
					b(i, 'href', '/npm/bootstrap-icons.css'),
					b(
						a,
						'class',
						(r =
							'form-control ' +
							('yes' === t[0] && t[1]?.required ? (t[4] ? 'is-valid' : 'is-invalid') : ''))
					);
			},
			m(e, t) {
				c(document.head, i),
					d(e, o, t),
					d(e, a, t),
					g && g.m(a, null),
					c(a, n),
					h && h.m(a, null),
					d(e, l, t),
					x && x.m(e, t),
					d(e, s, t);
			},
			p(e, [t]) {
				e[5]?.name ? (g ? g.p(e, t) : ((g = A(e)), g.c(), g.m(a, n))) : g && (g.d(1), (g = null)),
					e[5]?.name
						? h && (h.d(1), (h = null))
						: h
						? h.p(e, t)
						: ((h = W(e)), h.c(), h.m(a, null)),
					19 & t &&
						r !==
							(r =
								'form-control ' +
								('yes' === e[0] && e[1]?.required ? (e[4] ? 'is-valid' : 'is-invalid') : '')) &&
						b(a, 'class', r),
					e[1]?.validationTip && 'yes' === e[0]
						? x
							? x.p(e, t)
							: ((x = Y(e)), x.c(), x.m(s.parentNode, s))
						: x && (x.d(1), (x = null));
			},
			i: e,
			o: e,
			d(e) {
				p(i), e && p(o), e && p(a), g && g.d(), h && h.d(), e && p(l), x && x.d(e), e && p(s);
			}
		};
	}
	function ee(e, t, i) {
		var o, a, n, r, l;
		let { set_value: s } = t,
			{ set_valid: c } = t,
			{ show_validation: d } = t,
			{ schemaentry: p } = t;
		const m = {
			abs: 'audio/x-mpeg',
			ai: 'application/postscript',
			aif: 'audio/x-aiff',
			aifc: 'audio/x-aiff',
			aiff: 'audio/x-aiff',
			aim: 'application/x-aim',
			art: 'image/x-jg',
			asf: 'video/x-ms-asf',
			asx: 'video/x-ms-asf',
			au: 'audio/basic',
			avi: 'video/x-msvideo',
			avx: 'video/x-rad-screenplay',
			bcpio: 'application/x-bcpio',
			bin: 'application/octet-stream',
			bmp: 'image/bmp',
			body: 'text/html',
			cdf: 'application/x-cdf',
			cer: 'application/pkix-cert',
			class: 'application/java',
			cpio: 'application/x-cpio',
			csh: 'application/x-csh',
			css: 'text/css',
			dib: 'image/bmp',
			doc: 'application/msword',
			dtd: 'application/xml-dtd',
			dv: 'video/x-dv',
			dvi: 'application/x-dvi',
			eot: 'application/vnd.ms-fontobject',
			eps: 'application/postscript',
			etx: 'text/x-setext',
			exe: 'application/octet-stream',
			gif: 'image/gif',
			gtar: 'application/x-gtar',
			gz: 'application/x-gzip',
			hdf: 'application/x-hdf',
			hqx: 'application/mac-binhex40',
			htc: 'text/x-component',
			htm: 'text/html',
			html: 'text/html',
			ief: 'image/ief',
			jad: 'text/vnd.sun.j2me.app-descriptor',
			jar: 'application/java-archive',
			java: 'text/x-java-source',
			jnlp: 'application/x-java-jnlp-file',
			jpe: 'image/jpeg',
			jpeg: 'image/jpeg',
			jpg: 'image/jpeg',
			js: 'application/javascript',
			jsf: 'text/plain',
			json: 'application/json',
			jspf: 'text/plain',
			kar: 'audio/midi',
			latex: 'application/x-latex',
			m3u: 'audio/x-mpegurl',
			mac: 'image/x-macpaint',
			man: 'text/troff',
			mathml: 'application/mathml+xml',
			me: 'text/troff',
			mid: 'audio/midi',
			midi: 'audio/midi',
			mif: 'application/x-mif',
			mov: 'video/quicktime',
			movie: 'video/x-sgi-movie',
			mp1: 'audio/mpeg',
			mp2: 'audio/mpeg',
			mp3: 'audio/mpeg',
			mp4: 'video/mp4',
			mpa: 'audio/mpeg',
			mpe: 'video/mpeg',
			mpeg: 'video/mpeg',
			mpega: 'audio/x-mpeg',
			mpg: 'video/mpeg',
			mpv2: 'video/mpeg2',
			ms: 'application/x-wais-source',
			nc: 'application/x-netcdf',
			oda: 'application/oda',
			odb: 'application/vnd.oasis.opendocument.database',
			odc: 'application/vnd.oasis.opendocument.chart',
			odf: 'application/vnd.oasis.opendocument.formula',
			odg: 'application/vnd.oasis.opendocument.graphics',
			odi: 'application/vnd.oasis.opendocument.image',
			odm: 'application/vnd.oasis.opendocument.text-master',
			odp: 'application/vnd.oasis.opendocument.presentation',
			ods: 'application/vnd.oasis.opendocument.spreadsheet',
			odt: 'application/vnd.oasis.opendocument.text',
			otg: 'application/vnd.oasis.opendocument.graphics-template',
			oth: 'application/vnd.oasis.opendocument.text-web',
			otp: 'application/vnd.oasis.opendocument.presentation-template',
			ots: 'application/vnd.oasis.opendocument.spreadsheet-template',
			ott: 'application/vnd.oasis.opendocument.text-template',
			ogx: 'application/ogg',
			ogv: 'video/ogg',
			oga: 'audio/ogg',
			ogg: 'audio/ogg',
			otf: 'application/x-font-opentype',
			spx: 'audio/ogg',
			flac: 'audio/flac',
			anx: 'application/annodex',
			axa: 'audio/annodex',
			axv: 'video/annodex',
			xspf: 'application/xspf+xml',
			pbm: 'image/x-portable-bitmap',
			pct: 'image/pict',
			pdf: 'application/pdf',
			pgm: 'image/x-portable-graymap',
			pic: 'image/pict',
			pict: 'image/pict',
			pls: 'audio/x-scpls',
			png: 'image/png',
			pnm: 'image/x-portable-anymap',
			pnt: 'image/x-macpaint',
			ppm: 'image/x-portable-pixmap',
			ppt: 'application/vnd.ms-powerpoint',
			pps: 'application/vnd.ms-powerpoint',
			ps: 'application/postscript',
			psd: 'image/vnd.adobe.photoshop',
			qt: 'video/quicktime',
			qti: 'image/x-quicktime',
			qtif: 'image/x-quicktime',
			ras: 'image/x-cmu-raster',
			rdf: 'application/rdf+xml',
			rgb: 'image/x-rgb',
			rm: 'application/vnd.rn-realmedia',
			roff: 'text/troff',
			rtf: 'application/rtf',
			rtx: 'text/richtext',
			sfnt: 'application/font-sfnt',
			sh: 'application/x-sh',
			shar: 'application/x-shar',
			sit: 'application/x-stuffit',
			snd: 'audio/basic',
			src: 'application/x-wais-source',
			sv4cpio: 'application/x-sv4cpio',
			sv4crc: 'application/x-sv4crc',
			svg: 'image/svg+xml',
			svgz: 'image/svg+xml',
			swf: 'application/x-shockwave-flash',
			t: 'text/troff',
			tar: 'application/x-tar',
			tcl: 'application/x-tcl',
			tex: 'application/x-tex',
			texi: 'application/x-texinfo',
			texinfo: 'application/x-texinfo',
			tif: 'image/tiff',
			tiff: 'image/tiff',
			tr: 'text/troff',
			tsv: 'text/tab-separated-values',
			ttf: 'application/x-font-ttf',
			txt: 'text/plain',
			ulw: 'audio/basic',
			ustar: 'application/x-ustar',
			vxml: 'application/voicexml+xml',
			xbm: 'image/x-xbitmap',
			xht: 'application/xhtml+xml',
			xhtml: 'application/xhtml+xml',
			xls: 'application/vnd.ms-excel',
			xml: 'application/xml',
			xpm: 'image/x-xpixmap',
			xsl: 'application/xml',
			xslt: 'application/xslt+xml',
			xul: 'application/vnd.mozilla.xul+xml',
			xwd: 'image/x-xwindowdump',
			vsd: 'application/vnd.visio',
			wav: 'audio/x-wav',
			wbmp: 'image/vnd.wap.wbmp',
			wml: 'text/vnd.wap.wml',
			wmlc: 'application/vnd.wap.wmlc',
			wmls: 'text/vnd.wap.wmlsc',
			wmlscriptc: 'application/vnd.wap.wmlscriptc',
			wmv: 'video/x-ms-wmv',
			woff: 'application/font-woff',
			woff2: 'application/font-woff2',
			wrl: 'model/vrml',
			wspolicy: 'application/wspolicy+xml',
			z: 'application/x-compress',
			zip: 'application/zip'
		};
		let u,
			f,
			g,
			b,
			h,
			x = '',
			v = !1;
		const w = y(),
			$ = k();
		function _(e, t) {
			$(e, t), w.dispatchEvent && w.dispatchEvent(new CustomEvent(e, { detail: t }));
		}
		return (
			(e.$$set = (e) => {
				'set_value' in e && i(10, (s = e.set_value)),
					'set_valid' in e && i(11, (c = e.set_valid)),
					'show_validation' in e && i(0, (d = e.show_validation)),
					'schemaentry' in e && i(1, (p = e.schemaentry));
			}),
			(e.$$.update = () => {
				130111 & e.$$.dirty &&
					(d || i(0, (d = 'no')),
					p && 'string' == typeof p ? i(1, (p = JSON.parse(p))) : p || i(1, (p = null)),
					i(10, (s = !(!s && 'no' === s))),
					i(11, (c = !(!c && 'no' === c))),
					i(
						7,
						(g =
							(null === i(12, (o = null == p ? void 0 : p.params)) || void 0 === o
								? void 0
								: o.accept) || '*')
					),
					i(3, (f = null != f ? f : null == p ? void 0 : p.value)),
					(null == u ? void 0 : u[0]) &&
						'string' ==
							typeof (null === i(13, (a = null == u ? void 0 : u[0])) || void 0 === a
								? void 0
								: a.name) &&
						(i(5, (b = null == u ? void 0 : u[0])),
						i(
							6,
							(x =
								m[
									null ===
										i(
											16,
											(l =
												null ===
													i(
														15,
														(r =
															null === i(14, (n = null == b ? void 0 : b.name)) || void 0 === n
																? void 0
																: n.split('.'))
													) || void 0 === r
													? void 0
													: r.pop())
										) || void 0 === l
										? void 0
										: l.toLowerCase()
								] || 'unknown file type')
						),
						i(8, (h = URL.createObjectURL(b)))),
					i(
						4,
						(v =
							!!p &&
							(!(null == p ? void 0 : p.required) || (null != f && (null == b ? void 0 : b.name))))
					),
					console.log(v, f, 'validinput'),
					setTimeout(() => {
						s && _('setValue', { value: b || {}, id: null == p ? void 0 : p.id }),
							c && _('setValid', { valid: v, id: null == p ? void 0 : p.id });
					}, 0));
			}),
			[
				d,
				p,
				u,
				f,
				v,
				b,
				x,
				g,
				h,
				function () {
					const e = w.shadowRoot.querySelector('input[type="file"]');
					e && e.click(), console.log('changePlaceholderImage', e);
				},
				s,
				c,
				o,
				a,
				n,
				r,
				l,
				() => {
					i(5, (b = null)), i(4, (v = !1)), i(3, (f = null)), i(8, (h = null)), i(2, (u = []));
				},
				function () {
					(f = this.value),
						(u = this.files),
						i(3, f),
						i(0, d),
						i(1, p),
						i(10, s),
						i(11, c),
						i(12, o),
						i(2, u),
						i(13, a),
						i(5, b),
						i(14, n),
						i(15, r),
						i(16, l),
						i(4, v),
						i(2, u);
				}
			]
		);
	}
	'function' == typeof HTMLElement &&
		(T = class extends HTMLElement {
			constructor() {
				super(), this.attachShadow({ mode: 'open' });
			}
			connectedCallback() {
				const { on_mount: e } = this.$$;
				this.$$.on_disconnect = e.map(t).filter(a);
				for (const e in this.$$.slotted) this.appendChild(this.$$.slotted[e]);
			}
			attributeChangedCallback(e, t, i) {
				this[e] = i;
			}
			disconnectedCallback() {
				o(this.$$.on_disconnect);
			}
			$destroy() {
				M(this, 1), (this.$destroy = e);
			}
			$on(t, i) {
				if (!a(i)) return e;
				const o = this.$$.callbacks[t] || (this.$$.callbacks[t] = []);
				return (
					o.push(i),
					() => {
						const e = o.indexOf(i);
						-1 !== e && o.splice(e, 1);
					}
				);
			}
			$set(e) {
				var t;
				this.$$set &&
					((t = e), 0 !== Object.keys(t).length) &&
					((this.$$.skip_bound = !0), this.$$set(e), (this.$$.skip_bound = !1));
			}
		});
	class te extends T {
		constructor(e) {
			super();
			const t = document.createElement('style');
			(t.textContent =
				"@import url(\"https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap\");@import url(\"/npm/bootstrap-icons.css\");:host{--bs-blue:var(--bs-primary,#07689f);--bs-indigo:#6610f2;--bs-purple:#6f42c1;--bs-pink:#d63384;--bs-red:var(--bs-danger,#f67280);--bs-orange:#fd7e14;--bs-yellow:#ffc107;--bs-green:var(--bs-warning,#ffc107);--bs-teal:#20c997;--bs-cyan:var(--bs-info,#a2d5f2);--bs-white:#fff;--bs-gray:var(--bs-secondary,#c9d6df);--bs-gray-dark:#343a40;--bs-gray-100:#f8f9fa;--bs-gray-200:#e9ecef;--bs-gray-300:#dee2e6;--bs-gray-400:#ced4da;--bs-gray-500:#adb5bd;--bs-gray-600:var(--bs-secondary,#c9d6df);--bs-gray-700:#495057;--bs-gray-800:#343a40;--bs-gray-900:#212529;--bs-primary:var(--bs-primary,#07689f);--bs-secondary:var(--bs-secondary,#c9d6df);--bs-success:var(--bs-warning,#ffc107);--bs-info:var(--bs-info,#a2d5f2);--bs-warning:#ffc107;--bs-danger:var(--bs-danger,#f67280);--bs-light:#f8f9fa;--bs-dark:#212529;--bs-primary-rgb:13, 110, 253;--bs-secondary-rgb:108, 117, 125;--bs-success-rgb:25, 135, 84;--bs-info-rgb:13, 202, 240;--bs-warning-rgb:255, 193, 7;--bs-danger-rgb:220, 53, 69;--bs-light-rgb:248, 249, 250;--bs-dark-rgb:33, 37, 41;--bs-white-rgb:255, 255, 255;--bs-black-rgb:0, 0, 0;--bs-body-color-rgb:33, 37, 41;--bs-body-bg-rgb:255, 255, 255;--bs-font-sans-serif:system-ui, -apple-system, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";--bs-font-monospace:SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;--bs-gradient:linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));--bs-body-font-family:var(--bs-font-sans-serif);--bs-body-font-size:1rem;--bs-body-font-weight:400;--bs-body-line-height:1.5;--bs-body-color:#212529;--bs-body-bg:#fff}*,*::before,*::after{box-sizing:border-box}@media(prefers-reduced-motion: no-preference){:host{scroll-behavior:smooth}}@media(min-width: 1200px){}@media(min-width: 1200px){}@media(min-width: 1200px){}@media(min-width: 1200px){}img{vertical-align:middle}button{border-radius:0}button:focus:not(:focus-visible){outline:0}input,button{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button{text-transform:none}button{-webkit-appearance:button}button:not(:disabled){cursor:pointer}::-moz-focus-inner{padding:0;border-style:none}@media(min-width: 1200px){}::-webkit-datetime-edit-fields-wrapper,::-webkit-datetime-edit-text,::-webkit-datetime-edit-minute,::-webkit-datetime-edit-hour-field,::-webkit-datetime-edit-day-field,::-webkit-datetime-edit-month-field,::-webkit-datetime-edit-year-field{padding:0}::-webkit-inner-spin-button{height:auto}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-color-swatch-wrapper{padding:0}::file-selector-button{font:inherit}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}.form-control{display:block;width:100%;padding:0.375rem 0.75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;appearance:none;border-radius:0.25rem;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control{transition:none}}.form-control:focus{color:#212529;background-color:#fff;border-color:#86b7fe;outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-control::-webkit-date-and-time-value{height:1.5em}.form-control::placeholder{color:var(--bs-secondary,#c9d6df);opacity:1}.form-control:disabled{background-color:#e9ecef;opacity:1}.form-control::file-selector-button{padding:0.375rem 0.75rem;margin:-0.375rem -0.75rem;margin-inline-end:0.75rem;color:#212529;background-color:#e9ecef;pointer-events:none;border-color:inherit;border-style:solid;border-width:0;border-inline-end-width:1px;border-radius:0;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control::file-selector-button{transition:none}}.form-control:hover:not(:disabled):not([readonly])::file-selector-button{background-color:#dde0e3}.form-control::-webkit-file-upload-button{padding:0.375rem 0.75rem;margin:-0.375rem -0.75rem;margin-inline-end:0.75rem;color:#212529;background-color:#e9ecef;pointer-events:none;border-color:inherit;border-style:solid;border-width:0;border-inline-end-width:1px;border-radius:0;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control::-webkit-file-upload-button{transition:none}}.form-control:hover:not(:disabled):not([readonly])::-webkit-file-upload-button{background-color:#dde0e3}@media(prefers-reduced-motion: reduce){}@media(prefers-reduced-motion: reduce){}@media(prefers-reduced-motion: reduce){}@media(prefers-reduced-motion: reduce){}@media(prefers-reduced-motion: reduce){}.form-control.is-valid{border-color:var(--bs-warning,#ffc107);padding-right:calc(1.5em + 0.75rem);background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23198754' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e\");background-repeat:no-repeat;background-position:right calc(0.375em + 0.1875rem) center;background-size:calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.form-control.is-valid:focus{border-color:var(--bs-warning,#ffc107);box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.25)}.invalid-feedback{display:none;width:100%;margin-top:0.25rem;font-size:0.875em;color:var(--bs-danger,#f67280)}.is-invalid~.invalid-feedback{display:block}.form-control.is-invalid{border-color:var(--bs-danger,#f67280);padding-right:calc(1.5em + 0.75rem);background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e\");background-repeat:no-repeat;background-position:right calc(0.375em + 0.1875rem) center;background-size:calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.form-control.is-invalid:focus{border-color:var(--bs-danger,#f67280);box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.25)}.btn{display:inline-block;font-weight:400;line-height:1.5;color:#212529;text-align:center;text-decoration:none;vertical-align:middle;cursor:pointer;user-select:none;background-color:transparent;border:1px solid transparent;padding:0.375rem 0.75rem;font-size:1rem;border-radius:0.25rem;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.btn{transition:none}}.btn:hover{color:#212529}.btn:focus{outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.btn:disabled{pointer-events:none;opacity:0.65}@keyframes placeholder-glow{50%{opacity:0.2}}@keyframes placeholder-wave{100%{mask-position:-200% 0%}}@media(min-width: 576px){}@media(min-width: 768px){}@media(min-width: 992px){}@media(min-width: 1200px){}@media(min-width: 1400px){}:host{font-family:\"Roboto\", sans-serif}#preview{position:relative}#file-name{max-width:140px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:inline-block;vertical-align:middle;margin-left:10px}"),
				this.shadowRoot.appendChild(t),
				O(
					this,
					{ target: this.shadowRoot, props: v(this.attributes), customElement: !0 },
					ee,
					Z,
					n,
					{ set_value: 10, set_valid: 11, show_validation: 0, schemaentry: 1 },
					null
				),
				e && (e.target && d(e.target, this, e.anchor), e.props && (this.$set(e.props), S()));
		}
		static get observedAttributes() {
			return ['set_value', 'set_valid', 'show_validation', 'schemaentry'];
		}
		get set_value() {
			return this.$$.ctx[10];
		}
		set set_value(e) {
			this.$$set({ set_value: e }), S();
		}
		get set_valid() {
			return this.$$.ctx[11];
		}
		set set_valid(e) {
			this.$$set({ set_valid: e }), S();
		}
		get show_validation() {
			return this.$$.ctx[0];
		}
		set show_validation(e) {
			this.$$set({ show_validation: e }), S();
		}
		get schemaentry() {
			return this.$$.ctx[1];
		}
		set schemaentry(e) {
			this.$$set({ schemaentry: e }), S();
		}
	}
	return customElements.define('hb-input-file', te), te;
})(); //# sourceMappingURL=release.js.map
