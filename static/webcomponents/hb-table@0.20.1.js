var release = (function () {
	'use strict';
	function t() {}
	function r(t) {
		return t();
	}
	function e() {
		return Object.create(null);
	}
	function o(t) {
		t.forEach(r);
	}
	function n(t) {
		return 'function' == typeof t;
	}
	function i(t, r) {
		return t != t ? r == r : t !== r || (t && 'object' == typeof t) || 'function' == typeof t;
	}
	function a(t, r) {
		t.appendChild(r);
	}
	function l(t, r, e) {
		t.insertBefore(r, e || null);
	}
	function m(t) {
		t.parentNode && t.parentNode.removeChild(t);
	}
	function d(t) {
		return document.createElement(t);
	}
	function c(t) {
		return document.createTextNode(t);
	}
	function s() {
		return c(' ');
	}
	function p() {
		return c('');
	}
	function g(t, r, e, o) {
		return t.addEventListener(r, e, o), () => t.removeEventListener(r, e, o);
	}
	function f(t, r, e) {
		null == e ? t.removeAttribute(r) : t.getAttribute(r) !== e && t.setAttribute(r, e);
	}
	function b(t, r, e) {
		r in t ? (t[r] = ('boolean' == typeof t[r] && '' === e) || e) : f(t, r, e);
	}
	function u(t, r) {
		(r = '' + r), t.data !== r && (t.data = r);
	}
	function x(t, r, e, o) {
		null == e ? t.style.removeProperty(r) : t.style.setProperty(r, e, o ? 'important' : '');
	}
	function h(t) {
		const r = {};
		for (const e of t) r[e.name] = e.value;
		return r;
	}
	let w;
	function y(t) {
		w = t;
	}
	function v() {
		if (!w) throw new Error('Function called outside component initialization');
		return w;
	}
	function k() {
		const t = v();
		return (r, e, { cancelable: o = !1 } = {}) => {
			const n = t.$$.callbacks[r];
			if (n) {
				const i = (function (t, r, { bubbles: e = !1, cancelable: o = !1 } = {}) {
					const n = document.createEvent('CustomEvent');
					return n.initCustomEvent(t, e, o, r), n;
				})(r, e, { cancelable: o });
				return (
					n.slice().forEach((r) => {
						r.call(t, i);
					}),
					!i.defaultPrevented
				);
			}
			return !0;
		};
	}
	const $ = [],
		_ = [];
	let M = [];
	const j = [],
		z = Promise.resolve();
	let C = !1;
	function S(t) {
		M.push(t);
	}
	const D = new Set();
	let T = 0;
	function O() {
		if (0 !== T) return;
		const t = w;
		do {
			try {
				for (; T < $.length; ) {
					const t = $[T];
					T++, y(t), L(t.$$);
				}
			} catch (t) {
				throw (($.length = 0), (T = 0), t);
			}
			for (y(null), $.length = 0, T = 0; _.length; ) _.pop()();
			for (let t = 0; t < M.length; t += 1) {
				const r = M[t];
				D.has(r) || (D.add(r), r());
			}
			M.length = 0;
		} while ($.length);
		for (; j.length; ) j.pop()();
		(C = !1), D.clear(), y(t);
	}
	function L(t) {
		if (null !== t.fragment) {
			t.update(), o(t.before_update);
			const r = t.dirty;
			(t.dirty = [-1]), t.fragment && t.fragment.p(t.ctx, r), t.after_update.forEach(S);
		}
	}
	const E = new Set();
	function N(t, r) {
		t && t.i && (E.delete(t), t.i(r));
	}
	function Y(t, r) {
		t.d(1), r.delete(t.key);
	}
	function I(t, r, e, n, i, a, l, m, d, c, s, p) {
		let g = t.length,
			f = a.length,
			b = g;
		const u = {};
		for (; b--; ) u[t[b].key] = b;
		const x = [],
			h = new Map(),
			w = new Map(),
			y = [];
		for (b = f; b--; ) {
			const t = p(i, a, b),
				o = e(t);
			let m = l.get(o);
			m ? n && y.push(() => m.p(t, r)) : ((m = c(o, t)), m.c()),
				h.set(o, (x[b] = m)),
				o in u && w.set(o, Math.abs(b - u[o]));
		}
		const v = new Set(),
			k = new Set();
		function $(t) {
			N(t, 1), t.m(m, s), l.set(t.key, t), (s = t.first), f--;
		}
		for (; g && f; ) {
			const r = x[f - 1],
				e = t[g - 1],
				o = r.key,
				n = e.key;
			r === e
				? ((s = r.first), g--, f--)
				: h.has(n)
				? !l.has(o) || v.has(o)
					? $(r)
					: k.has(n)
					? g--
					: w.get(o) > w.get(n)
					? (k.add(o), $(r))
					: (v.add(n), g--)
				: (d(e, l), g--);
		}
		for (; g--; ) {
			const r = t[g];
			h.has(r.key) || d(r, l);
		}
		for (; f; ) $(x[f - 1]);
		return o(y), x;
	}
	function A(t, r) {
		const e = t.$$;
		null !== e.fragment &&
			(!(function (t) {
				const r = [],
					e = [];
				M.forEach((o) => (-1 === t.indexOf(o) ? r.push(o) : e.push(o))),
					e.forEach((t) => t()),
					(M = r);
			})(e.after_update),
			o(e.on_destroy),
			e.fragment && e.fragment.d(r),
			(e.on_destroy = e.fragment = null),
			(e.ctx = []));
	}
	function H(t, r) {
		-1 === t.$$.dirty[0] && ($.push(t), C || ((C = !0), z.then(O)), t.$$.dirty.fill(0)),
			(t.$$.dirty[(r / 31) | 0] |= 1 << r % 31);
	}
	function B(i, a, l, d, c, s, p, g = [-1]) {
		const f = w;
		y(i);
		const b = (i.$$ = {
			fragment: null,
			ctx: [],
			props: s,
			update: t,
			not_equal: c,
			bound: e(),
			on_mount: [],
			on_destroy: [],
			on_disconnect: [],
			before_update: [],
			after_update: [],
			context: new Map(a.context || (f ? f.$$.context : [])),
			callbacks: e(),
			dirty: g,
			skip_bound: !1,
			root: a.target || f.$$.root
		});
		p && p(b.root);
		let u = !1;
		if (
			((b.ctx = l
				? l(i, a.props || {}, (t, r, ...e) => {
						const o = e.length ? e[0] : r;
						return (
							b.ctx &&
								c(b.ctx[t], (b.ctx[t] = o)) &&
								(!b.skip_bound && b.bound[t] && b.bound[t](o), u && H(i, t)),
							r
						);
				  })
				: []),
			b.update(),
			(u = !0),
			o(b.before_update),
			(b.fragment = !!d && d(b.ctx)),
			a.target)
		) {
			if (a.hydrate) {
				const t = (function (t) {
					return Array.from(t.childNodes);
				})(a.target);
				b.fragment && b.fragment.l(t), t.forEach(m);
			} else b.fragment && b.fragment.c();
			a.intro && N(i.$$.fragment),
				(function (t, e, i, a) {
					const { fragment: l, after_update: m } = t.$$;
					l && l.m(e, i),
						a ||
							S(() => {
								const e = t.$$.on_mount.map(r).filter(n);
								t.$$.on_destroy ? t.$$.on_destroy.push(...e) : o(e), (t.$$.on_mount = []);
							}),
						m.forEach(S);
				})(i, a.target, a.anchor, a.customElement),
				O();
		}
		y(f);
	}
	let V;
	'function' == typeof HTMLElement &&
		(V = class extends HTMLElement {
			constructor() {
				super(), this.attachShadow({ mode: 'open' });
			}
			connectedCallback() {
				const { on_mount: t } = this.$$;
				this.$$.on_disconnect = t.map(r).filter(n);
				for (const t in this.$$.slotted) this.appendChild(this.$$.slotted[t]);
			}
			attributeChangedCallback(t, r, e) {
				this[t] = e;
			}
			disconnectedCallback() {
				o(this.$$.on_disconnect);
			}
			$destroy() {
				A(this, 1), (this.$destroy = t);
			}
			$on(r, e) {
				if (!n(e)) return t;
				const o = this.$$.callbacks[r] || (this.$$.callbacks[r] = []);
				return (
					o.push(e),
					() => {
						const t = o.indexOf(e);
						-1 !== t && o.splice(t, 1);
					}
				);
			}
			$set(t) {
				var r;
				this.$$set &&
					((r = t), 0 !== Object.keys(r).length) &&
					((this.$$.skip_bound = !0), this.$$set(t), (this.$$.skip_bound = !1));
			}
		});
	'undefined' != typeof globalThis
		? globalThis
		: 'undefined' != typeof window
		? window
		: 'undefined' != typeof global
		? global
		: 'undefined' != typeof self && self;
	function F(t) {
		var r = { exports: {} };
		return t(r, r.exports), r.exports;
	}
	var U = F(function (t, r) {
		t.exports = (function () {
			var t = 1e3,
				r = 6e4,
				e = 36e5,
				o = 'millisecond',
				n = 'second',
				i = 'minute',
				a = 'hour',
				l = 'day',
				m = 'week',
				d = 'month',
				c = 'quarter',
				s = 'year',
				p = 'date',
				g = 'Invalid Date',
				f =
					/^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,
				b = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,
				u = {
					name: 'en',
					weekdays: 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
					months:
						'January_February_March_April_May_June_July_August_September_October_November_December'.split(
							'_'
						),
					ordinal: function (t) {
						var r = ['th', 'st', 'nd', 'rd'],
							e = t % 100;
						return '[' + t + (r[(e - 20) % 10] || r[e] || r[0]) + ']';
					}
				},
				x = function (t, r, e) {
					var o = String(t);
					return !o || o.length >= r ? t : '' + Array(r + 1 - o.length).join(e) + t;
				},
				h = {
					s: x,
					z: function (t) {
						var r = -t.utcOffset(),
							e = Math.abs(r),
							o = Math.floor(e / 60),
							n = e % 60;
						return (r <= 0 ? '+' : '-') + x(o, 2, '0') + ':' + x(n, 2, '0');
					},
					m: function t(r, e) {
						if (r.date() < e.date()) return -t(e, r);
						var o = 12 * (e.year() - r.year()) + (e.month() - r.month()),
							n = r.clone().add(o, d),
							i = e - n < 0,
							a = r.clone().add(o + (i ? -1 : 1), d);
						return +(-(o + (e - n) / (i ? n - a : a - n)) || 0);
					},
					a: function (t) {
						return t < 0 ? Math.ceil(t) || 0 : Math.floor(t);
					},
					p: function (t) {
						return (
							{ M: d, y: s, w: m, d: l, D: p, h: a, m: i, s: n, ms: o, Q: c }[t] ||
							String(t || '')
								.toLowerCase()
								.replace(/s$/, '')
						);
					},
					u: function (t) {
						return void 0 === t;
					}
				},
				w = 'en',
				y = {};
			y[w] = u;
			var v = '$isDayjsObject',
				k = function (t) {
					return t instanceof j || !(!t || !t[v]);
				},
				$ = function t(r, e, o) {
					var n;
					if (!r) return w;
					if ('string' == typeof r) {
						var i = r.toLowerCase();
						y[i] && (n = i), e && ((y[i] = e), (n = i));
						var a = r.split('-');
						if (!n && a.length > 1) return t(a[0]);
					} else {
						var l = r.name;
						(y[l] = r), (n = l);
					}
					return !o && n && (w = n), n || (!o && w);
				},
				_ = function (t, r) {
					if (k(t)) return t.clone();
					var e = 'object' == typeof r ? r : {};
					return (e.date = t), (e.args = arguments), new j(e);
				},
				M = h;
			(M.l = $),
				(M.i = k),
				(M.w = function (t, r) {
					return _(t, { locale: r.$L, utc: r.$u, x: r.$x, $offset: r.$offset });
				});
			var j = (function () {
					function u(t) {
						(this.$L = $(t.locale, null, !0)),
							this.parse(t),
							(this.$x = this.$x || t.x || {}),
							(this[v] = !0);
					}
					var x = u.prototype;
					return (
						(x.parse = function (t) {
							(this.$d = (function (t) {
								var r = t.date,
									e = t.utc;
								if (null === r) return new Date(NaN);
								if (M.u(r)) return new Date();
								if (r instanceof Date) return new Date(r);
								if ('string' == typeof r && !/Z$/i.test(r)) {
									var o = r.match(f);
									if (o) {
										var n = o[2] - 1 || 0,
											i = (o[7] || '0').substring(0, 3);
										return e
											? new Date(Date.UTC(o[1], n, o[3] || 1, o[4] || 0, o[5] || 0, o[6] || 0, i))
											: new Date(o[1], n, o[3] || 1, o[4] || 0, o[5] || 0, o[6] || 0, i);
									}
								}
								return new Date(r);
							})(t)),
								this.init();
						}),
						(x.init = function () {
							var t = this.$d;
							(this.$y = t.getFullYear()),
								(this.$M = t.getMonth()),
								(this.$D = t.getDate()),
								(this.$W = t.getDay()),
								(this.$H = t.getHours()),
								(this.$m = t.getMinutes()),
								(this.$s = t.getSeconds()),
								(this.$ms = t.getMilliseconds());
						}),
						(x.$utils = function () {
							return M;
						}),
						(x.isValid = function () {
							return !(this.$d.toString() === g);
						}),
						(x.isSame = function (t, r) {
							var e = _(t);
							return this.startOf(r) <= e && e <= this.endOf(r);
						}),
						(x.isAfter = function (t, r) {
							return _(t) < this.startOf(r);
						}),
						(x.isBefore = function (t, r) {
							return this.endOf(r) < _(t);
						}),
						(x.$g = function (t, r, e) {
							return M.u(t) ? this[r] : this.set(e, t);
						}),
						(x.unix = function () {
							return Math.floor(this.valueOf() / 1e3);
						}),
						(x.valueOf = function () {
							return this.$d.getTime();
						}),
						(x.startOf = function (t, r) {
							var e = this,
								o = !!M.u(r) || r,
								c = M.p(t),
								g = function (t, r) {
									var n = M.w(e.$u ? Date.UTC(e.$y, r, t) : new Date(e.$y, r, t), e);
									return o ? n : n.endOf(l);
								},
								f = function (t, r) {
									return M.w(
										e
											.toDate()
											[t].apply(e.toDate('s'), (o ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(r)),
										e
									);
								},
								b = this.$W,
								u = this.$M,
								x = this.$D,
								h = 'set' + (this.$u ? 'UTC' : '');
							switch (c) {
								case s:
									return o ? g(1, 0) : g(31, 11);
								case d:
									return o ? g(1, u) : g(0, u + 1);
								case m:
									var w = this.$locale().weekStart || 0,
										y = (b < w ? b + 7 : b) - w;
									return g(o ? x - y : x + (6 - y), u);
								case l:
								case p:
									return f(h + 'Hours', 0);
								case a:
									return f(h + 'Minutes', 1);
								case i:
									return f(h + 'Seconds', 2);
								case n:
									return f(h + 'Milliseconds', 3);
								default:
									return this.clone();
							}
						}),
						(x.endOf = function (t) {
							return this.startOf(t, !1);
						}),
						(x.$set = function (t, r) {
							var e,
								m = M.p(t),
								c = 'set' + (this.$u ? 'UTC' : ''),
								g = ((e = {}),
								(e[l] = c + 'Date'),
								(e[p] = c + 'Date'),
								(e[d] = c + 'Month'),
								(e[s] = c + 'FullYear'),
								(e[a] = c + 'Hours'),
								(e[i] = c + 'Minutes'),
								(e[n] = c + 'Seconds'),
								(e[o] = c + 'Milliseconds'),
								e)[m],
								f = m === l ? this.$D + (r - this.$W) : r;
							if (m === d || m === s) {
								var b = this.clone().set(p, 1);
								b.$d[g](f), b.init(), (this.$d = b.set(p, Math.min(this.$D, b.daysInMonth())).$d);
							} else g && this.$d[g](f);
							return this.init(), this;
						}),
						(x.set = function (t, r) {
							return this.clone().$set(t, r);
						}),
						(x.get = function (t) {
							return this[M.p(t)]();
						}),
						(x.add = function (o, c) {
							var p,
								g = this;
							o = Number(o);
							var f = M.p(c),
								b = function (t) {
									var r = _(g);
									return M.w(r.date(r.date() + Math.round(t * o)), g);
								};
							if (f === d) return this.set(d, this.$M + o);
							if (f === s) return this.set(s, this.$y + o);
							if (f === l) return b(1);
							if (f === m) return b(7);
							var u = ((p = {}), (p[i] = r), (p[a] = e), (p[n] = t), p)[f] || 1,
								x = this.$d.getTime() + o * u;
							return M.w(x, this);
						}),
						(x.subtract = function (t, r) {
							return this.add(-1 * t, r);
						}),
						(x.format = function (t) {
							var r = this,
								e = this.$locale();
							if (!this.isValid()) return e.invalidDate || g;
							var o = t || 'YYYY-MM-DDTHH:mm:ssZ',
								n = M.z(this),
								i = this.$H,
								a = this.$m,
								l = this.$M,
								m = e.weekdays,
								d = e.months,
								c = e.meridiem,
								s = function (t, e, n, i) {
									return (t && (t[e] || t(r, o))) || n[e].slice(0, i);
								},
								p = function (t) {
									return M.s(i % 12 || 12, t, '0');
								},
								f =
									c ||
									function (t, r, e) {
										var o = t < 12 ? 'AM' : 'PM';
										return e ? o.toLowerCase() : o;
									};
							return o.replace(b, function (t, o) {
								return (
									o ||
									(function (t) {
										switch (t) {
											case 'YY':
												return String(r.$y).slice(-2);
											case 'YYYY':
												return M.s(r.$y, 4, '0');
											case 'M':
												return l + 1;
											case 'MM':
												return M.s(l + 1, 2, '0');
											case 'MMM':
												return s(e.monthsShort, l, d, 3);
											case 'MMMM':
												return s(d, l);
											case 'D':
												return r.$D;
											case 'DD':
												return M.s(r.$D, 2, '0');
											case 'd':
												return String(r.$W);
											case 'dd':
												return s(e.weekdaysMin, r.$W, m, 2);
											case 'ddd':
												return s(e.weekdaysShort, r.$W, m, 3);
											case 'dddd':
												return m[r.$W];
											case 'H':
												return String(i);
											case 'HH':
												return M.s(i, 2, '0');
											case 'h':
												return p(1);
											case 'hh':
												return p(2);
											case 'a':
												return f(i, a, !0);
											case 'A':
												return f(i, a, !1);
											case 'm':
												return String(a);
											case 'mm':
												return M.s(a, 2, '0');
											case 's':
												return String(r.$s);
											case 'ss':
												return M.s(r.$s, 2, '0');
											case 'SSS':
												return M.s(r.$ms, 3, '0');
											case 'Z':
												return n;
										}
										return null;
									})(t) ||
									n.replace(':', '')
								);
							});
						}),
						(x.utcOffset = function () {
							return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
						}),
						(x.diff = function (o, p, g) {
							var f,
								b = this,
								u = M.p(p),
								x = _(o),
								h = (x.utcOffset() - this.utcOffset()) * r,
								w = this - x,
								y = function () {
									return M.m(b, x);
								};
							switch (u) {
								case s:
									f = y() / 12;
									break;
								case d:
									f = y();
									break;
								case c:
									f = y() / 3;
									break;
								case m:
									f = (w - h) / 6048e5;
									break;
								case l:
									f = (w - h) / 864e5;
									break;
								case a:
									f = w / e;
									break;
								case i:
									f = w / r;
									break;
								case n:
									f = w / t;
									break;
								default:
									f = w;
							}
							return g ? f : M.a(f);
						}),
						(x.daysInMonth = function () {
							return this.endOf(d).$D;
						}),
						(x.$locale = function () {
							return y[this.$L];
						}),
						(x.locale = function (t, r) {
							if (!t) return this.$L;
							var e = this.clone(),
								o = $(t, r, !0);
							return o && (e.$L = o), e;
						}),
						(x.clone = function () {
							return M.w(this.$d, this);
						}),
						(x.toDate = function () {
							return new Date(this.valueOf());
						}),
						(x.toJSON = function () {
							return this.isValid() ? this.toISOString() : null;
						}),
						(x.toISOString = function () {
							return this.$d.toISOString();
						}),
						(x.toString = function () {
							return this.$d.toUTCString();
						}),
						u
					);
				})(),
				z = j.prototype;
			return (
				(_.prototype = z),
				[
					['$ms', o],
					['$s', n],
					['$m', i],
					['$H', a],
					['$W', l],
					['$M', d],
					['$y', s],
					['$D', p]
				].forEach(function (t) {
					z[t[1]] = function (r) {
						return this.$g(r, t[0], t[1]);
					};
				}),
				(_.extend = function (t, r) {
					return t.$i || (t(r, j, _), (t.$i = !0)), _;
				}),
				(_.locale = $),
				(_.isDayjs = k),
				(_.unix = function (t) {
					return _(1e3 * t);
				}),
				(_.en = y[w]),
				(_.Ls = y),
				(_.p = {}),
				_
			);
		})();
	});
	F(function (t, r) {
		t.exports = (function (t) {
			function r(t) {
				return t && 'object' == typeof t && 'default' in t ? t : { default: t };
			}
			var e = r(t),
				o = {
					name: 'it',
					weekdays: 'domenica_lunedì_martedì_mercoledì_giovedì_venerdì_sabato'.split('_'),
					weekdaysShort: 'dom_lun_mar_mer_gio_ven_sab'.split('_'),
					weekdaysMin: 'do_lu_ma_me_gi_ve_sa'.split('_'),
					months:
						'gennaio_febbraio_marzo_aprile_maggio_giugno_luglio_agosto_settembre_ottobre_novembre_dicembre'.split(
							'_'
						),
					weekStart: 1,
					monthsShort: 'gen_feb_mar_apr_mag_giu_lug_ago_set_ott_nov_dic'.split('_'),
					formats: {
						LT: 'HH:mm',
						LTS: 'HH:mm:ss',
						L: 'DD/MM/YYYY',
						LL: 'D MMMM YYYY',
						LLL: 'D MMMM YYYY HH:mm',
						LLLL: 'dddd D MMMM YYYY HH:mm'
					},
					relativeTime: {
						future: 'tra %s',
						past: '%s fa',
						s: 'qualche secondo',
						m: 'un minuto',
						mm: '%d minuti',
						h: "un' ora",
						hh: '%d ore',
						d: 'un giorno',
						dd: '%d giorni',
						M: 'un mese',
						MM: '%d mesi',
						y: 'un anno',
						yy: '%d anni'
					},
					ordinal: function (t) {
						return t + 'º';
					}
				};
			return e.default.locale(o, null, !0), o;
		})(U);
	});
	var P = '0.20.1',
		W = F(function (t, r) {
			(r.__esModule = !0),
				(r.LanguageTranslator = r.addComponent = r.getChildStyleToPass = void 0),
				(r.getChildStyleToPass = function (t, r) {
					var e,
						o,
						n = '';
					if (
						t &&
						(null == r ? void 0 : r.length) &&
						(null === (e = Object.keys(t)) || void 0 === e ? void 0 : e.length) &&
						(null ===
							(o =
								null == r
									? void 0
									: r.filter(function (r) {
											return Object.keys(t).includes(r.name);
									  })) || void 0 === o
							? void 0
							: o.length)
					)
						for (
							var i = function (e) {
									(null == r
										? void 0
										: r.filter(function (r) {
												return r.name === e && r.defaultValue !== t[e];
										  })) && (n += ''.concat(e, ':').concat(t[e], ';'));
								},
								a = 0,
								l = Object.keys(t);
							a < l.length;
							a++
						) {
							i(l[a]);
						}
					return n;
				}),
				(r.addComponent = function (t) {
					var r,
						e =
							(null === (r = null == t ? void 0 : t.repoName.split('/')) || void 0 === r
								? void 0
								: r[1]) || (null == t ? void 0 : t.repoName);
					if (!e) throw new Error('wrong componentPath ' + (null == t ? void 0 : t.repoName));
					if (!(null == t ? void 0 : t.version))
						throw new Error('wrong version ' + (null == t ? void 0 : t.version));
					var o = (null == t ? void 0 : t.iifePath) || 'release/release.js';
					if (!document.getElementById(e + '-script'))
						try {
							var n = document.createElement('script');
							(n.id = e + '-script'),
								(n.src = 'https://cdn.jsdelivr.net/npm/'
									.concat(t.repoName, '@')
									.concat(t.version, '/')
									.concat(o)),
								(null == t ? void 0 : t.local) &&
									location.href.includes('localhost') &&
									(n.src = ''.concat(t.local)),
								document.head.appendChild(n);
						} catch (t) {
							console.warn(t);
						}
				});
			var e = (function () {
				function t(t) {
					if (!(null == t ? void 0 : t.dictionary)) throw new Error('no dictionary provided');
					(this.dictionary = t.dictionary), this.setLang(null == t ? void 0 : t.lang);
				}
				return (
					(t.prototype.setLang = function (r) {
						r || (r = t.getDefaultLang()), (this.lang = r);
					}),
					(t.prototype.translateWord = function (r, e) {
						return t.getDictionaryWord(r, this.dictionary, e || this.lang);
					}),
					(t.prototype.translateDate = function (r, e, o) {
						return t.formatDate(r, e, o || this.lang);
					}),
					(t.getDefaultLang = function () {
						var t,
							r,
							e,
							o,
							n,
							i = 'en';
						return (
							(null === navigator || void 0 === navigator ? void 0 : navigator.languages) &&
								(null ===
									(e =
										null ===
											(r =
												null === (t = navigator.languages[0]) || void 0 === t
													? void 0
													: t.split('-')[0]) || void 0 === r
											? void 0
											: r.toLowerCase()) || void 0 === e
									? void 0
									: e.length) &&
								(i =
									null ===
										(n =
											null === (o = navigator.languages[0]) || void 0 === o
												? void 0
												: o.split('-')[0]) || void 0 === n
										? void 0
										: n.toLowerCase()),
							i
						);
					}),
					(t.getDictionaryWord = function (r, e, o) {
						var n;
						if (!r) throw new Error('no wordKey provided');
						if (!e) throw new Error('no dictionary provided');
						if (o && (null === (n = e[o]) || void 0 === n ? void 0 : n[r])) return e[o][r];
						var i = '',
							a = t.getDefaultLang();
						if (!o || a !== o) {
							var l = null == e ? void 0 : e[a];
							(null == l ? void 0 : l[r]) && (i = l[r]);
						}
						return i;
					}),
					(t.formatDate = function (r, e, o) {
						if (!r) throw new Error('no date provided');
						if ('function' != typeof r.getMonth) throw new Error('wrong date format');
						return new Intl.DateTimeFormat(o || t.getDefaultLang(), e).format(r);
					}),
					t
				);
			})();
			r.LanguageTranslator = e;
		}),
		J = /\/\*[^*]*\*+([^/*][^*]*\*+)*\//g,
		R = /\n/g,
		Z = /^\s*/,
		q = /^(\*?[-#/*\\\w]+(\[[0-9a-z_-]+\])?)\s*/,
		K = /^:\s*/,
		Q = /^((?:'(?:\\'|.)*?'|"(?:\\"|.)*?"|\([^)]*?\)|[^};])+)/,
		X = /^[;\s]*/,
		G = /^\s+|\s+$/g,
		tt = '';
	function rt(t) {
		return t ? t.replace(G, tt) : tt;
	}
	var et = function (t, r) {
			var e,
				o = null;
			if (!t || 'string' != typeof t) return o;
			for (
				var n,
					i,
					a = (function (t, r) {
						if ('string' != typeof t) throw new TypeError('First argument must be a string');
						if (!t) return [];
						r = r || {};
						var e = 1,
							o = 1;
						function n(t) {
							var r = t.match(R);
							r && (e += r.length);
							var n = t.lastIndexOf('\n');
							o = ~n ? t.length - n : o + t.length;
						}
						function i() {
							var t = { line: e, column: o };
							return function (r) {
								return (r.position = new a(t)), d(), r;
							};
						}
						function a(t) {
							(this.start = t), (this.end = { line: e, column: o }), (this.source = r.source);
						}
						function l(n) {
							var i = new Error(r.source + ':' + e + ':' + o + ': ' + n);
							if (
								((i.reason = n),
								(i.filename = r.source),
								(i.line = e),
								(i.column = o),
								(i.source = t),
								!r.silent)
							)
								throw i;
						}
						function m(r) {
							var e = r.exec(t);
							if (e) {
								var o = e[0];
								return n(o), (t = t.slice(o.length)), e;
							}
						}
						function d() {
							m(Z);
						}
						function c(t) {
							var r;
							for (t = t || []; (r = s()); ) !1 !== r && t.push(r);
							return t;
						}
						function s() {
							var r = i();
							if ('/' == t.charAt(0) && '*' == t.charAt(1)) {
								for (
									var e = 2;
									tt != t.charAt(e) && ('*' != t.charAt(e) || '/' != t.charAt(e + 1));

								)
									++e;
								if (((e += 2), tt === t.charAt(e - 1))) return l('End of comment missing');
								var a = t.slice(2, e - 2);
								return (
									(o += 2), n(a), (t = t.slice(e)), (o += 2), r({ type: 'comment', comment: a })
								);
							}
						}
						function p() {
							var t = i(),
								r = m(q);
							if (r) {
								if ((s(), !m(K))) return l("property missing ':'");
								var e = m(Q),
									o = t({
										type: 'declaration',
										property: rt(r[0].replace(J, tt)),
										value: e ? rt(e[0].replace(J, tt)) : tt
									});
								return m(X), o;
							}
						}
						return (
							(a.prototype.content = t),
							d(),
							(function () {
								var t,
									r = [];
								for (c(r); (t = p()); ) !1 !== t && (r.push(t), c(r));
								return r;
							})()
						);
					})(t),
					l = 'function' == typeof r,
					m = 0,
					d = a.length;
				m < d;
				m++
			)
				(n = (e = a[m]).property), (i = e.value), l ? r(n, i, e) : i && (o || (o = {}), (o[n] = i));
			return o;
		},
		ot = {
			vars: [
				{ name: '--bs-primary', valueType: 'color', theme: 'bootstrap', defaultValue: '#07689f' }
			],
			parts: []
		},
		nt = {
			vars: [
				{ name: '--bs-primary', valueType: 'color', theme: 'bootstrap', defaultValue: '#07689f' },
				{ name: '--bs-secondary', valueType: 'color', theme: 'bootstrap', defaultValue: '#c9d6df' },
				{ name: '--bs-success', valueType: 'color', theme: 'bootstrap', defaultValue: '#11d3bc' },
				{ name: '--bs-info', valueType: 'color', theme: 'bootstrap', defaultValue: '#a2d5f2' },
				{ name: '--bs-warning', valueType: 'color', theme: 'bootstrap', defaultValue: '#ffc107' },
				{ name: '--bs-danger', valueType: 'color', theme: 'bootstrap', defaultValue: '#f67280' },
				{ name: '--hb-modal-max-width', valueType: 'htmlsize', defaultValue: '500px' }
			],
			parts: [{ name: 'modal-dialog', description: 'The modal dialog' }]
		},
		it = {
			vars: [
				{ name: '--bs-primary', valueType: 'color', theme: 'bootstrap', defaultValue: '#07689f' },
				{ name: '--bs-secondary', valueType: 'color', theme: 'bootstrap', defaultValue: '#c9d6df' },
				{ name: '--bs-success', valueType: 'color', theme: 'bootstrap', defaultValue: '#11d3bc' },
				{ name: '--bs-info', valueType: 'color', theme: 'bootstrap', defaultValue: '#a2d5f2' },
				{ name: '--bs-warning', valueType: 'color', theme: 'bootstrap', defaultValue: '#ffc107' },
				{ name: '--bs-danger', valueType: 'color', theme: 'bootstrap', defaultValue: '#f67280' }
			],
			parts: []
		};
	function at(t, r, e) {
		const o = t.slice();
		return (o[75] = r[e]), o;
	}
	function lt(t, r, e) {
		const o = t.slice();
		return (o[78] = r[e]), o;
	}
	function mt(t, r, e) {
		const o = t.slice();
		return (o[81] = r[e]), o;
	}
	function dt(t, r, e) {
		const o = t.slice();
		return (o[84] = r[e]), o;
	}
	function ct(t, r, e) {
		const o = t.slice();
		return (o[81] = r[e]), o;
	}
	function st(t, r, e) {
		const o = t.slice();
		return (o[89] = r[e]), o;
	}
	function pt(t, r, e) {
		const o = t.slice();
		return (o[89] = r[e]), o;
	}
	function gt(t, r, e) {
		const o = t.slice();
		return (o[96] = r[e]), o;
	}
	function ft(t, r, e) {
		const o = t.slice();
		return (o[89] = r[e]), o;
	}
	function bt(t, r, e) {
		const o = t.slice();
		return (o[89] = r[e]), o;
	}
	function ut(t) {
		let r,
			e,
			o,
			n,
			i,
			c,
			p,
			g,
			b,
			u,
			h,
			w,
			y,
			v,
			k = [],
			$ = new Map(),
			_ = t[11] && t[7]?.length && xt(t),
			M = t[5];
		const j = (t) => t[89].key;
		for (let r = 0; r < M.length; r += 1) {
			let e = bt(t, M, r),
				o = j(e);
			$.set(o, (k[r] = jt(o, e)));
		}
		let z = t[6] && zt();
		function C(t, r) {
			return t[19] ? Ct : St;
		}
		let S = C(t),
			D = S(t),
			T = t[1]?.length && Zt(t),
			O = t[7]?.length && hr(t),
			L = !t[11] && t[10] && kr(t),
			E = !0 !== t[9] && $r(t);
		return {
			c() {
				(r = d('table')), (e = d('thead')), (o = d('tr')), _ && _.c(), (n = s());
				for (let t = 0; t < k.length; t += 1) k[t].c();
				(i = s()),
					z && z.c(),
					(c = s()),
					D.c(),
					(p = s()),
					(g = d('tbody')),
					T && T.c(),
					(b = s()),
					(u = d('nav')),
					O && O.c(),
					(h = s()),
					L && L.c(),
					(w = s()),
					(y = d('slot')),
					(v = s()),
					E && E.c(),
					f(r, 'class', 'table table-responsive table-striped table-hover align-middle'),
					x(r, 'width', '100%'),
					x(r, 'text-align', 'left'),
					f(y, 'name', 'buttons-container'),
					x(u, 'margin-top', '20px'),
					f(u, 'aria-label', 'actions on selected');
			},
			m(t, m) {
				l(t, r, m), a(r, e), a(e, o), _ && _.m(o, null), a(o, n);
				for (let t = 0; t < k.length; t += 1) k[t] && k[t].m(o, null);
				a(o, i),
					z && z.m(o, null),
					a(e, c),
					D.m(e, null),
					a(r, p),
					a(r, g),
					T && T.m(g, null),
					l(t, b, m),
					l(t, u, m),
					O && O.m(u, null),
					a(u, h),
					L && L.m(u, null),
					a(u, w),
					a(u, y),
					a(u, v),
					E && E.m(u, null);
			},
			p(t, r) {
				t[11] && t[7]?.length
					? _
						? _.p(t, r)
						: ((_ = xt(t)), _.c(), _.m(o, n))
					: _ && (_.d(1), (_ = null)),
					(12320 & r[0]) | (16 & r[1]) &&
						((M = t[5]), (k = I(k, r, j, 1, t, M, $, o, Y, jt, i, bt))),
					t[6] ? z || ((z = zt()), z.c(), z.m(o, null)) : z && (z.d(1), (z = null)),
					S === (S = C(t)) && D ? D.p(t, r) : (D.d(1), (D = S(t)), D && (D.c(), D.m(e, null))),
					t[1]?.length
						? T
							? T.p(t, r)
							: ((T = Zt(t)), T.c(), T.m(g, null))
						: T && (T.d(1), (T = null)),
					t[7]?.length
						? O
							? O.p(t, r)
							: ((O = hr(t)), O.c(), O.m(u, h))
						: O && (O.d(1), (O = null)),
					!t[11] && t[10]
						? L
							? L.p(t, r)
							: ((L = kr(t)), L.c(), L.m(u, w))
						: L && (L.d(1), (L = null)),
					!0 !== t[9]
						? E
							? E.p(t, r)
							: ((E = $r(t)), E.c(), E.m(u, null))
						: E && (E.d(1), (E = null));
			},
			d(t) {
				t && m(r), _ && _.d();
				for (let t = 0; t < k.length; t += 1) k[t].d();
				z && z.d(), D.d(), T && T.d(), t && m(b), t && m(u), O && O.d(), L && L.d(), E && E.d();
			}
		};
	}
	function xt(t) {
		let r;
		function e(t, r) {
			return t[19] ? ht : wt;
		}
		let o = e(t),
			n = o(t);
		return {
			c() {
				n.c(), (r = p());
			},
			m(t, e) {
				n.m(t, e), l(t, r, e);
			},
			p(t, i) {
				o === (o = e(t)) && n
					? n.p(t, i)
					: (n.d(1), (n = o(t)), n && (n.c(), n.m(r.parentNode, r)));
			},
			d(t) {
				n.d(t), t && m(r);
			}
		};
	}
	function ht(r) {
		let e;
		return {
			c() {
				(e = d('th')), f(e, 'scope', 'col');
			},
			m(t, r) {
				l(t, e, r);
			},
			p: t,
			d(t) {
				t && m(e);
			}
		};
	}
	function wt(t) {
		let r;
		function e(t, r) {
			return t[20].length !== t[1].length ? vt : yt;
		}
		let o = e(t),
			n = o(t);
		return {
			c() {
				n.c(), (r = p());
			},
			m(t, e) {
				n.m(t, e), l(t, r, e);
			},
			p(t, i) {
				o === (o = e(t)) && n
					? n.p(t, i)
					: (n.d(1), (n = o(t)), n && (n.c(), n.m(r.parentNode, r)));
			},
			d(t) {
				n.d(t), t && m(r);
			}
		};
	}
	function yt(r) {
		let e, o, n;
		return {
			c() {
				(e = d('button')), (e.textContent = 'rimuovi tutti'), f(e, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, e, i), o || ((n = g(e, 'click', r[33])), (o = !0));
			},
			p: t,
			d(t) {
				t && m(e), (o = !1), n();
			}
		};
	}
	function vt(r) {
		let e, o, n;
		return {
			c() {
				(e = d('button')), (e.textContent = 'seleziona tutti'), f(e, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, e, i), o || ((n = g(e, 'click', r[32])), (o = !0));
			},
			p: t,
			d(t) {
				t && m(e), (o = !1), n();
			}
		};
	}
	function kt(t) {
		let r, e, o;
		function n(t, r) {
			return t[12] && t[89].key === t[12]
				? 'asc' === t[13]
					? _t
					: 'desc' === t[13]
					? $t
					: void 0
				: Mt;
		}
		let i = n(t),
			a = i && i(t);
		function c() {
			return t[52](t[89]);
		}
		return {
			c() {
				(r = d('button')), a && a.c(), x(r, 'border', 'none'), x(r, 'background-color', 'inherit');
			},
			m(t, n) {
				l(t, r, n), a && a.m(r, null), e || ((o = g(r, 'click', c)), (e = !0));
			},
			p(e, o) {
				i !== (i = n((t = e))) && (a && a.d(1), (a = i && i(t)), a && (a.c(), a.m(r, null)));
			},
			d(t) {
				t && m(r), a && a.d(), (e = !1), o();
			}
		};
	}
	function $t(t) {
		let r;
		return {
			c() {
				r = c('↥');
			},
			m(t, e) {
				l(t, r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function _t(t) {
		let r;
		return {
			c() {
				r = c('↧');
			},
			m(t, e) {
				l(t, r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function Mt(t) {
		let r;
		return {
			c() {
				r = c('⇅');
			},
			m(t, e) {
				l(t, r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function jt(t, r) {
		let e,
			o,
			n,
			i = r[89].label + '',
			p = !r[89].nosort && 'actions' !== r[89].type && kt(r);
		return {
			key: t,
			first: null,
			c() {
				(e = d('th')), (o = c(i)), (n = s()), p && p.c(), f(e, 'scope', 'col'), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), a(e, o), a(e, n), p && p.m(e, null);
			},
			p(t, n) {
				(r = t),
					32 & n[0] && i !== (i = r[89].label + '') && u(o, i),
					r[89].nosort || 'actions' === r[89].type
						? p && (p.d(1), (p = null))
						: p
						? p.p(r, n)
						: ((p = kt(r)), p.c(), p.m(e, null));
			},
			d(t) {
				t && m(e), p && p.d();
			}
		};
	}
	function zt(t) {
		let r;
		return {
			c() {
				(r = d('th')), (r.textContent = 'azioni'), f(r, 'scope', 'col');
			},
			m(t, e) {
				l(t, r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function Ct(t) {
		let r,
			e,
			o,
			n,
			i,
			c = [],
			p = new Map(),
			g = [],
			f = new Map(),
			b = t[11] && t[7]?.length && Dt(t),
			u = t[5];
		const x = (t) => t[89].key;
		for (let r = 0; r < u.length; r += 1) {
			let e = pt(t, u, r),
				o = x(e);
			p.set(o, (c[r] = Ht(o, e)));
		}
		let h = t[11] && t[7]?.length && Bt(t),
			w = t[5];
		const y = (t) => t[89].key;
		for (let r = 0; r < w.length; r += 1) {
			let e = st(t, w, r),
				o = y(e);
			f.set(o, (g[r] = Ft(o, e)));
		}
		return {
			c() {
				(r = d('tr')), b && b.c(), (e = s());
				for (let t = 0; t < c.length; t += 1) c[t].c();
				(o = s()), (n = d('tr')), h && h.c(), (i = s());
				for (let t = 0; t < g.length; t += 1) g[t].c();
			},
			m(t, m) {
				l(t, r, m), b && b.m(r, null), a(r, e);
				for (let t = 0; t < c.length; t += 1) c[t] && c[t].m(r, null);
				l(t, o, m), l(t, n, m), h && h.m(n, null), a(n, i);
				for (let t = 0; t < g.length; t += 1) g[t] && g[t].m(n, null);
			},
			p(t, o) {
				t[11] && t[7]?.length
					? b
						? b.p(t, o)
						: ((b = Dt(t)), b.c(), b.m(r, e))
					: b && (b.d(1), (b = null)),
					25165856 & o[0] && ((u = t[5]), (c = I(c, o, x, 1, t, u, p, r, Y, Ht, null, pt))),
					t[11] && t[7]?.length
						? h
							? h.p(t, o)
							: ((h = Bt(t)), h.c(), h.m(n, i))
						: h && (h.d(1), (h = null)),
					33554464 & o[0] && ((w = t[5]), (g = I(g, o, y, 1, t, w, f, n, Y, Ft, null, st)));
			},
			d(t) {
				t && m(r), b && b.d();
				for (let t = 0; t < c.length; t += 1) c[t].d();
				t && m(o), t && m(n), h && h.d();
				for (let t = 0; t < g.length; t += 1) g[t].d();
			}
		};
	}
	function St(t) {
		let r,
			e,
			o = t[5].find(Mr),
			n = t[11] && t[7]?.length && Ut(t),
			i = o && Pt(t);
		return {
			c() {
				(r = d('tr')), n && n.c(), (e = s()), i && i.c();
			},
			m(t, o) {
				l(t, r, o), n && n.m(r, null), a(r, e), i && i.m(r, null);
			},
			p(t, a) {
				t[11] && t[7]?.length
					? n
						? n.p(t, a)
						: ((n = Ut(t)), n.c(), n.m(r, e))
					: n && (n.d(1), (n = null)),
					32 & a[0] && (o = t[5].find(Mr)),
					o ? (i ? i.p(t, a) : ((i = Pt(t)), i.c(), i.m(r, null))) : i && (i.d(1), (i = null));
			},
			d(t) {
				t && m(r), n && n.d(), i && i.d();
			}
		};
	}
	function Dt(t) {
		let r;
		function e(t, r) {
			return t[20].length !== t[1].length ? Ot : Tt;
		}
		let o = e(t),
			n = o(t);
		return {
			c() {
				(r = d('th')), n.c(), f(r, 'scope', 'col');
			},
			m(t, e) {
				l(t, r, e), n.m(r, null);
			},
			p(t, i) {
				o === (o = e(t)) && n ? n.p(t, i) : (n.d(1), (n = o(t)), n && (n.c(), n.m(r, null)));
			},
			d(t) {
				t && m(r), n.d();
			}
		};
	}
	function Tt(r) {
		let e, o, n;
		return {
			c() {
				(e = d('button')), (e.textContent = 'rimuovi tutti'), f(e, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, e, i), o || ((n = g(e, 'click', r[33])), (o = !0));
			},
			p: t,
			d(t) {
				t && m(e), (o = !1), n();
			}
		};
	}
	function Ot(r) {
		let e, o, n;
		return {
			c() {
				(e = d('button')), (e.textContent = 'seleziona tutti'), f(e, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, e, i), o || ((n = g(e, 'click', r[32])), (o = !0));
			},
			p: t,
			d(t) {
				t && m(e), (o = !1), n();
			}
		};
	}
	function Lt(r) {
		let e;
		return {
			c() {
				e = c(' ');
			},
			m(t, r) {
				l(t, e, r);
			},
			p: t,
			d(t) {
				t && m(e);
			}
		};
	}
	function Et(t) {
		let r;
		function e(t, r) {
			return t[89].type && 'datetime' === t[89].type
				? It
				: t[89].type && 'string' !== t[89].type
				? t[89].type && 'enum' === t[89].type
					? Nt
					: void 0
				: Yt;
		}
		let o = e(t),
			n = o && o(t);
		return {
			c() {
				n && n.c(), (r = p());
			},
			m(t, e) {
				n && n.m(t, e), l(t, r, e);
			},
			p(t, i) {
				o === (o = e(t)) && n
					? n.p(t, i)
					: (n && n.d(1), (n = o && o(t)), n && (n.c(), n.m(r.parentNode, r)));
			},
			d(t) {
				n && n.d(t), t && m(r);
			}
		};
	}
	function Nt(t) {
		let r,
			e,
			o,
			n,
			i = [],
			c = new Map(),
			s = t[89].select;
		const p = (t) => t[96];
		for (let r = 0; r < s.length; r += 1) {
			let e = gt(t, s, r),
				o = p(e);
			c.set(o, (i[r] = At(o, e)));
		}
		function b(...r) {
			return t[56](t[89], ...r);
		}
		return {
			c() {
				(r = d('select')), (e = d('option')), (e.textContent = 'tutti');
				for (let t = 0; t < i.length; t += 1) i[t].c();
				(e.__value = ''), (e.value = e.__value), f(r, 'class', 'form-select');
			},
			m(t, m) {
				l(t, r, m), a(r, e);
				for (let t = 0; t < i.length; t += 1) i[t] && i[t].m(r, null);
				o || ((n = g(r, 'input', b)), (o = !0));
			},
			p(e, o) {
				(t = e),
					32 & o[0] && ((s = t[89].select), (i = I(i, o, p, 1, t, s, c, r, Y, At, null, gt)));
			},
			d(t) {
				t && m(r);
				for (let t = 0; t < i.length; t += 1) i[t].d();
				(o = !1), n();
			}
		};
	}
	function Yt(t) {
		let r, e, o;
		function n(...r) {
			return t[55](t[89], ...r);
		}
		return {
			c() {
				(r = d('input')),
					f(r, 'type', 'text'),
					x(r, 'width', 'auto'),
					f(r, 'class', 'form-control'),
					f(r, 'placeholder', '...'),
					f(r, 'aria-label', 'Search'),
					f(r, 'aria-describedby', 'search');
			},
			m(t, i) {
				l(t, r, i), e || ((o = g(r, 'input', n)), (e = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), (e = !1), o();
			}
		};
	}
	function It(t) {
		let r, e, o, n, i;
		function a(...r) {
			return t[54](t[89], ...r);
		}
		return {
			c() {
				(r = d('span')),
					(r.textContent = 'Inizio'),
					(e = s()),
					(o = d('input')),
					x(r, 'width', '50px'),
					x(r, 'display', 'inline-block'),
					f(o, 'type', 'date'),
					f(o, 'class', 'form-control'),
					x(o, 'max-width', '200px'),
					x(o, 'display', 'inline-block');
			},
			m(t, m) {
				l(t, r, m), l(t, e, m), l(t, o, m), n || ((i = g(o, 'input', a)), (n = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), t && m(e), t && m(o), (n = !1), i();
			}
		};
	}
	function At(t, r) {
		let e,
			o,
			n,
			i = r[96] + '';
		return {
			key: t,
			first: null,
			c() {
				(e = d('option')),
					(o = c(i)),
					(e.__value = n = r[96]),
					(e.value = e.__value),
					(this.first = e);
			},
			m(t, r) {
				l(t, e, r), a(e, o);
			},
			p(t, a) {
				(r = t),
					32 & a[0] && i !== (i = r[96] + '') && u(o, i),
					32 & a[0] && n !== (n = r[96]) && ((e.__value = n), (e.value = e.__value));
			},
			d(t) {
				t && m(e);
			}
		};
	}
	function Ht(t, r) {
		let e, o;
		function n(t, r) {
			return t[89].search ? Et : Lt;
		}
		let i = n(r),
			c = i(r);
		return {
			key: t,
			first: null,
			c() {
				(e = d('th')), c.c(), (o = s()), f(e, 'scope', 'col'), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), c.m(e, null), a(e, o);
			},
			p(t, a) {
				i === (i = n((r = t))) && c ? c.p(r, a) : (c.d(1), (c = i(r)), c && (c.c(), c.m(e, o)));
			},
			d(t) {
				t && m(e), c.d();
			}
		};
	}
	function Bt(t) {
		let r,
			e,
			o,
			n,
			i = t[20].length + '',
			s = t[1].length + '';
		return {
			c() {
				(r = d('th')), (e = c(i)), (o = c('/')), (n = c(s)), f(r, 'scope', 'col');
			},
			m(t, i) {
				l(t, r, i), a(r, e), a(r, o), a(r, n);
			},
			p(t, r) {
				1048576 & r[0] && i !== (i = t[20].length + '') && u(e, i),
					2 & r[0] && s !== (s = t[1].length + '') && u(n, s);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function Vt(t) {
		let r, e, o, n, i;
		function a(...r) {
			return t[57](t[89], ...r);
		}
		return {
			c() {
				(r = d('span')),
					(r.textContent = 'Fine'),
					(e = s()),
					(o = d('input')),
					x(r, 'width', '50px'),
					x(r, 'display', 'inline-block'),
					f(o, 'type', 'date'),
					f(o, 'class', 'form-control'),
					x(o, 'max-width', '200px'),
					x(o, 'display', 'inline-block');
			},
			m(t, m) {
				l(t, r, m), l(t, e, m), l(t, o, m), n || ((i = g(o, 'input', a)), (n = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), t && m(e), t && m(o), (n = !1), i();
			}
		};
	}
	function Ft(t, r) {
		let e,
			o,
			n = r[89].search && r[89].type && 'datetime' === r[89].type && Vt(r);
		return {
			key: t,
			first: null,
			c() {
				(e = d('th')), n && n.c(), (o = s()), f(e, 'scope', 'col'), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), n && n.m(e, null), a(e, o);
			},
			p(t, i) {
				(r = t)[89].search && r[89].type && 'datetime' === r[89].type
					? n
						? n.p(r, i)
						: ((n = Vt(r)), n.c(), n.m(e, o))
					: n && (n.d(1), (n = null));
			},
			d(t) {
				t && m(e), n && n.d();
			}
		};
	}
	function Ut(t) {
		let r,
			e,
			o,
			n,
			i = t[20].length + '',
			s = t[1].length + '';
		return {
			c() {
				(r = d('th')), (e = c(i)), (o = c('/')), (n = c(s)), f(r, 'scope', 'col');
			},
			m(t, i) {
				l(t, r, i), a(r, e), a(r, o), a(r, n);
			},
			p(t, r) {
				1048576 & r[0] && i !== (i = t[20].length + '') && u(e, i),
					2 & r[0] && s !== (s = t[1].length + '') && u(n, s);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function Pt(t) {
		let r,
			e = [],
			o = new Map(),
			n = t[5];
		const i = (t) => t[89].key;
		for (let r = 0; r < n.length; r += 1) {
			let a = ft(t, n, r),
				l = i(a);
			o.set(l, (e[r] = Rt(l, a)));
		}
		return {
			c() {
				for (let t = 0; t < e.length; t += 1) e[t].c();
				r = p();
			},
			m(t, o) {
				for (let r = 0; r < e.length; r += 1) e[r] && e[r].m(t, o);
				l(t, r, o);
			},
			p(t, a) {
				8388640 & a[0] && ((n = t[5]), (e = I(e, a, i, 1, t, n, o, r.parentNode, Y, Rt, r, ft)));
			},
			d(t) {
				for (let r = 0; r < e.length; r += 1) e[r].d(t);
				t && m(r);
			}
		};
	}
	function Wt(t) {
		let r, e, o;
		function n(...r) {
			return t[53](t[89], ...r);
		}
		return {
			c() {
				(r = d('input')),
					f(r, 'type', 'text'),
					x(r, 'width', 'auto'),
					f(r, 'class', 'form-control'),
					f(r, 'placeholder', '...'),
					f(r, 'aria-label', 'Search'),
					f(r, 'aria-describedby', 'search');
			},
			m(t, i) {
				l(t, r, i), e || ((o = g(r, 'input', n)), (e = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), (e = !1), o();
			}
		};
	}
	function Jt(t) {
		let r;
		return {
			c() {
				r = c(' ');
			},
			m(t, e) {
				l(t, r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function Rt(t, r) {
		let e,
			o,
			n,
			i = r[89].search && Wt(r),
			c = !r[89].search && Jt();
		return {
			key: t,
			first: null,
			c() {
				(e = d('th')),
					i && i.c(),
					(o = s()),
					c && c.c(),
					(n = s()),
					f(e, 'scope', 'col'),
					(this.first = e);
			},
			m(t, r) {
				l(t, e, r), i && i.m(e, null), a(e, o), c && c.m(e, null), a(e, n);
			},
			p(t, a) {
				(r = t)[89].search
					? i
						? i.p(r, a)
						: ((i = Wt(r)), i.c(), i.m(e, o))
					: i && (i.d(1), (i = null)),
					r[89].search ? c && (c.d(1), (c = null)) : c || ((c = Jt()), c.c(), c.m(e, n));
			},
			d(t) {
				t && m(e), i && i.d(), c && c.d();
			}
		};
	}
	function Zt(t) {
		let r,
			e = [],
			o = new Map(),
			n = t[0] ? t[1] : t[1].slice(t[3] * t[2], (t[3] + 1) * t[2]);
		const i = (t) => t[78]._id;
		for (let r = 0; r < n.length; r += 1) {
			let a = lt(t, n, r),
				l = i(a);
			o.set(l, (e[r] = xr(l, a)));
		}
		return {
			c() {
				for (let t = 0; t < e.length; t += 1) e[t].c();
				r = p();
			},
			m(t, o) {
				for (let r = 0; r < e.length; r += 1) e[r] && e[r].m(t, o);
				l(t, r, o);
			},
			p(t, a) {
				(1011878383 & a[0]) | (520 & a[1]) &&
					((n = t[0] ? t[1] : t[1].slice(t[3] * t[2], (t[3] + 1) * t[2])),
					(e = I(e, a, i, 1, t, n, o, r.parentNode, Y, xr, r, lt)));
			},
			d(t) {
				for (let r = 0; r < e.length; r += 1) e[r].d(t);
				t && m(r);
			}
		};
	}
	function qt(t) {
		let r, e, o;
		function n(...r) {
			return t[47](t[78], ...r);
		}
		function i(t, r) {
			return 1048591 & r[0] && (o = null), null == o && (o = !!t[20].find(n)), o ? Qt : Kt;
		}
		let c = i(t, [-1, -1, -1, -1]),
			s = c(t);
		return {
			c() {
				(r = d('td')),
					(e = d('div')),
					s.c(),
					f(e, 'class', 'form-check'),
					x(r, 'box-shadow', 'none');
			},
			m(t, o) {
				l(t, r, o), a(r, e), s.m(e, null);
			},
			p(r, o) {
				c === (c = i((t = r), o)) && s
					? s.p(t, o)
					: (s.d(1), (s = c(t)), s && (s.c(), s.m(e, null)));
			},
			d(t) {
				t && m(r), s.d();
			}
		};
	}
	function Kt(t) {
		let r, e, o;
		function n(...r) {
			return t[59](t[78], ...r);
		}
		return {
			c() {
				(r = d('input')),
					f(r, 'id', 'flexCheckDefault'),
					f(r, 'class', 'form-check-input'),
					f(r, 'type', 'checkbox');
			},
			m(t, i) {
				l(t, r, i), e || ((o = g(r, 'change', n)), (e = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), (e = !1), o();
			}
		};
	}
	function Qt(t) {
		let r, e, o;
		function n(...r) {
			return t[58](t[78], ...r);
		}
		return {
			c() {
				(r = d('input')),
					f(r, 'id', 'flexCheckDefault'),
					f(r, 'class', 'form-check-input'),
					f(r, 'type', 'checkbox'),
					(r.checked = !0);
			},
			m(t, i) {
				l(t, r, i), e || ((o = g(r, 'change', n)), (e = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), (e = !1), o();
			}
		};
	}
	function Xt(t) {
		let r,
			e = [],
			o = new Map(),
			n = t[5];
		const i = (t) => t[84].key;
		for (let r = 0; r < n.length; r += 1) {
			let a = dt(t, n, r),
				l = i(a);
			o.set(l, (e[r] = pr(l, a)));
		}
		return {
			c() {
				for (let t = 0; t < e.length; t += 1) e[t].c();
				r = p();
			},
			m(t, o) {
				for (let r = 0; r < e.length; r += 1) e[r] && e[r].m(t, o);
				l(t, r, o);
			},
			p(t, a) {
				(339738927 & a[0]) | (520 & a[1]) &&
					((n = t[5]), (e = I(e, a, i, 1, t, n, o, r.parentNode, Y, pr, r, dt)));
			},
			d(t) {
				for (let r = 0; r < e.length; r += 1) e[r].d(t);
				t && m(r);
			}
		};
	}
	function Gt(t) {
		let r,
			e,
			o,
			n,
			i,
			c =
				('string' === t[84].type || !t[84].type) &&
				t[22](t[78], t[84]).toString().length &&
				t[84].copyTxt;
		function p(t, r) {
			return t[84].click ? er : rr;
		}
		let f = p(t),
			b = f(t),
			u = c && or(t);
		function x() {
			return t[66](t[78]);
		}
		return {
			c() {
				(r = d('td')), b.c(), (e = s()), u && u.c(), (o = s());
			},
			m(t, m) {
				l(t, r, m),
					b.m(r, null),
					a(r, e),
					u && u.m(r, null),
					a(r, o),
					n || ((i = g(r, 'click', x)), (n = !0));
			},
			p(n, i) {
				f === (f = p((t = n))) && b ? b.p(t, i) : (b.d(1), (b = f(t)), b && (b.c(), b.m(r, e))),
					47 & i[0] &&
						(c =
							('string' === t[84].type || !t[84].type) &&
							t[22](t[78], t[84]).toString().length &&
							t[84].copyTxt),
					c ? (u ? u.p(t, i) : ((u = or(t)), u.c(), u.m(r, o))) : u && (u.d(1), (u = null));
			},
			d(t) {
				t && m(r), b.d(), u && u.d(), (n = !1), i();
			}
		};
	}
	function tr(t) {
		let r;
		function e(t, r) {
			return t[78]._actions?.length ? ir : nr;
		}
		let o = e(t),
			n = o(t);
		return {
			c() {
				n.c(), (r = p());
			},
			m(t, e) {
				n.m(t, e), l(t, r, e);
			},
			p(t, i) {
				o === (o = e(t)) && n
					? n.p(t, i)
					: (n.d(1), (n = o(t)), n && (n.c(), n.m(r.parentNode, r)));
			},
			d(t) {
				n.d(t), t && m(r);
			}
		};
	}
	function rr(t) {
		let r,
			e = t[22](t[78], t[84], !0) + '';
		return {
			c() {
				r = c(e);
			},
			m(t, e) {
				l(t, r, e);
			},
			p(t, o) {
				47 & o[0] && e !== (e = t[22](t[78], t[84], !0) + '') && u(r, e);
			},
			d(t) {
				t && m(r);
			}
		};
	}
	function er(t) {
		let r,
			e,
			o,
			n,
			i = t[22](t[78], t[84], !0) + '';
		function s() {
			return t[64](t[78], t[84]);
		}
		return {
			c() {
				(r = d('button')), (e = c(i)), f(r, 'type', 'button'), f(r, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, r, i), a(r, e), o || ((n = g(r, 'click', s)), (o = !0));
			},
			p(r, o) {
				(t = r), 47 & o[0] && i !== (i = t[22](t[78], t[84], !0) + '') && u(e, i);
			},
			d(t) {
				t && m(r), (o = !1), n();
			}
		};
	}
	function or(t) {
		let r, e, o;
		function n() {
			return t[65](t[78], t[84]);
		}
		return {
			c() {
				(r = d('button')),
					(r.innerHTML = '<i class="bi-clipboard"></i>'),
					f(r, 'type', 'button'),
					f(r, 'class', 'btn btn-link');
			},
			m(t, i) {
				l(t, r, i), e || ((o = g(r, 'click', n)), (e = !0));
			},
			p(r, e) {
				t = r;
			},
			d(t) {
				t && m(r), (e = !1), o();
			}
		};
	}
	function nr(r) {
		let e;
		return {
			c() {
				e = d('td');
			},
			m(t, r) {
				l(t, e, r);
			},
			p: t,
			d(t) {
				t && m(e);
			}
		};
	}
	function ir(t) {
		let r,
			e,
			o = [],
			n = new Map(),
			i = t[78]._actions;
		const c = (t) => t[81].name;
		for (let r = 0; r < i.length; r += 1) {
			let e = ct(t, i, r),
				a = c(e);
			n.set(a, (o[r] = sr(a, e)));
		}
		return {
			c() {
				r = d('td');
				for (let t = 0; t < o.length; t += 1) o[t].c();
				e = s();
			},
			m(t, n) {
				l(t, r, n);
				for (let t = 0; t < o.length; t += 1) o[t] && o[t].m(r, null);
				a(r, e);
			},
			p(t, a) {
				67108879 & a[0] && ((i = t[78]._actions), (o = I(o, a, c, 1, t, i, n, r, Y, sr, e, ct)));
			},
			d(t) {
				t && m(r);
				for (let t = 0; t < o.length; t += 1) o[t].d();
			}
		};
	}
	function ar(t) {
		let r, e, o, n, i, c;
		function s() {
			return t[63](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = d('i')),
					f(e, 'class', (o = 'bi-' + t[81].iconOrText)),
					f(r, 'type', 'button'),
					f(r, 'class', (n = 'btn btn-' + (t[81].btnClass || 'light'))),
					x(r, 'margin-right', '10px');
			},
			m(t, o) {
				l(t, r, o), a(r, e), i || ((c = g(r, 'click', s)), (i = !0));
			},
			p(i, a) {
				(t = i),
					15 & a[0] && o !== (o = 'bi-' + t[81].iconOrText) && f(e, 'class', o),
					15 & a[0] && n !== (n = 'btn btn-' + (t[81].btnClass || 'light')) && f(r, 'class', n);
			},
			d(t) {
				t && m(r), (i = !1), c();
			}
		};
	}
	function lr(t) {
		let r,
			e,
			o,
			n,
			i,
			s = t[81].iconOrText + '';
		function p() {
			return t[62](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = c(s)),
					f(r, 'type', 'button'),
					f(r, 'class', (o = 'btn btn-' + (t[81].btnClass || 'link'))),
					x(r, 'margin-right', '10px');
			},
			m(t, o) {
				l(t, r, o), a(r, e), n || ((i = g(r, 'click', p)), (n = !0));
			},
			p(n, i) {
				(t = n),
					15 & i[0] && s !== (s = t[81].iconOrText + '') && u(e, s),
					15 & i[0] && o !== (o = 'btn btn-' + (t[81].btnClass || 'link')) && f(r, 'class', o);
			},
			d(t) {
				t && m(r), (n = !1), i();
			}
		};
	}
	function mr(t) {
		let r;
		function e(t, r) {
			return 'text' === t[81].type ? cr : 'icon' === t[81].type ? dr : void 0;
		}
		let o = e(t),
			n = o && o(t);
		return {
			c() {
				n && n.c(), (r = p());
			},
			m(t, e) {
				n && n.m(t, e), l(t, r, e);
			},
			p(t, i) {
				o === (o = e(t)) && n
					? n.p(t, i)
					: (n && n.d(1), (n = o && o(t)), n && (n.c(), n.m(r.parentNode, r)));
			},
			d(t) {
				n && n.d(t), t && m(r);
			}
		};
	}
	function dr(t) {
		let r, e, o, n, i, c;
		function s() {
			return t[61](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = d('i')),
					f(e, 'class', (o = 'bi-' + t[81].iconOrText)),
					f(r, 'type', 'button'),
					f(r, 'class', (n = 'btn btn-' + (t[81].btnClass || 'light'))),
					x(r, 'margin-right', '10px'),
					(r.disabled = !0);
			},
			m(t, o) {
				l(t, r, o), a(r, e), i || ((c = g(r, 'click', s)), (i = !0));
			},
			p(i, a) {
				(t = i),
					15 & a[0] && o !== (o = 'bi-' + t[81].iconOrText) && f(e, 'class', o),
					15 & a[0] && n !== (n = 'btn btn-' + (t[81].btnClass || 'light')) && f(r, 'class', n);
			},
			d(t) {
				t && m(r), (i = !1), c();
			}
		};
	}
	function cr(t) {
		let r,
			e,
			o,
			n,
			i,
			s = t[81].iconOrText + '';
		function p() {
			return t[60](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = c(s)),
					f(r, 'type', 'button'),
					f(r, 'class', (o = 'btn btn-' + (t[81].btnClass || 'link'))),
					x(r, 'margin-right', '10px'),
					(r.disabled = !0);
			},
			m(t, o) {
				l(t, r, o), a(r, e), n || ((i = g(r, 'click', p)), (n = !0));
			},
			p(n, i) {
				(t = n),
					15 & i[0] && s !== (s = t[81].iconOrText + '') && u(e, s),
					15 & i[0] && o !== (o = 'btn btn-' + (t[81].btnClass || 'link')) && f(r, 'class', o);
			},
			d(t) {
				t && m(r), (n = !1), i();
			}
		};
	}
	function sr(t, r) {
		let e, o;
		function n(t, r) {
			return t[81].disabled ? mr : 'text' === t[81].type ? lr : 'icon' === t[81].type ? ar : void 0;
		}
		let i = n(r),
			a = i && i(r);
		return {
			key: t,
			first: null,
			c() {
				(e = p()), a && a.c(), (o = p()), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), a && a.m(t, r), l(t, o, r);
			},
			p(t, e) {
				i === (i = n((r = t))) && a
					? a.p(r, e)
					: (a && a.d(1), (a = i && i(r)), a && (a.c(), a.m(o.parentNode, o)));
			},
			d(t) {
				t && m(e), a && a.d(t), t && m(o);
			}
		};
	}
	function pr(t, r) {
		let e, o;
		function n(t, r) {
			return 'actions' === t[84].type ? tr : Gt;
		}
		let i = n(r),
			a = i(r);
		return {
			key: t,
			first: null,
			c() {
				(e = p()), a.c(), (o = p()), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), a.m(t, r), l(t, o, r);
			},
			p(t, e) {
				i === (i = n((r = t))) && a
					? a.p(r, e)
					: (a.d(1), (a = i(r)), a && (a.c(), a.m(o.parentNode, o)));
			},
			d(t) {
				t && m(e), a.d(t), t && m(o);
			}
		};
	}
	function gr(t) {
		let r,
			e = [],
			o = new Map(),
			n = t[6];
		const i = (t) => t[81].name;
		for (let r = 0; r < n.length; r += 1) {
			let a = mt(t, n, r),
				l = i(a);
			o.set(l, (e[r] = ur(l, a)));
		}
		return {
			c() {
				r = d('td');
				for (let t = 0; t < e.length; t += 1) e[t].c();
			},
			m(t, o) {
				l(t, r, o);
				for (let t = 0; t < e.length; t += 1) e[t] && e[t].m(r, null);
			},
			p(t, a) {
				134217807 & a[0] && ((n = t[6]), (e = I(e, a, i, 1, t, n, o, r, Y, ur, null, mt)));
			},
			d(t) {
				t && m(r);
				for (let t = 0; t < e.length; t += 1) e[t].d();
			}
		};
	}
	function fr(t) {
		let r, e, o, n, i, c, p;
		function b() {
			return t[68](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = d('i')),
					(n = s()),
					f(e, 'class', (o = 'bi-' + t[81].iconOrText)),
					f(r, 'type', 'button'),
					f(r, 'class', (i = 'btn btn-' + (t[81].btnClass || 'light'))),
					x(r, 'margin-right', '10px');
			},
			m(t, o) {
				l(t, r, o), a(r, e), a(r, n), c || ((p = g(r, 'click', b)), (c = !0));
			},
			p(n, a) {
				(t = n),
					64 & a[0] && o !== (o = 'bi-' + t[81].iconOrText) && f(e, 'class', o),
					64 & a[0] && i !== (i = 'btn btn-' + (t[81].btnClass || 'light')) && f(r, 'class', i);
			},
			d(t) {
				t && m(r), (c = !1), p();
			}
		};
	}
	function br(t) {
		let r,
			e,
			o,
			n,
			i,
			s = t[81].iconOrText + '';
		function p() {
			return t[67](t[78], t[81]);
		}
		return {
			c() {
				(r = d('button')),
					(e = c(s)),
					f(r, 'type', 'button'),
					f(r, 'class', (o = 'btn btn-' + (t[81].btnClass || 'link'))),
					x(r, 'margin-right', '10px');
			},
			m(t, o) {
				l(t, r, o), a(r, e), n || ((i = g(r, 'click', p)), (n = !0));
			},
			p(n, i) {
				(t = n),
					64 & i[0] && s !== (s = t[81].iconOrText + '') && u(e, s),
					64 & i[0] && o !== (o = 'btn btn-' + (t[81].btnClass || 'link')) && f(r, 'class', o);
			},
			d(t) {
				t && m(r), (n = !1), i();
			}
		};
	}
	function ur(t, r) {
		let e, o;
		function n(t, r) {
			return 'text' === t[81].type ? br : 'icon' === t[81].type ? fr : void 0;
		}
		let i = n(r),
			a = i && i(r);
		return {
			key: t,
			first: null,
			c() {
				(e = p()), a && a.c(), (o = p()), (this.first = e);
			},
			m(t, r) {
				l(t, e, r), a && a.m(t, r), l(t, o, r);
			},
			p(t, e) {
				i === (i = n((r = t))) && a
					? a.p(r, e)
					: (a && a.d(1), (a = i && i(r)), a && (a.c(), a.m(o.parentNode, o)));
			},
			d(t) {
				t && m(e), a && a.d(t), t && m(o);
			}
		};
	}
	function xr(t, r) {
		let e,
			o,
			n,
			i,
			c = r[11] && r[7]?.length && qt(r),
			p = r[5].length && Xt(r),
			g = r[6] && gr(r);
		return {
			key: t,
			first: null,
			c() {
				(e = d('tr')),
					c && c.c(),
					(o = s()),
					p && p.c(),
					(n = s()),
					g && g.c(),
					(i = s()),
					(this.first = e);
			},
			m(t, r) {
				l(t, e, r),
					c && c.m(e, null),
					a(e, o),
					p && p.m(e, null),
					a(e, n),
					g && g.m(e, null),
					a(e, i);
			},
			p(t, a) {
				(r = t)[11] && r[7]?.length
					? c
						? c.p(r, a)
						: ((c = qt(r)), c.c(), c.m(e, o))
					: c && (c.d(1), (c = null)),
					r[5].length
						? p
							? p.p(r, a)
							: ((p = Xt(r)), p.c(), p.m(e, n))
						: p && (p.d(1), (p = null)),
					r[6] ? (g ? g.p(r, a) : ((g = gr(r)), g.c(), g.m(e, i))) : g && (g.d(1), (g = null));
			},
			d(t) {
				t && m(e), c && c.d(), p && p.d(), g && g.d();
			}
		};
	}
	function hr(t) {
		let r,
			e,
			o,
			n,
			i,
			c,
			b,
			u = t[11] && wr(t);
		return {
			c() {
				(r = d('button')),
					(e = d('i')),
					(n = s()),
					u && u.c(),
					(i = p()),
					f(e, 'class', 'bi-gear'),
					f(r, 'class', (o = 'btn btn-' + (t[11] ? 'primary' : 'secondary') + ' btn-sm'));
			},
			m(o, m) {
				l(o, r, m),
					a(r, e),
					l(o, n, m),
					u && u.m(o, m),
					l(o, i, m),
					c || ((b = g(r, 'click', t[30])), (c = !0));
			},
			p(t, e) {
				2048 & e[0] &&
					o !== (o = 'btn btn-' + (t[11] ? 'primary' : 'secondary') + ' btn-sm') &&
					f(r, 'class', o),
					t[11]
						? u
							? u.p(t, e)
							: ((u = wr(t)), u.c(), u.m(i.parentNode, i))
						: u && (u.d(1), (u = null));
			},
			d(t) {
				t && m(r), t && m(n), u && u.d(t), t && m(i), (c = !1), b();
			}
		};
	}
	function wr(t) {
		let r,
			e = t[7]?.length && yr(t);
		return {
			c() {
				e && e.c(), (r = p());
			},
			m(t, o) {
				e && e.m(t, o), l(t, r, o);
			},
			p(t, o) {
				t[7]?.length
					? e
						? e.p(t, o)
						: ((e = yr(t)), e.c(), e.m(r.parentNode, r))
					: e && (e.d(1), (e = null));
			},
			d(t) {
				e && e.d(t), t && m(r);
			}
		};
	}
	function yr(t) {
		let r,
			e = [],
			o = new Map(),
			n = t[7];
		const i = (t) => t[75].name;
		for (let r = 0; r < n.length; r += 1) {
			let a = at(t, n, r),
				l = i(a);
			o.set(l, (e[r] = vr(l, a)));
		}
		return {
			c() {
				r = d('span');
				for (let t = 0; t < e.length; t += 1) e[t].c();
				x(r, 'margin-left', '20px'),
					f(r, 'class', 'btn-group btn-group-sm'),
					f(r, 'role', 'group'),
					f(r, 'aria-label', 'Basic example');
			},
			m(t, o) {
				l(t, r, o);
				for (let t = 0; t < e.length; t += 1) e[t] && e[t].m(r, null);
			},
			p(t, a) {
				(128 & a[0]) | (1 & a[1]) && ((n = t[7]), (e = I(e, a, i, 1, t, n, o, r, Y, vr, null, at)));
			},
			d(t) {
				t && m(r);
				for (let t = 0; t < e.length; t += 1) e[t].d();
			}
		};
	}
	function vr(t, r) {
		let e,
			o,
			n,
			i,
			p,
			b = r[75].name + '';
		function x() {
			return r[69](r[75]);
		}
		return {
			key: t,
			first: null,
			c() {
				(e = d('button')),
					(o = c(b)),
					(n = s()),
					f(e, 'type', 'button'),
					f(e, 'class', 'btn btn-outline-primary'),
					(this.first = e);
			},
			m(t, r) {
				l(t, e, r), a(e, o), a(e, n), i || ((p = g(e, 'click', x)), (i = !0));
			},
			p(t, e) {
				(r = t), 128 & e[0] && b !== (b = r[75].name + '') && u(o, b);
			},
			d(t) {
				t && m(e), (i = !1), p();
			}
		};
	}
	function kr(r) {
		let e, o, n;
		return {
			c() {
				(e = d('button')),
					(e.innerHTML = '<slot name="add-button-content"><i class="bi-plus"></i></slot>'),
					x(e, 'margin-left', '20px'),
					f(e, 'class', 'btn btn-primary btn-sm');
			},
			m(t, i) {
				l(t, e, i), o || ((n = g(e, 'click', r[41])), (o = !0));
			},
			p: t,
			d(t) {
				t && m(e), (o = !1), n();
			}
		};
	}
	function $r(t) {
		let r, e, o, n, i, a;
		return {
			c() {
				(r = d('hb-paginate')),
					b(r, 'style', (e = 'float:right;' + t[16])),
					b(r, 'page', (o = t[3]?.toString() || 1)),
					b(r, 'pages', (n = t[4]?.toString() || 1));
			},
			m(e, o) {
				l(e, r, o), i || ((a = g(r, 'pageChange', t[21])), (i = !0));
			},
			p(t, i) {
				65536 & i[0] && e !== (e = 'float:right;' + t[16]) && b(r, 'style', e),
					8 & i[0] && o !== (o = t[3]?.toString() || 1) && b(r, 'page', o),
					16 & i[0] && n !== (n = t[4]?.toString() || 1) && b(r, 'pages', n);
			},
			d(t) {
				t && m(r), (i = !1), a();
			}
		};
	}
	function _r(r) {
		let e,
			n,
			i,
			c,
			p,
			u,
			h,
			w,
			y,
			v,
			k,
			$,
			_,
			M,
			j,
			z,
			C,
			S,
			D,
			T,
			O,
			L,
			E,
			N = r[5] && Array.isArray(r[5]),
			Y = N && ut(r);
		return {
			c() {
				(e = d('link')),
					(n = s()),
					(i = d('hb-dialogform')),
					(k = s()),
					($ = d('hb-dialog')),
					(D = s()),
					(T = d('div')),
					(O = d('div')),
					Y && Y.c(),
					(this.c = t),
					f(e, 'rel', 'stylesheet'),
					f(e, 'href', '/npm/bootstrap-icons.css'),
					b(i, 'style', r[18]),
					b(i, 'id', (c = r[15].itemId || 'confirmationModalForm')),
					b(i, 'show', (p = r[15].show)),
					b(i, 'title', (u = r[15].title)),
					b(i, 'confirmlabel', (h = r[15].confirmlabel || 'Conferma')),
					b(i, 'content', (w = r[15].content)),
					b(i, 'closelabel', (y = r[15].closelabel || 'Close')),
					b(i, 'schema', (v = r[15].schema ? JSON.stringify(r[15].schema) : '[]')),
					b($, 'style', r[17]),
					b($, 'id', (_ = r[14].itemId || 'confirmationModal')),
					b($, 'show', (M = r[14].show)),
					b($, 'title', (j = r[14].title)),
					b($, 'confirmlabel', (z = r[14].confirmlabel || 'Conferma')),
					b($, 'content', (C = r[14].content)),
					b($, 'closelabel', (S = r[14].closelabel || 'Close')),
					f(O, 'class', 'container-fluid'),
					x(O, 'padding', '0px'),
					x(O, 'margin-left', '0px'),
					x(O, 'margin-right', '0px'),
					f(T, 'id', 'webcomponent');
			},
			m(t, o) {
				a(document.head, e),
					l(t, n, o),
					l(t, i, o),
					l(t, k, o),
					l(t, $, o),
					l(t, D, o),
					l(t, T, o),
					a(T, O),
					Y && Y.m(O, null),
					L ||
						((E = [
							g(i, 'modalFormConfirm', r[48]),
							g(i, 'modalShow', r[49]),
							g($, 'modalConfirm', r[50]),
							g($, 'modalShow', r[51])
						]),
						(L = !0));
			},
			p(t, r) {
				262144 & r[0] && b(i, 'style', t[18]),
					32768 & r[0] && c !== (c = t[15].itemId || 'confirmationModalForm') && b(i, 'id', c),
					32768 & r[0] && p !== (p = t[15].show) && b(i, 'show', p),
					32768 & r[0] && u !== (u = t[15].title) && b(i, 'title', u),
					32768 & r[0] && h !== (h = t[15].confirmlabel || 'Conferma') && b(i, 'confirmlabel', h),
					32768 & r[0] && w !== (w = t[15].content) && b(i, 'content', w),
					32768 & r[0] && y !== (y = t[15].closelabel || 'Close') && b(i, 'closelabel', y),
					32768 & r[0] &&
						v !== (v = t[15].schema ? JSON.stringify(t[15].schema) : '[]') &&
						b(i, 'schema', v),
					131072 & r[0] && b($, 'style', t[17]),
					16384 & r[0] && _ !== (_ = t[14].itemId || 'confirmationModal') && b($, 'id', _),
					16384 & r[0] && M !== (M = t[14].show) && b($, 'show', M),
					16384 & r[0] && j !== (j = t[14].title) && b($, 'title', j),
					16384 & r[0] && z !== (z = t[14].confirmlabel || 'Conferma') && b($, 'confirmlabel', z),
					16384 & r[0] && C !== (C = t[14].content) && b($, 'content', C),
					16384 & r[0] && S !== (S = t[14].closelabel || 'Close') && b($, 'closelabel', S),
					32 & r[0] && (N = t[5] && Array.isArray(t[5])),
					N ? (Y ? Y.p(t, r) : ((Y = ut(t)), Y.c(), Y.m(O, null))) : Y && (Y.d(1), (Y = null));
			},
			i: t,
			o: t,
			d(t) {
				m(e),
					t && m(n),
					t && m(i),
					t && m(k),
					t && m($),
					t && m(D),
					t && m(T),
					Y && Y.d(),
					(L = !1),
					o(E);
			}
		};
	}
	const Mr = (t) => t.search;
	function jr(t, r, e) {
		let o,
			{ style: n } = r,
			i = '',
			a = '',
			l = '',
			{ id: m } = r,
			{ externalfilter: d } = r,
			{ rows: c } = r,
			{ size: s } = r,
			{ page: p } = r,
			{ pages: g } = r,
			{ headers: f } = r,
			{ actions: b } = r,
			{ selectactions: u } = r,
			{ selectrow: x } = r,
			{ enableselect: h } = r,
			{ disablepagination: w } = r,
			{ add_item: y } = r;
		m || (m = null);
		let $,
			_,
			M,
			j,
			z,
			C = !1,
			S = [],
			D = [];
		const T = v(),
			O = k();
		function L(t, r) {
			O(t, r), T.dispatchEvent && T.dispatchEvent(new CustomEvent(t, { detail: r }));
		}
		function E(t, r, e) {
			if (!r) return '';
			let o;
			if (r.key.includes('.')) {
				let e = t;
				for (const t of r.key.split('.')) {
					if (!e[t]) {
						o = void 0;
						break;
					}
					(e = e[t]), (o = e);
				}
			} else o = t[r.key];
			return o || 0 === o
				? r.type && 'string' !== r.type && 'enum' !== r.type
					? 'number' === r.type
						? Number(o)
						: 'datetime' === r.type
						? r.format
							? U(o).format(r.format)
							: U(o).format()
						: ''
					: e && r.truncateAt && o.length > r.truncateAt
					? o.substring(0, r.truncateAt) + '...'
					: o
				: '';
		}
		function N(t) {
			L('changeFilter', { filter: t });
			const r = S.find((r) => r.key === t.key);
			r
				? ((r.key = t.key),
				  (r.type = t.type),
				  (r.value = t.value),
				  (r.start = t.start),
				  (r.end = t.end),
				  e(45, S))
				: (S.push({ key: t.key, type: t.type, value: t.value, start: t.start, end: t.end }),
				  e(45, S)),
				e(20, (D.length = 0), D);
		}
		function Y(t, r) {
			const o = t.value;
			var n;
			'actions' !== r.type && o && o.length
				? N({ key: r.key, type: r.type, value: o })
				: (L('removeFilter', { key: (n = r.key) }),
				  S.find((t) => t.key === n) && e(45, (S = S.filter((t) => t.key !== n))),
				  e(20, (D.length = 0), D));
		}
		function I(t, r) {
			const e = t.value,
				o = S.find((t) => t.key === r.key);
			N({
				key: r.key,
				type: r.type,
				start: U(e, 'YYYY-MM-DD').toDate(),
				end: o && o.end ? o.end : void 0
			});
		}
		function A(t, r) {
			const e = t.value,
				o = S.find((t) => t.key === r.key);
			N({
				key: r.key,
				type: r.type,
				end: U(e, 'YYYY-MM-DD').toDate(),
				start: o && o.start ? o.start : void 0
			});
		}
		function H(t, r) {
			console.log('action', r, t),
				L('tableCustomActionClick', { itemId: t._id, action: r.name }),
				console.log('tttttttt', r, j),
				r.confirm
					? e(
							14,
							(j = {
								show: 'yes',
								itemId: t._id,
								action: r.name,
								confirmlabel: r.confirm.confirmLabel,
								title: r.confirm.title,
								content: r.confirm.content
							})
					  )
					: r.edit &&
					  e(
							15,
							(z = {
								show: 'yes',
								itemId: t._id,
								action: r.name,
								confirmlabel: r.edit.confirmLabel,
								title: r.edit.title,
								schema: r.edit.schema
							})
					  );
		}
		function B(t, r) {
			console.log('action', t, r), L('tableaction', { itemId: t, action: r });
		}
		function V(t, r) {
			console.log('cellclick', t, r), L('cellclick', { rowId: t, colId: r });
		}
		function F(t, r) {
			if ((console.log(t, r.checked, D), !r || !t)) return console.error('wrong params');
			r.checked && !D.find((r) => r === t) && D.push(t),
				!r.checked &&
					D.find((r) => r === t) &&
					D.splice(
						D.findIndex((r) => r === t),
						1
					),
				console.log(t, r.checked, D),
				e(20, D);
		}
		function J(t) {
			console.log('action', t, t), L('actiononselected', { key: t, selectedItems: D });
		}
		function R(t) {
			L('clickonrow', { itemId: t });
		}
		function Z(t) {
			console.log(_, M),
				_ && t === _
					? t === _ && 'asc' === M
						? e(13, (M = 'desc'))
						: t === _ && 'desc' === M && (e(13, (M = null)), e(12, (_ = null)))
					: (e(13, (M = 'asc')), e(12, (_ = t))),
				L('changeSort', { sortedBy: _, sortedDirection: M }),
				console.log(_, M);
		}
		function q(t, r) {
			L('showConfirmModal', Object.assign({ action: r }, t)),
				t.show ||
					e(
						14,
						(j = {
							show: 'no',
							itemId: null,
							action: null,
							confirmlabel: null,
							title: null,
							content: null,
							closelabel: null
						})
					);
		}
		function K(t, r) {
			L('showConfirmModalForm', Object.assign({ action: r }, t)),
				t.show ||
					e(
						15,
						(z = {
							show: 'no',
							itemId: null,
							action: null,
							confirmlabel: null,
							title: null,
							content: null,
							closelabel: null
						})
					);
		}
		function Q(t, r) {
			L('confirmActionModal', Object.assign({ action: r }, t));
		}
		function X(t, r) {
			console.log(r, 'action'), L('confirmActionModalForm', Object.assign({}, t, { _action: r }));
		}
		function G(t) {
			navigator.clipboard.writeText(t), L('clipboardCopyText', { text: t });
		}
		W.addComponent({ repoName: '@htmlbricks/hb-paginate', version: P }),
			W.addComponent({ repoName: '@htmlbricks/hb-dialog', version: P }),
			W.addComponent({ repoName: '@htmlbricks/hb-dialogform', version: P });
		return (
			(t.$$set = (t) => {
				'style' in t && e(43, (n = t.style)),
					'id' in t && e(42, (m = t.id)),
					'externalfilter' in t && e(0, (d = t.externalfilter)),
					'rows' in t && e(1, (c = t.rows)),
					'size' in t && e(2, (s = t.size)),
					'page' in t && e(3, (p = t.page)),
					'pages' in t && e(4, (g = t.pages)),
					'headers' in t && e(5, (f = t.headers)),
					'actions' in t && e(6, (b = t.actions)),
					'selectactions' in t && e(7, (u = t.selectactions)),
					'selectrow' in t && e(8, (x = t.selectrow)),
					'enableselect' in t && e(11, (h = t.enableselect)),
					'disablepagination' in t && e(9, (w = t.disablepagination)),
					'add_item' in t && e(10, (y = t.add_item));
			}),
			(t.$$.update = () => {
				if ((63487 & t.$$.dirty[0]) | (61440 & t.$$.dirty[1])) {
					n &&
						(e(44, (o = et(n))),
						e(16, (i = W.getChildStyleToPass(o, null == ot ? void 0 : ot.vars))),
						e(17, (a = W.getChildStyleToPass(o, null == nt ? void 0 : nt.vars))),
						e(18, (l = W.getChildStyleToPass(o, null == it ? void 0 : it.vars)))),
						y || e(10, (y = !1)),
						'string' == typeof y && e(10, (y = 'true' === y || 'yes' === y)),
						j ||
							e(
								14,
								(j = {
									show: 'no',
									itemId: null,
									action: null,
									confirmlabel: null,
									title: null,
									content: null,
									closelabel: null
								})
							),
						z ||
							e(
								15,
								(z = {
									show: 'no',
									itemId: null,
									action: null,
									confirmlabel: null,
									title: null,
									content: null,
									closelabel: null
								})
							),
						console.log(w, 'disablepag'),
						'boolean' != typeof w && e(9, (w = '' === w || 'yes' === w || 'true' === w)),
						console.log(w, 'disablepag2'),
						d || '' === d
							? '' === d || 'yes' === d || 'true' === d
								? e(0, (d = !0))
								: 'no' === d && e(0, (d = !1))
							: e(0, (d = !1)),
						e(4, (g = g ? Number(g) : 1)),
						u ? 'string' == typeof u && e(7, (u = JSON.parse(u))) : e(7, (u = [])),
						x || e(8, (x = null)),
						e(2, (s = s ? parseInt(s) : 12)),
						e(3, (p = p ? parseInt(p) : 0));
					try {
						if (!f) throw new Error('no headers');
						if (!c) throw new Error('no rows');
						if (
							(f &&
								('string' == typeof f && e(5, (f = JSON.parse(f))),
								f.find((t) => 'datetime' === t.type) && e(19, (C = !0)),
								f.forEach((t) => {
									t.sortBy || (t.sortBy = 'none'), t.type || (t.type = 'string');
								})),
							b ? 'string' == typeof b && e(6, (b = JSON.parse(b))) : e(6, (b = null)),
							'string' == typeof c && e(46, ($ = JSON.parse(c))),
							e(1, (c = Object.assign([], $))),
							(null == S ? void 0 : S.length) && !d)
						) {
							console.log('filters', S, c);
							for (const t of S)
								'datetime' === t.type
									? (t.start &&
											e(1, (c = c.filter((r) => U(E(r, t)).valueOf() >= U(t.start).valueOf()))),
									  t.end &&
											e(1, (c = c.filter((r) => U(E(r, t)).valueOf() <= U(t.end).valueOf()))))
									: e(1, (c = c.filter((r) => E(r, t).toString().includes(t.value))));
						}
						_ &&
							!d &&
							(console.log('resort'),
							'asc' === M &&
								e(
									1,
									(c = c.sort((t, r) =>
										(!t[_] && 0 !== t[_]) ||
										(!r[_] && 0 !== r[_]) ||
										'number' != typeof t[_] ||
										'number' != typeof r[_]
											? t === r
												? 0
												: t[_]
												? r[_]
													? t[_].toUpperCase() < r[_].toUpperCase()
														? 1
														: t[_].toUpperCase() > r[_].toUpperCase()
														? -1
														: 0
													: -1
												: 1
											: r[_] - t[_]
									))
								),
							'desc' === M &&
								e(
									1,
									(c = c.sort((t, r) =>
										(!t[_] && 0 !== t[_]) || (!r[_] && 0 !== r[_]) || 'number' != typeof r[_]
											? t === r
												? 0
												: t[_]
												? r[_]
													? t[_].toUpperCase() < r[_].toUpperCase()
														? -1
														: t[_].toUpperCase() > r[_].toUpperCase()
														? 1
														: 0
													: 1
												: -1
											: t[_] - r[_]
									))
								)),
							!c.length ||
								(d && g) ||
								e(4, (g = Math.floor(c.length / s) + (c.length % s ? 1 : 0)));
					} catch (t) {}
					console.log(_, M);
				}
			}),
			[
				d,
				c,
				s,
				p,
				g,
				f,
				b,
				u,
				x,
				w,
				y,
				h,
				_,
				M,
				j,
				z,
				i,
				a,
				l,
				C,
				D,
				function (t) {
					d || e(3, (p = t.detail.page)),
						e(20, (D.length = 0), D),
						L('pageChange', { page: t.detail.page });
				},
				E,
				Y,
				I,
				A,
				H,
				B,
				V,
				F,
				function () {
					h ? (e(11, (h = null)), e(20, (D.length = 0), D)) : e(11, (h = 'yes'));
				},
				J,
				function () {
					e(20, (D = c.map((t) => t._id)));
				},
				function () {
					e(20, (D.length = 0), D);
				},
				R,
				Z,
				q,
				K,
				Q,
				X,
				G,
				function () {
					L('addItem', { id: m });
				},
				m,
				n,
				o,
				S,
				$,
				(t, r) => r === t._id,
				(t) => X(t.detail, z.action),
				(t) => K(t.detail, z.action),
				(t) => Q(t.detail, j.action),
				(t) => q(t.detail, j.action),
				(t) => Z(t.key),
				(t, r) => Y(r.target, t),
				(t, r) => I(r.target, t),
				(t, r) => Y(r.target, t),
				(t, r) => Y(r.target, t),
				(t, r) => A(r.target, t),
				(t, r) => {
					F(t._id, r.target);
				},
				(t, r) => {
					F(t._id, r.target);
				},
				(t, r) => H(t, r),
				(t, r) => H(t, r),
				(t, r) => H(t, r),
				(t, r) => H(t, r),
				(t, r) => V(t._id, r.key),
				(t, r) => G(E(t, r).toString()),
				(t) => {
					x && R(t._id);
				},
				(t, r) => B(t._id, r.name),
				(t, r) => B(t._id, r.name),
				(t) => {
					J(t.name);
				}
			]
		);
	}
	class zr extends V {
		constructor(t) {
			super();
			const r = document.createElement('style');
			(r.textContent =
				"@import url(\"https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap\");@import url(\"/npm/bootstrap-icons.css\");:host{--bs-blue:var(--bs-primary,#07689f);--bs-indigo:#6610f2;--bs-purple:#6f42c1;--bs-pink:#d63384;--bs-red:var(--bs-danger,#f67280);--bs-orange:#fd7e14;--bs-yellow:#ffc107;--bs-green:var(--bs-warning,#ffc107);--bs-teal:#20c997;--bs-cyan:var(--bs-info,#a2d5f2);--bs-white:#fff;--bs-gray:var(--bs-secondary,#c9d6df);--bs-gray-dark:#343a40;--bs-gray-100:#f8f9fa;--bs-gray-200:#e9ecef;--bs-gray-300:#dee2e6;--bs-gray-400:#ced4da;--bs-gray-500:#adb5bd;--bs-gray-600:var(--bs-secondary,#c9d6df);--bs-gray-700:#495057;--bs-gray-800:#343a40;--bs-gray-900:#212529;--bs-primary:var(--bs-primary,#07689f);--bs-secondary:var(--bs-secondary,#c9d6df);--bs-success:var(--bs-warning,#ffc107);--bs-info:var(--bs-info,#a2d5f2);--bs-warning:#ffc107;--bs-danger:var(--bs-danger,#f67280);--bs-light:#f8f9fa;--bs-dark:#212529;--bs-primary-rgb:13, 110, 253;--bs-secondary-rgb:108, 117, 125;--bs-success-rgb:25, 135, 84;--bs-info-rgb:13, 202, 240;--bs-warning-rgb:255, 193, 7;--bs-danger-rgb:220, 53, 69;--bs-light-rgb:248, 249, 250;--bs-dark-rgb:33, 37, 41;--bs-white-rgb:255, 255, 255;--bs-black-rgb:0, 0, 0;--bs-body-color-rgb:33, 37, 41;--bs-body-bg-rgb:255, 255, 255;--bs-font-sans-serif:system-ui, -apple-system, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";--bs-font-monospace:SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;--bs-gradient:linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));--bs-body-font-family:var(--bs-font-sans-serif);--bs-body-font-size:1rem;--bs-body-font-weight:400;--bs-body-line-height:1.5;--bs-body-color:#212529;--bs-body-bg:#fff}*,*::before,*::after{box-sizing:border-box}@media(prefers-reduced-motion: no-preference){:host{scroll-behavior:smooth}}@media(min-width: 1200px){}@media(min-width: 1200px){}@media(min-width: 1200px){}@media(min-width: 1200px){}table{caption-side:bottom;border-collapse:collapse}th{text-align:inherit;text-align:-webkit-match-parent}thead,tbody,tr,td,th{border-color:inherit;border-style:solid;border-width:0}button{border-radius:0}button:focus:not(:focus-visible){outline:0}input,button,select{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,select{text-transform:none}select{word-wrap:normal}select:disabled{opacity:1}button,[type=button]{-webkit-appearance:button}button:not(:disabled),[type=button]:not(:disabled){cursor:pointer}::-moz-focus-inner{padding:0;border-style:none}@media(min-width: 1200px){}::-webkit-datetime-edit-fields-wrapper,::-webkit-datetime-edit-text,::-webkit-datetime-edit-minute,::-webkit-datetime-edit-hour-field,::-webkit-datetime-edit-day-field,::-webkit-datetime-edit-month-field,::-webkit-datetime-edit-year-field{padding:0}::-webkit-inner-spin-button{height:auto}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-color-swatch-wrapper{padding:0}::file-selector-button{font:inherit}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}.container,.container-fluid,.container-xxl,.container-xl,.container-lg,.container-md,.container-sm{width:100%;padding-right:var(--bs-gutter-x, 0.75rem);padding-left:var(--bs-gutter-x, 0.75rem);margin-right:auto;margin-left:auto}@media(min-width: 576px){.container-sm,.container{max-width:540px}}@media(min-width: 768px){.container-md,.container-sm,.container{max-width:720px}}@media(min-width: 992px){.container-lg,.container-md,.container-sm,.container{max-width:960px}}@media(min-width: 1200px){.container-xl,.container-lg,.container-md,.container-sm,.container{max-width:1140px}}@media(min-width: 1400px){.container-xxl,.container-xl,.container-lg,.container-md,.container-sm,.container{max-width:1320px}}.row{--bs-gutter-x:1.5rem;--bs-gutter-y:0;display:flex;flex-wrap:wrap;margin-top:calc(-1 * var(--bs-gutter-y));margin-right:calc(-0.5 * var(--bs-gutter-x));margin-left:calc(-0.5 * var(--bs-gutter-x))}.row>*{flex-shrink:0;width:100%;max-width:100%;padding-right:calc(var(--bs-gutter-x) * 0.5);padding-left:calc(var(--bs-gutter-x) * 0.5);margin-top:var(--bs-gutter-y)}.col{flex:1 0 0%}.row-cols-auto>*{flex:0 0 auto;width:auto}.row-cols-1>*{flex:0 0 auto;width:100%}.row-cols-2>*{flex:0 0 auto;width:50%}.row-cols-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-4>*{flex:0 0 auto;width:25%}.row-cols-5>*{flex:0 0 auto;width:20%}.row-cols-6>*{flex:0 0 auto;width:16.6666666667%}.col-auto{flex:0 0 auto;width:auto}.col-1{flex:0 0 auto;width:8.33333333%}.col-2{flex:0 0 auto;width:16.66666667%}.col-3{flex:0 0 auto;width:25%}.col-4{flex:0 0 auto;width:33.33333333%}.col-5{flex:0 0 auto;width:41.66666667%}.col-6{flex:0 0 auto;width:50%}.col-7{flex:0 0 auto;width:58.33333333%}.col-8{flex:0 0 auto;width:66.66666667%}.col-9{flex:0 0 auto;width:75%}.col-10{flex:0 0 auto;width:83.33333333%}.col-11{flex:0 0 auto;width:91.66666667%}.col-12{flex:0 0 auto;width:100%}.offset-1{margin-left:8.33333333%}.offset-2{margin-left:16.66666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.33333333%}.offset-5{margin-left:41.66666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.33333333%}.offset-8{margin-left:66.66666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.33333333%}.offset-11{margin-left:91.66666667%}.g-0,.gx-0{--bs-gutter-x:0}.g-0,.gy-0{--bs-gutter-y:0}.g-1,.gx-1{--bs-gutter-x:0.25rem}.g-1,.gy-1{--bs-gutter-y:0.25rem}.g-2,.gx-2{--bs-gutter-x:0.5rem}.g-2,.gy-2{--bs-gutter-y:0.5rem}.g-3,.gx-3{--bs-gutter-x:1rem}.g-3,.gy-3{--bs-gutter-y:1rem}.g-4,.gx-4{--bs-gutter-x:1.5rem}.g-4,.gy-4{--bs-gutter-y:1.5rem}.g-5,.gx-5{--bs-gutter-x:3rem}.g-5,.gy-5{--bs-gutter-y:3rem}@media(min-width: 576px){.col-sm{flex:1 0 0%}.row-cols-sm-auto>*{flex:0 0 auto;width:auto}.row-cols-sm-1>*{flex:0 0 auto;width:100%}.row-cols-sm-2>*{flex:0 0 auto;width:50%}.row-cols-sm-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-sm-4>*{flex:0 0 auto;width:25%}.row-cols-sm-5>*{flex:0 0 auto;width:20%}.row-cols-sm-6>*{flex:0 0 auto;width:16.6666666667%}.col-sm-auto{flex:0 0 auto;width:auto}.col-sm-1{flex:0 0 auto;width:8.33333333%}.col-sm-2{flex:0 0 auto;width:16.66666667%}.col-sm-3{flex:0 0 auto;width:25%}.col-sm-4{flex:0 0 auto;width:33.33333333%}.col-sm-5{flex:0 0 auto;width:41.66666667%}.col-sm-6{flex:0 0 auto;width:50%}.col-sm-7{flex:0 0 auto;width:58.33333333%}.col-sm-8{flex:0 0 auto;width:66.66666667%}.col-sm-9{flex:0 0 auto;width:75%}.col-sm-10{flex:0 0 auto;width:83.33333333%}.col-sm-11{flex:0 0 auto;width:91.66666667%}.col-sm-12{flex:0 0 auto;width:100%}.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.33333333%}.offset-sm-2{margin-left:16.66666667%}.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.33333333%}.offset-sm-5{margin-left:41.66666667%}.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.33333333%}.offset-sm-8{margin-left:66.66666667%}.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.33333333%}.offset-sm-11{margin-left:91.66666667%}.g-sm-0,.gx-sm-0{--bs-gutter-x:0}.g-sm-0,.gy-sm-0{--bs-gutter-y:0}.g-sm-1,.gx-sm-1{--bs-gutter-x:0.25rem}.g-sm-1,.gy-sm-1{--bs-gutter-y:0.25rem}.g-sm-2,.gx-sm-2{--bs-gutter-x:0.5rem}.g-sm-2,.gy-sm-2{--bs-gutter-y:0.5rem}.g-sm-3,.gx-sm-3{--bs-gutter-x:1rem}.g-sm-3,.gy-sm-3{--bs-gutter-y:1rem}.g-sm-4,.gx-sm-4{--bs-gutter-x:1.5rem}.g-sm-4,.gy-sm-4{--bs-gutter-y:1.5rem}.g-sm-5,.gx-sm-5{--bs-gutter-x:3rem}.g-sm-5,.gy-sm-5{--bs-gutter-y:3rem}}@media(min-width: 768px){.col-md{flex:1 0 0%}.row-cols-md-auto>*{flex:0 0 auto;width:auto}.row-cols-md-1>*{flex:0 0 auto;width:100%}.row-cols-md-2>*{flex:0 0 auto;width:50%}.row-cols-md-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-md-4>*{flex:0 0 auto;width:25%}.row-cols-md-5>*{flex:0 0 auto;width:20%}.row-cols-md-6>*{flex:0 0 auto;width:16.6666666667%}.col-md-auto{flex:0 0 auto;width:auto}.col-md-1{flex:0 0 auto;width:8.33333333%}.col-md-2{flex:0 0 auto;width:16.66666667%}.col-md-3{flex:0 0 auto;width:25%}.col-md-4{flex:0 0 auto;width:33.33333333%}.col-md-5{flex:0 0 auto;width:41.66666667%}.col-md-6{flex:0 0 auto;width:50%}.col-md-7{flex:0 0 auto;width:58.33333333%}.col-md-8{flex:0 0 auto;width:66.66666667%}.col-md-9{flex:0 0 auto;width:75%}.col-md-10{flex:0 0 auto;width:83.33333333%}.col-md-11{flex:0 0 auto;width:91.66666667%}.col-md-12{flex:0 0 auto;width:100%}.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.33333333%}.offset-md-2{margin-left:16.66666667%}.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.33333333%}.offset-md-5{margin-left:41.66666667%}.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.33333333%}.offset-md-8{margin-left:66.66666667%}.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.33333333%}.offset-md-11{margin-left:91.66666667%}.g-md-0,.gx-md-0{--bs-gutter-x:0}.g-md-0,.gy-md-0{--bs-gutter-y:0}.g-md-1,.gx-md-1{--bs-gutter-x:0.25rem}.g-md-1,.gy-md-1{--bs-gutter-y:0.25rem}.g-md-2,.gx-md-2{--bs-gutter-x:0.5rem}.g-md-2,.gy-md-2{--bs-gutter-y:0.5rem}.g-md-3,.gx-md-3{--bs-gutter-x:1rem}.g-md-3,.gy-md-3{--bs-gutter-y:1rem}.g-md-4,.gx-md-4{--bs-gutter-x:1.5rem}.g-md-4,.gy-md-4{--bs-gutter-y:1.5rem}.g-md-5,.gx-md-5{--bs-gutter-x:3rem}.g-md-5,.gy-md-5{--bs-gutter-y:3rem}}@media(min-width: 992px){.col-lg{flex:1 0 0%}.row-cols-lg-auto>*{flex:0 0 auto;width:auto}.row-cols-lg-1>*{flex:0 0 auto;width:100%}.row-cols-lg-2>*{flex:0 0 auto;width:50%}.row-cols-lg-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-lg-4>*{flex:0 0 auto;width:25%}.row-cols-lg-5>*{flex:0 0 auto;width:20%}.row-cols-lg-6>*{flex:0 0 auto;width:16.6666666667%}.col-lg-auto{flex:0 0 auto;width:auto}.col-lg-1{flex:0 0 auto;width:8.33333333%}.col-lg-2{flex:0 0 auto;width:16.66666667%}.col-lg-3{flex:0 0 auto;width:25%}.col-lg-4{flex:0 0 auto;width:33.33333333%}.col-lg-5{flex:0 0 auto;width:41.66666667%}.col-lg-6{flex:0 0 auto;width:50%}.col-lg-7{flex:0 0 auto;width:58.33333333%}.col-lg-8{flex:0 0 auto;width:66.66666667%}.col-lg-9{flex:0 0 auto;width:75%}.col-lg-10{flex:0 0 auto;width:83.33333333%}.col-lg-11{flex:0 0 auto;width:91.66666667%}.col-lg-12{flex:0 0 auto;width:100%}.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.33333333%}.offset-lg-2{margin-left:16.66666667%}.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.33333333%}.offset-lg-5{margin-left:41.66666667%}.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.33333333%}.offset-lg-8{margin-left:66.66666667%}.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.33333333%}.offset-lg-11{margin-left:91.66666667%}.g-lg-0,.gx-lg-0{--bs-gutter-x:0}.g-lg-0,.gy-lg-0{--bs-gutter-y:0}.g-lg-1,.gx-lg-1{--bs-gutter-x:0.25rem}.g-lg-1,.gy-lg-1{--bs-gutter-y:0.25rem}.g-lg-2,.gx-lg-2{--bs-gutter-x:0.5rem}.g-lg-2,.gy-lg-2{--bs-gutter-y:0.5rem}.g-lg-3,.gx-lg-3{--bs-gutter-x:1rem}.g-lg-3,.gy-lg-3{--bs-gutter-y:1rem}.g-lg-4,.gx-lg-4{--bs-gutter-x:1.5rem}.g-lg-4,.gy-lg-4{--bs-gutter-y:1.5rem}.g-lg-5,.gx-lg-5{--bs-gutter-x:3rem}.g-lg-5,.gy-lg-5{--bs-gutter-y:3rem}}@media(min-width: 1200px){.col-xl{flex:1 0 0%}.row-cols-xl-auto>*{flex:0 0 auto;width:auto}.row-cols-xl-1>*{flex:0 0 auto;width:100%}.row-cols-xl-2>*{flex:0 0 auto;width:50%}.row-cols-xl-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-xl-4>*{flex:0 0 auto;width:25%}.row-cols-xl-5>*{flex:0 0 auto;width:20%}.row-cols-xl-6>*{flex:0 0 auto;width:16.6666666667%}.col-xl-auto{flex:0 0 auto;width:auto}.col-xl-1{flex:0 0 auto;width:8.33333333%}.col-xl-2{flex:0 0 auto;width:16.66666667%}.col-xl-3{flex:0 0 auto;width:25%}.col-xl-4{flex:0 0 auto;width:33.33333333%}.col-xl-5{flex:0 0 auto;width:41.66666667%}.col-xl-6{flex:0 0 auto;width:50%}.col-xl-7{flex:0 0 auto;width:58.33333333%}.col-xl-8{flex:0 0 auto;width:66.66666667%}.col-xl-9{flex:0 0 auto;width:75%}.col-xl-10{flex:0 0 auto;width:83.33333333%}.col-xl-11{flex:0 0 auto;width:91.66666667%}.col-xl-12{flex:0 0 auto;width:100%}.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.33333333%}.offset-xl-2{margin-left:16.66666667%}.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.33333333%}.offset-xl-5{margin-left:41.66666667%}.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.33333333%}.offset-xl-8{margin-left:66.66666667%}.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.33333333%}.offset-xl-11{margin-left:91.66666667%}.g-xl-0,.gx-xl-0{--bs-gutter-x:0}.g-xl-0,.gy-xl-0{--bs-gutter-y:0}.g-xl-1,.gx-xl-1{--bs-gutter-x:0.25rem}.g-xl-1,.gy-xl-1{--bs-gutter-y:0.25rem}.g-xl-2,.gx-xl-2{--bs-gutter-x:0.5rem}.g-xl-2,.gy-xl-2{--bs-gutter-y:0.5rem}.g-xl-3,.gx-xl-3{--bs-gutter-x:1rem}.g-xl-3,.gy-xl-3{--bs-gutter-y:1rem}.g-xl-4,.gx-xl-4{--bs-gutter-x:1.5rem}.g-xl-4,.gy-xl-4{--bs-gutter-y:1.5rem}.g-xl-5,.gx-xl-5{--bs-gutter-x:3rem}.g-xl-5,.gy-xl-5{--bs-gutter-y:3rem}}@media(min-width: 1400px){.col-xxl{flex:1 0 0%}.row-cols-xxl-auto>*{flex:0 0 auto;width:auto}.row-cols-xxl-1>*{flex:0 0 auto;width:100%}.row-cols-xxl-2>*{flex:0 0 auto;width:50%}.row-cols-xxl-3>*{flex:0 0 auto;width:33.3333333333%}.row-cols-xxl-4>*{flex:0 0 auto;width:25%}.row-cols-xxl-5>*{flex:0 0 auto;width:20%}.row-cols-xxl-6>*{flex:0 0 auto;width:16.6666666667%}.col-xxl-auto{flex:0 0 auto;width:auto}.col-xxl-1{flex:0 0 auto;width:8.33333333%}.col-xxl-2{flex:0 0 auto;width:16.66666667%}.col-xxl-3{flex:0 0 auto;width:25%}.col-xxl-4{flex:0 0 auto;width:33.33333333%}.col-xxl-5{flex:0 0 auto;width:41.66666667%}.col-xxl-6{flex:0 0 auto;width:50%}.col-xxl-7{flex:0 0 auto;width:58.33333333%}.col-xxl-8{flex:0 0 auto;width:66.66666667%}.col-xxl-9{flex:0 0 auto;width:75%}.col-xxl-10{flex:0 0 auto;width:83.33333333%}.col-xxl-11{flex:0 0 auto;width:91.66666667%}.col-xxl-12{flex:0 0 auto;width:100%}.offset-xxl-0{margin-left:0}.offset-xxl-1{margin-left:8.33333333%}.offset-xxl-2{margin-left:16.66666667%}.offset-xxl-3{margin-left:25%}.offset-xxl-4{margin-left:33.33333333%}.offset-xxl-5{margin-left:41.66666667%}.offset-xxl-6{margin-left:50%}.offset-xxl-7{margin-left:58.33333333%}.offset-xxl-8{margin-left:66.66666667%}.offset-xxl-9{margin-left:75%}.offset-xxl-10{margin-left:83.33333333%}.offset-xxl-11{margin-left:91.66666667%}.g-xxl-0,.gx-xxl-0{--bs-gutter-x:0}.g-xxl-0,.gy-xxl-0{--bs-gutter-y:0}.g-xxl-1,.gx-xxl-1{--bs-gutter-x:0.25rem}.g-xxl-1,.gy-xxl-1{--bs-gutter-y:0.25rem}.g-xxl-2,.gx-xxl-2{--bs-gutter-x:0.5rem}.g-xxl-2,.gy-xxl-2{--bs-gutter-y:0.5rem}.g-xxl-3,.gx-xxl-3{--bs-gutter-x:1rem}.g-xxl-3,.gy-xxl-3{--bs-gutter-y:1rem}.g-xxl-4,.gx-xxl-4{--bs-gutter-x:1.5rem}.g-xxl-4,.gy-xxl-4{--bs-gutter-y:1.5rem}.g-xxl-5,.gx-xxl-5{--bs-gutter-x:3rem}.g-xxl-5,.gy-xxl-5{--bs-gutter-y:3rem}}.form-label{margin-bottom:0.5rem}.col-form-label{padding-top:calc(0.375rem + 1px);padding-bottom:calc(0.375rem + 1px);margin-bottom:0;font-size:inherit;line-height:1.5}.col-form-label-lg{padding-top:calc(0.5rem + 1px);padding-bottom:calc(0.5rem + 1px);font-size:1.25rem}.col-form-label-sm{padding-top:calc(0.25rem + 1px);padding-bottom:calc(0.25rem + 1px);font-size:0.875rem}.form-text{margin-top:0.25rem;font-size:0.875em;color:var(--bs-secondary,#c9d6df)}.form-control{display:block;width:100%;padding:0.375rem 0.75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;appearance:none;border-radius:0.25rem;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control{transition:none}}.form-control:focus{color:#212529;background-color:#fff;border-color:#86b7fe;outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-control::-webkit-date-and-time-value{height:1.5em}.form-control::placeholder{color:var(--bs-secondary,#c9d6df);opacity:1}.form-control:disabled{background-color:#e9ecef;opacity:1}.form-control::file-selector-button{padding:0.375rem 0.75rem;margin:-0.375rem -0.75rem;margin-inline-end:0.75rem;color:#212529;background-color:#e9ecef;pointer-events:none;border-color:inherit;border-style:solid;border-width:0;border-inline-end-width:1px;border-radius:0;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control::file-selector-button{transition:none}}.form-control:hover:not(:disabled):not([readonly])::file-selector-button{background-color:#dde0e3}.form-control::-webkit-file-upload-button{padding:0.375rem 0.75rem;margin:-0.375rem -0.75rem;margin-inline-end:0.75rem;color:#212529;background-color:#e9ecef;pointer-events:none;border-color:inherit;border-style:solid;border-width:0;border-inline-end-width:1px;border-radius:0;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-control::-webkit-file-upload-button{transition:none}}.form-control:hover:not(:disabled):not([readonly])::-webkit-file-upload-button{background-color:#dde0e3}.form-control-plaintext{display:block;width:100%;padding:0.375rem 0;margin-bottom:0;line-height:1.5;color:#212529;background-color:transparent;border:solid transparent;border-width:1px 0}.form-control-plaintext.form-control-sm,.form-control-plaintext.form-control-lg{padding-right:0;padding-left:0}.form-control-sm{min-height:calc(1.5em + 0.5rem + 2px);padding:0.25rem 0.5rem;font-size:0.875rem;border-radius:0.2rem}.form-control-sm::file-selector-button{padding:0.25rem 0.5rem;margin:-0.25rem -0.5rem;margin-inline-end:0.5rem}.form-control-sm::-webkit-file-upload-button{padding:0.25rem 0.5rem;margin:-0.25rem -0.5rem;margin-inline-end:0.5rem}.form-control-lg{min-height:calc(1.5em + 1rem + 2px);padding:0.5rem 1rem;font-size:1.25rem;border-radius:0.3rem}.form-control-lg::file-selector-button{padding:0.5rem 1rem;margin:-0.5rem -1rem;margin-inline-end:1rem}.form-control-lg::-webkit-file-upload-button{padding:0.5rem 1rem;margin:-0.5rem -1rem;margin-inline-end:1rem}.form-control-color{width:3rem;height:auto;padding:0.375rem}.form-control-color:not(:disabled):not([readonly]){cursor:pointer}.form-control-color::-moz-color-swatch{height:1.5em;border-radius:0.25rem}.form-control-color::-webkit-color-swatch{height:1.5em;border-radius:0.25rem}.form-select{display:block;width:100%;padding:0.375rem 2.25rem 0.375rem 0.75rem;-moz-padding-start:calc(0.75rem - 3px);font-size:1rem;font-weight:400;line-height:1.5;color:#212529;background-color:#fff;background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e\");background-repeat:no-repeat;background-position:right 0.75rem center;background-size:16px 12px;border:1px solid #ced4da;border-radius:0.25rem;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;appearance:none}@media(prefers-reduced-motion: reduce){.form-select{transition:none}}.form-select:focus{border-color:#86b7fe;outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-select:disabled{background-color:#e9ecef}.form-select:-moz-focusring{color:transparent;text-shadow:0 0 0 #212529}.form-select-sm{padding-top:0.25rem;padding-bottom:0.25rem;padding-left:0.5rem;font-size:0.875rem;border-radius:0.2rem}.form-select-lg{padding-top:0.5rem;padding-bottom:0.5rem;padding-left:1rem;font-size:1.25rem;border-radius:0.3rem}.form-check{display:block;min-height:1.5rem;padding-left:1.5em;margin-bottom:0.125rem}.form-check .form-check-input{float:left;margin-left:-1.5em}.form-check-input{width:1em;height:1em;margin-top:0.25em;vertical-align:top;background-color:#fff;background-repeat:no-repeat;background-position:center;background-size:contain;border:1px solid rgba(0, 0, 0, 0.25);appearance:none;color-adjust:exact}.form-check-input[type=checkbox]{border-radius:0.25em}.form-check-input:active{filter:brightness(90%)}.form-check-input:focus{border-color:#86b7fe;outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-check-input:checked{background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.form-check-input:checked[type=checkbox]{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10l3 3l6-6'/%3e%3c/svg%3e\")}.form-check-input[type=checkbox]:indeterminate{background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f);background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10h8'/%3e%3c/svg%3e\")}.form-check-input:disabled{pointer-events:none;filter:none;opacity:0.5}.form-check-input[disabled]~.form-check-label,.form-check-input:disabled~.form-check-label{opacity:0.5}.form-switch{padding-left:2.5em}.form-switch .form-check-input{width:2em;margin-left:-2.5em;background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='rgba%280, 0, 0, 0.25%29'/%3e%3c/svg%3e\");background-position:left center;border-radius:2em;transition:background-position 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.form-switch .form-check-input{transition:none}}.form-switch .form-check-input:focus{background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%2386b7fe'/%3e%3c/svg%3e\")}.form-switch .form-check-input:checked{background-position:right center;background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%23fff'/%3e%3c/svg%3e\")}.form-check-inline{display:inline-block;margin-right:1rem}.btn-check{position:absolute;clip:rect(0, 0, 0, 0);pointer-events:none}.btn-check[disabled]+.btn,.btn-check:disabled+.btn{pointer-events:none;filter:none;opacity:0.65}.form-range{width:100%;height:1.5rem;padding:0;background-color:transparent;appearance:none}.form-range:focus{outline:0}.form-range:focus::-webkit-slider-thumb{box-shadow:0 0 0 1px #fff, 0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-range:focus::-moz-range-thumb{box-shadow:0 0 0 1px #fff, 0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.form-range::-moz-focus-outer{border:0}.form-range::-webkit-slider-thumb{width:1rem;height:1rem;margin-top:-0.25rem;background-color:var(--bs-primary,#07689f);border:0;border-radius:1rem;transition:background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;appearance:none}@media(prefers-reduced-motion: reduce){.form-range::-webkit-slider-thumb{transition:none}}.form-range::-webkit-slider-thumb:active{background-color:#b6d4fe}.form-range::-webkit-slider-runnable-track{width:100%;height:0.5rem;color:transparent;cursor:pointer;background-color:#dee2e6;border-color:transparent;border-radius:1rem}.form-range::-moz-range-thumb{width:1rem;height:1rem;background-color:var(--bs-primary,#07689f);border:0;border-radius:1rem;transition:background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;appearance:none}@media(prefers-reduced-motion: reduce){.form-range::-moz-range-thumb{transition:none}}.form-range::-moz-range-thumb:active{background-color:#b6d4fe}.form-range::-moz-range-track{width:100%;height:0.5rem;color:transparent;cursor:pointer;background-color:#dee2e6;border-color:transparent;border-radius:1rem}.form-range:disabled{pointer-events:none}.form-range:disabled::-webkit-slider-thumb{background-color:#adb5bd}.form-range:disabled::-moz-range-thumb{background-color:#adb5bd}.form-floating{position:relative}.form-floating>.form-control,.form-floating>.form-select{height:calc(3.5rem + 2px);line-height:1.25}@media(prefers-reduced-motion: reduce){}.form-floating>.form-control{padding:1rem 0.75rem}.form-floating>.form-control::placeholder{color:transparent}.form-floating>.form-control:focus,.form-floating>.form-control:not(:placeholder-shown){padding-top:1.625rem;padding-bottom:0.625rem}.form-floating>.form-control:-webkit-autofill{padding-top:1.625rem;padding-bottom:0.625rem}.form-floating>.form-select{padding-top:1.625rem;padding-bottom:0.625rem}.input-group{position:relative;display:flex;flex-wrap:wrap;align-items:stretch;width:100%}.input-group>.form-control,.input-group>.form-select{position:relative;flex:1 1 auto;width:1%;min-width:0}.input-group>.form-control:focus,.input-group>.form-select:focus{z-index:3}.input-group .btn{position:relative;z-index:2}.input-group .btn:focus{z-index:3}.input-group-text{display:flex;align-items:center;padding:0.375rem 0.75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:center;white-space:nowrap;background-color:#e9ecef;border:1px solid #ced4da;border-radius:0.25rem}.input-group-lg>.form-control,.input-group-lg>.form-select,.input-group-lg>.input-group-text,.input-group-lg>.btn{padding:0.5rem 1rem;font-size:1.25rem;border-radius:0.3rem}.input-group-sm>.form-control,.input-group-sm>.form-select,.input-group-sm>.input-group-text,.input-group-sm>.btn{padding:0.25rem 0.5rem;font-size:0.875rem;border-radius:0.2rem}.input-group-lg>.form-select,.input-group-sm>.form-select{padding-right:3rem}.input-group:not(.has-validation)>:not(:last-child):not(.dropdown-toggle):not(.dropdown-menu),.input-group:not(.has-validation)>.dropdown-toggle:nth-last-child(n+3){border-top-right-radius:0;border-bottom-right-radius:0}.input-group.has-validation>:nth-last-child(n+3):not(.dropdown-toggle):not(.dropdown-menu),.input-group.has-validation>.dropdown-toggle:nth-last-child(n+4){border-top-right-radius:0;border-bottom-right-radius:0}.input-group>:not(:first-child):not(.dropdown-menu):not(.valid-tooltip):not(.valid-feedback):not(.invalid-tooltip):not(.invalid-feedback){margin-left:-1px;border-top-left-radius:0;border-bottom-left-radius:0}.valid-feedback{display:none;width:100%;margin-top:0.25rem;font-size:0.875em;color:var(--bs-warning,#ffc107)}.valid-tooltip{position:absolute;top:100%;z-index:5;display:none;max-width:100%;padding:0.25rem 0.5rem;margin-top:0.1rem;font-size:0.875rem;color:#fff;background-color:rgba(25, 135, 84, 0.9);border-radius:0.25rem}.is-valid~.valid-feedback,.is-valid~.valid-tooltip{display:block}.was-validated .form-control:valid,.form-control.is-valid{border-color:var(--bs-warning,#ffc107);padding-right:calc(1.5em + 0.75rem);background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23198754' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e\");background-repeat:no-repeat;background-position:right calc(0.375em + 0.1875rem) center;background-size:calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.was-validated .form-control:valid:focus,.form-control.is-valid:focus{border-color:var(--bs-warning,#ffc107);box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.25)}.was-validated .form-select:valid,.form-select.is-valid{border-color:var(--bs-warning,#ffc107)}.was-validated .form-select:valid:not([multiple]):not([size]),.form-select.is-valid:not([multiple]):not([size]){padding-right:4.125rem;background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e\"), url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23198754' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e\");background-position:right 0.75rem center, center right 2.25rem;background-size:16px 12px, calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.was-validated .form-select:valid:focus,.form-select.is-valid:focus{border-color:var(--bs-warning,#ffc107);box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.25)}.was-validated .form-check-input:valid,.form-check-input.is-valid{border-color:var(--bs-warning,#ffc107)}.was-validated .form-check-input:valid:checked,.form-check-input.is-valid:checked{background-color:var(--bs-warning,#ffc107)}.was-validated .form-check-input:valid:focus,.form-check-input.is-valid:focus{box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.25)}.form-check-input.is-valid~.form-check-label{color:var(--bs-warning,#ffc107)}.was-validated .input-group .form-control:valid,.input-group .form-control.is-valid,.was-validated .input-group .form-select:valid,.input-group .form-select.is-valid{z-index:1}.was-validated .input-group .form-control:valid:focus,.input-group .form-control.is-valid:focus,.was-validated .input-group .form-select:valid:focus,.input-group .form-select.is-valid:focus{z-index:3}.invalid-feedback{display:none;width:100%;margin-top:0.25rem;font-size:0.875em;color:var(--bs-danger,#f67280)}.invalid-tooltip{position:absolute;top:100%;z-index:5;display:none;max-width:100%;padding:0.25rem 0.5rem;margin-top:0.1rem;font-size:0.875rem;color:#fff;background-color:rgba(220, 53, 69, 0.9);border-radius:0.25rem}.is-invalid~.invalid-feedback,.is-invalid~.invalid-tooltip{display:block}.was-validated .form-control:invalid,.form-control.is-invalid{border-color:var(--bs-danger,#f67280);padding-right:calc(1.5em + 0.75rem);background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e\");background-repeat:no-repeat;background-position:right calc(0.375em + 0.1875rem) center;background-size:calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.was-validated .form-control:invalid:focus,.form-control.is-invalid:focus{border-color:var(--bs-danger,#f67280);box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.25)}.was-validated .form-select:invalid,.form-select.is-invalid{border-color:var(--bs-danger,#f67280)}.was-validated .form-select:invalid:not([multiple]):not([size]),.form-select.is-invalid:not([multiple]):not([size]){padding-right:4.125rem;background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e\"), url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e\");background-position:right 0.75rem center, center right 2.25rem;background-size:16px 12px, calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)}.was-validated .form-select:invalid:focus,.form-select.is-invalid:focus{border-color:var(--bs-danger,#f67280);box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.25)}.was-validated .form-check-input:invalid,.form-check-input.is-invalid{border-color:var(--bs-danger,#f67280)}.was-validated .form-check-input:invalid:checked,.form-check-input.is-invalid:checked{background-color:var(--bs-danger,#f67280)}.was-validated .form-check-input:invalid:focus,.form-check-input.is-invalid:focus{box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.25)}.form-check-input.is-invalid~.form-check-label{color:var(--bs-danger,#f67280)}.was-validated .input-group .form-control:invalid,.input-group .form-control.is-invalid,.was-validated .input-group .form-select:invalid,.input-group .form-select.is-invalid{z-index:2}.was-validated .input-group .form-control:invalid:focus,.input-group .form-control.is-invalid:focus,.was-validated .input-group .form-select:invalid:focus,.input-group .form-select.is-invalid:focus{z-index:3}.table{--bs-table-bg:transparent;--bs-table-accent-bg:transparent;--bs-table-striped-color:#212529;--bs-table-striped-bg:rgba(0, 0, 0, 0.05);--bs-table-active-color:#212529;--bs-table-active-bg:rgba(0, 0, 0, 0.1);--bs-table-hover-color:#212529;--bs-table-hover-bg:rgba(0, 0, 0, 0.075);width:100%;margin-bottom:1rem;color:#212529;vertical-align:top;border-color:#dee2e6}.table>:not(caption)>*>*{padding:0.5rem 0.5rem;background-color:var(--bs-table-bg);border-bottom-width:1px;box-shadow:inset 0 0 0 9999px var(--bs-table-accent-bg)}.table>tbody{vertical-align:inherit}.table>thead{vertical-align:bottom}.table>:not(:first-child){border-top:2px solid currentColor}.caption-top{caption-side:top}.table-borderless>:not(:first-child){border-top-width:0}.table-striped>tbody>tr:nth-of-type(odd)>*{--bs-table-accent-bg:var(--bs-table-striped-bg);color:var(--bs-table-striped-color)}.table-active{--bs-table-accent-bg:var(--bs-table-active-bg);color:var(--bs-table-active-color)}.table-hover>tbody>tr:hover>*{--bs-table-accent-bg:var(--bs-table-hover-bg);color:var(--bs-table-hover-color)}.table-primary{--bs-table-bg:#cfe2ff;--bs-table-striped-bg:#c5d7f2;--bs-table-striped-color:#000;--bs-table-active-bg:#bacbe6;--bs-table-active-color:#000;--bs-table-hover-bg:#bfd1ec;--bs-table-hover-color:#000;color:#000;border-color:#bacbe6}.table-secondary{--bs-table-bg:#e2e3e5;--bs-table-striped-bg:#d7d8da;--bs-table-striped-color:#000;--bs-table-active-bg:#cbccce;--bs-table-active-color:#000;--bs-table-hover-bg:#d1d2d4;--bs-table-hover-color:#000;color:#000;border-color:#cbccce}.table-success{--bs-table-bg:#d1e7dd;--bs-table-striped-bg:#c7dbd2;--bs-table-striped-color:#000;--bs-table-active-bg:#bcd0c7;--bs-table-active-color:#000;--bs-table-hover-bg:#c1d6cc;--bs-table-hover-color:#000;color:#000;border-color:#bcd0c7}.table-info{--bs-table-bg:#cff4fc;--bs-table-striped-bg:#c5e8ef;--bs-table-striped-color:#000;--bs-table-active-bg:#badce3;--bs-table-active-color:#000;--bs-table-hover-bg:#bfe2e9;--bs-table-hover-color:#000;color:#000;border-color:#badce3}.table-warning{--bs-table-bg:#fff3cd;--bs-table-striped-bg:#f2e7c3;--bs-table-striped-color:#000;--bs-table-active-bg:#e6dbb9;--bs-table-active-color:#000;--bs-table-hover-bg:#ece1be;--bs-table-hover-color:#000;color:#000;border-color:#e6dbb9}.table-danger{--bs-table-bg:#f8d7da;--bs-table-striped-bg:#eccccf;--bs-table-striped-color:#000;--bs-table-active-bg:#dfc2c4;--bs-table-active-color:#000;--bs-table-hover-bg:#e5c7ca;--bs-table-hover-color:#000;color:#000;border-color:#dfc2c4}.table-light{--bs-table-bg:#f8f9fa;--bs-table-striped-bg:#ecedee;--bs-table-striped-color:#000;--bs-table-active-bg:#dfe0e1;--bs-table-active-color:#000;--bs-table-hover-bg:#e5e6e7;--bs-table-hover-color:#000;color:#000;border-color:#dfe0e1}.table-dark{--bs-table-bg:#212529;--bs-table-striped-bg:#2c3034;--bs-table-striped-color:#fff;--bs-table-active-bg:#373b3e;--bs-table-active-color:#fff;--bs-table-hover-bg:#323539;--bs-table-hover-color:#fff;color:#fff;border-color:#373b3e}.table-responsive{overflow-x:auto;-webkit-overflow-scrolling:touch}@media(max-width: 575.98px){.table-responsive-sm{overflow-x:auto;-webkit-overflow-scrolling:touch}}@media(max-width: 767.98px){.table-responsive-md{overflow-x:auto;-webkit-overflow-scrolling:touch}}@media(max-width: 991.98px){.table-responsive-lg{overflow-x:auto;-webkit-overflow-scrolling:touch}}@media(max-width: 1199.98px){.table-responsive-xl{overflow-x:auto;-webkit-overflow-scrolling:touch}}@media(max-width: 1399.98px){.table-responsive-xxl{overflow-x:auto;-webkit-overflow-scrolling:touch}}.btn{display:inline-block;font-weight:400;line-height:1.5;color:#212529;text-align:center;text-decoration:none;vertical-align:middle;cursor:pointer;user-select:none;background-color:transparent;border:1px solid transparent;padding:0.375rem 0.75rem;font-size:1rem;border-radius:0.25rem;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out}@media(prefers-reduced-motion: reduce){.btn{transition:none}}.btn:hover{color:#212529}.btn-check:focus+.btn,.btn:focus{outline:0;box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.25)}.btn:disabled,.btn.disabled{pointer-events:none;opacity:0.65}.btn-primary{color:#fff;background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.btn-primary:hover{color:#fff;background-color:#0b5ed7;border-color:#0a58ca}.btn-check:focus+.btn-primary,.btn-primary:focus{color:#fff;background-color:#0b5ed7;border-color:#0a58ca;box-shadow:0 0 0 0.25rem rgba(49, 132, 253, 0.5)}.btn-check:checked+.btn-primary,.btn-check:active+.btn-primary,.btn-primary:active,.btn-primary.active,.show>.btn-primary.dropdown-toggle{color:#fff;background-color:#0a58ca;border-color:#0a53be}.btn-check:checked+.btn-primary:focus,.btn-check:active+.btn-primary:focus,.btn-primary:active:focus,.btn-primary.active:focus,.show>.btn-primary.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(49, 132, 253, 0.5)}.btn-primary:disabled,.btn-primary.disabled{color:#fff;background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.btn-secondary{color:#fff;background-color:var(--bs-secondary,#c9d6df);border-color:var(--bs-secondary,#c9d6df)}.btn-secondary:hover{color:#fff;background-color:#5c636a;border-color:#565e64}.btn-check:focus+.btn-secondary,.btn-secondary:focus{color:#fff;background-color:#5c636a;border-color:#565e64;box-shadow:0 0 0 0.25rem rgba(130, 138, 145, 0.5)}.btn-check:checked+.btn-secondary,.btn-check:active+.btn-secondary,.btn-secondary:active,.btn-secondary.active,.show>.btn-secondary.dropdown-toggle{color:#fff;background-color:#565e64;border-color:#51585e}.btn-check:checked+.btn-secondary:focus,.btn-check:active+.btn-secondary:focus,.btn-secondary:active:focus,.btn-secondary.active:focus,.show>.btn-secondary.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(130, 138, 145, 0.5)}.btn-secondary:disabled,.btn-secondary.disabled{color:#fff;background-color:var(--bs-secondary,#c9d6df);border-color:var(--bs-secondary,#c9d6df)}.btn-success{color:#fff;background-color:var(--bs-warning,#ffc107);border-color:var(--bs-warning,#ffc107)}.btn-success:hover{color:#fff;background-color:#157347;border-color:#146c43}.btn-check:focus+.btn-success,.btn-success:focus{color:#fff;background-color:#157347;border-color:#146c43;box-shadow:0 0 0 0.25rem rgba(60, 153, 110, 0.5)}.btn-check:checked+.btn-success,.btn-check:active+.btn-success,.btn-success:active,.btn-success.active,.show>.btn-success.dropdown-toggle{color:#fff;background-color:#146c43;border-color:#13653f}.btn-check:checked+.btn-success:focus,.btn-check:active+.btn-success:focus,.btn-success:active:focus,.btn-success.active:focus,.show>.btn-success.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(60, 153, 110, 0.5)}.btn-success:disabled,.btn-success.disabled{color:#fff;background-color:var(--bs-warning,#ffc107);border-color:var(--bs-warning,#ffc107)}.btn-info{color:#000;background-color:var(--bs-info,#a2d5f2);border-color:var(--bs-info,#a2d5f2)}.btn-info:hover{color:#000;background-color:#31d2f2;border-color:#25cff2}.btn-check:focus+.btn-info,.btn-info:focus{color:#000;background-color:#31d2f2;border-color:#25cff2;box-shadow:0 0 0 0.25rem rgba(11, 172, 204, 0.5)}.btn-check:checked+.btn-info,.btn-check:active+.btn-info,.btn-info:active,.btn-info.active,.show>.btn-info.dropdown-toggle{color:#000;background-color:#3dd5f3;border-color:#25cff2}.btn-check:checked+.btn-info:focus,.btn-check:active+.btn-info:focus,.btn-info:active:focus,.btn-info.active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(11, 172, 204, 0.5)}.btn-info:disabled,.btn-info.disabled{color:#000;background-color:var(--bs-info,#a2d5f2);border-color:var(--bs-info,#a2d5f2)}.btn-warning{color:#000;background-color:#ffc107;border-color:#ffc107}.btn-warning:hover{color:#000;background-color:#ffca2c;border-color:#ffc720}.btn-check:focus+.btn-warning,.btn-warning:focus{color:#000;background-color:#ffca2c;border-color:#ffc720;box-shadow:0 0 0 0.25rem rgba(217, 164, 6, 0.5)}.btn-check:checked+.btn-warning,.btn-check:active+.btn-warning,.btn-warning:active,.btn-warning.active,.show>.btn-warning.dropdown-toggle{color:#000;background-color:#ffcd39;border-color:#ffc720}.btn-check:checked+.btn-warning:focus,.btn-check:active+.btn-warning:focus,.btn-warning:active:focus,.btn-warning.active:focus,.show>.btn-warning.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(217, 164, 6, 0.5)}.btn-warning:disabled,.btn-warning.disabled{color:#000;background-color:#ffc107;border-color:#ffc107}.btn-danger{color:#fff;background-color:var(--bs-danger,#f67280);border-color:var(--bs-danger,#f67280)}.btn-danger:hover{color:#fff;background-color:#bb2d3b;border-color:#b02a37}.btn-check:focus+.btn-danger,.btn-danger:focus{color:#fff;background-color:#bb2d3b;border-color:#b02a37;box-shadow:0 0 0 0.25rem rgba(225, 83, 97, 0.5)}.btn-check:checked+.btn-danger,.btn-check:active+.btn-danger,.btn-danger:active,.btn-danger.active,.show>.btn-danger.dropdown-toggle{color:#fff;background-color:#b02a37;border-color:#a52834}.btn-check:checked+.btn-danger:focus,.btn-check:active+.btn-danger:focus,.btn-danger:active:focus,.btn-danger.active:focus,.show>.btn-danger.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(225, 83, 97, 0.5)}.btn-danger:disabled,.btn-danger.disabled{color:#fff;background-color:var(--bs-danger,#f67280);border-color:var(--bs-danger,#f67280)}.btn-light{color:#000;background-color:#f8f9fa;border-color:#f8f9fa}.btn-light:hover{color:#000;background-color:#f9fafb;border-color:#f9fafb}.btn-check:focus+.btn-light,.btn-light:focus{color:#000;background-color:#f9fafb;border-color:#f9fafb;box-shadow:0 0 0 0.25rem rgba(211, 212, 213, 0.5)}.btn-check:checked+.btn-light,.btn-check:active+.btn-light,.btn-light:active,.btn-light.active,.show>.btn-light.dropdown-toggle{color:#000;background-color:#f9fafb;border-color:#f9fafb}.btn-check:checked+.btn-light:focus,.btn-check:active+.btn-light:focus,.btn-light:active:focus,.btn-light.active:focus,.show>.btn-light.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(211, 212, 213, 0.5)}.btn-light:disabled,.btn-light.disabled{color:#000;background-color:#f8f9fa;border-color:#f8f9fa}.btn-dark{color:#fff;background-color:#212529;border-color:#212529}.btn-dark:hover{color:#fff;background-color:#1c1f23;border-color:#1a1e21}.btn-check:focus+.btn-dark,.btn-dark:focus{color:#fff;background-color:#1c1f23;border-color:#1a1e21;box-shadow:0 0 0 0.25rem rgba(66, 70, 73, 0.5)}.btn-check:checked+.btn-dark,.btn-check:active+.btn-dark,.btn-dark:active,.btn-dark.active,.show>.btn-dark.dropdown-toggle{color:#fff;background-color:#1a1e21;border-color:#191c1f}.btn-check:checked+.btn-dark:focus,.btn-check:active+.btn-dark:focus,.btn-dark:active:focus,.btn-dark.active:focus,.show>.btn-dark.dropdown-toggle:focus{box-shadow:0 0 0 0.25rem rgba(66, 70, 73, 0.5)}.btn-dark:disabled,.btn-dark.disabled{color:#fff;background-color:#212529;border-color:#212529}.btn-outline-primary{color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.btn-outline-primary:hover{color:#fff;background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.btn-check:focus+.btn-outline-primary,.btn-outline-primary:focus{box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.5)}.btn-check:checked+.btn-outline-primary,.btn-check:active+.btn-outline-primary,.btn-outline-primary:active,.btn-outline-primary.active,.btn-outline-primary.dropdown-toggle.show{color:#fff;background-color:var(--bs-primary,#07689f);border-color:var(--bs-primary,#07689f)}.btn-check:checked+.btn-outline-primary:focus,.btn-check:active+.btn-outline-primary:focus,.btn-outline-primary:active:focus,.btn-outline-primary.active:focus,.btn-outline-primary.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(13, 110, 253, 0.5)}.btn-outline-primary:disabled,.btn-outline-primary.disabled{color:var(--bs-primary,#07689f);background-color:transparent}.btn-outline-secondary{color:var(--bs-secondary,#c9d6df);border-color:var(--bs-secondary,#c9d6df)}.btn-outline-secondary:hover{color:#fff;background-color:var(--bs-secondary,#c9d6df);border-color:var(--bs-secondary,#c9d6df)}.btn-check:focus+.btn-outline-secondary,.btn-outline-secondary:focus{box-shadow:0 0 0 0.25rem rgba(108, 117, 125, 0.5)}.btn-check:checked+.btn-outline-secondary,.btn-check:active+.btn-outline-secondary,.btn-outline-secondary:active,.btn-outline-secondary.active,.btn-outline-secondary.dropdown-toggle.show{color:#fff;background-color:var(--bs-secondary,#c9d6df);border-color:var(--bs-secondary,#c9d6df)}.btn-check:checked+.btn-outline-secondary:focus,.btn-check:active+.btn-outline-secondary:focus,.btn-outline-secondary:active:focus,.btn-outline-secondary.active:focus,.btn-outline-secondary.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(108, 117, 125, 0.5)}.btn-outline-secondary:disabled,.btn-outline-secondary.disabled{color:var(--bs-secondary,#c9d6df);background-color:transparent}.btn-outline-success{color:var(--bs-warning,#ffc107);border-color:var(--bs-warning,#ffc107)}.btn-outline-success:hover{color:#fff;background-color:var(--bs-warning,#ffc107);border-color:var(--bs-warning,#ffc107)}.btn-check:focus+.btn-outline-success,.btn-outline-success:focus{box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.5)}.btn-check:checked+.btn-outline-success,.btn-check:active+.btn-outline-success,.btn-outline-success:active,.btn-outline-success.active,.btn-outline-success.dropdown-toggle.show{color:#fff;background-color:var(--bs-warning,#ffc107);border-color:var(--bs-warning,#ffc107)}.btn-check:checked+.btn-outline-success:focus,.btn-check:active+.btn-outline-success:focus,.btn-outline-success:active:focus,.btn-outline-success.active:focus,.btn-outline-success.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(25, 135, 84, 0.5)}.btn-outline-success:disabled,.btn-outline-success.disabled{color:var(--bs-warning,#ffc107);background-color:transparent}.btn-outline-info{color:var(--bs-info,#a2d5f2);border-color:var(--bs-info,#a2d5f2)}.btn-outline-info:hover{color:#000;background-color:var(--bs-info,#a2d5f2);border-color:var(--bs-info,#a2d5f2)}.btn-check:focus+.btn-outline-info,.btn-outline-info:focus{box-shadow:0 0 0 0.25rem rgba(13, 202, 240, 0.5)}.btn-check:checked+.btn-outline-info,.btn-check:active+.btn-outline-info,.btn-outline-info:active,.btn-outline-info.active,.btn-outline-info.dropdown-toggle.show{color:#000;background-color:var(--bs-info,#a2d5f2);border-color:var(--bs-info,#a2d5f2)}.btn-check:checked+.btn-outline-info:focus,.btn-check:active+.btn-outline-info:focus,.btn-outline-info:active:focus,.btn-outline-info.active:focus,.btn-outline-info.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(13, 202, 240, 0.5)}.btn-outline-info:disabled,.btn-outline-info.disabled{color:var(--bs-info,#a2d5f2);background-color:transparent}.btn-outline-warning{color:#ffc107;border-color:#ffc107}.btn-outline-warning:hover{color:#000;background-color:#ffc107;border-color:#ffc107}.btn-check:focus+.btn-outline-warning,.btn-outline-warning:focus{box-shadow:0 0 0 0.25rem rgba(255, 193, 7, 0.5)}.btn-check:checked+.btn-outline-warning,.btn-check:active+.btn-outline-warning,.btn-outline-warning:active,.btn-outline-warning.active,.btn-outline-warning.dropdown-toggle.show{color:#000;background-color:#ffc107;border-color:#ffc107}.btn-check:checked+.btn-outline-warning:focus,.btn-check:active+.btn-outline-warning:focus,.btn-outline-warning:active:focus,.btn-outline-warning.active:focus,.btn-outline-warning.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(255, 193, 7, 0.5)}.btn-outline-warning:disabled,.btn-outline-warning.disabled{color:#ffc107;background-color:transparent}.btn-outline-danger{color:var(--bs-danger,#f67280);border-color:var(--bs-danger,#f67280)}.btn-outline-danger:hover{color:#fff;background-color:var(--bs-danger,#f67280);border-color:var(--bs-danger,#f67280)}.btn-check:focus+.btn-outline-danger,.btn-outline-danger:focus{box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.5)}.btn-check:checked+.btn-outline-danger,.btn-check:active+.btn-outline-danger,.btn-outline-danger:active,.btn-outline-danger.active,.btn-outline-danger.dropdown-toggle.show{color:#fff;background-color:var(--bs-danger,#f67280);border-color:var(--bs-danger,#f67280)}.btn-check:checked+.btn-outline-danger:focus,.btn-check:active+.btn-outline-danger:focus,.btn-outline-danger:active:focus,.btn-outline-danger.active:focus,.btn-outline-danger.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(220, 53, 69, 0.5)}.btn-outline-danger:disabled,.btn-outline-danger.disabled{color:var(--bs-danger,#f67280);background-color:transparent}.btn-outline-light{color:#f8f9fa;border-color:#f8f9fa}.btn-outline-light:hover{color:#000;background-color:#f8f9fa;border-color:#f8f9fa}.btn-check:focus+.btn-outline-light,.btn-outline-light:focus{box-shadow:0 0 0 0.25rem rgba(248, 249, 250, 0.5)}.btn-check:checked+.btn-outline-light,.btn-check:active+.btn-outline-light,.btn-outline-light:active,.btn-outline-light.active,.btn-outline-light.dropdown-toggle.show{color:#000;background-color:#f8f9fa;border-color:#f8f9fa}.btn-check:checked+.btn-outline-light:focus,.btn-check:active+.btn-outline-light:focus,.btn-outline-light:active:focus,.btn-outline-light.active:focus,.btn-outline-light.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(248, 249, 250, 0.5)}.btn-outline-light:disabled,.btn-outline-light.disabled{color:#f8f9fa;background-color:transparent}.btn-outline-dark{color:#212529;border-color:#212529}.btn-outline-dark:hover{color:#fff;background-color:#212529;border-color:#212529}.btn-check:focus+.btn-outline-dark,.btn-outline-dark:focus{box-shadow:0 0 0 0.25rem rgba(33, 37, 41, 0.5)}.btn-check:checked+.btn-outline-dark,.btn-check:active+.btn-outline-dark,.btn-outline-dark:active,.btn-outline-dark.active,.btn-outline-dark.dropdown-toggle.show{color:#fff;background-color:#212529;border-color:#212529}.btn-check:checked+.btn-outline-dark:focus,.btn-check:active+.btn-outline-dark:focus,.btn-outline-dark:active:focus,.btn-outline-dark.active:focus,.btn-outline-dark.dropdown-toggle.show:focus{box-shadow:0 0 0 0.25rem rgba(33, 37, 41, 0.5)}.btn-outline-dark:disabled,.btn-outline-dark.disabled{color:#212529;background-color:transparent}.btn-link{font-weight:400;color:var(--bs-primary,#07689f);text-decoration:underline}.btn-link:hover{color:#0a58ca}.btn-link:disabled,.btn-link.disabled{color:var(--bs-secondary,#c9d6df)}.btn-lg,.btn-group-lg>.btn{padding:0.5rem 1rem;font-size:1.25rem;border-radius:0.3rem}.btn-sm,.btn-group-sm>.btn{padding:0.25rem 0.5rem;font-size:0.875rem;border-radius:0.2rem}.btn-group,.btn-group-vertical{position:relative;display:inline-flex;vertical-align:middle}.btn-group>.btn,.btn-group-vertical>.btn{position:relative;flex:1 1 auto}.btn-group>.btn:hover,.btn-group>.btn:focus,.btn-group>.btn:active,.btn-group>.btn.active,.btn-group-vertical>.btn:hover,.btn-group-vertical>.btn:focus,.btn-group-vertical>.btn:active,.btn-group-vertical>.btn.active{z-index:1}.btn-toolbar{display:flex;flex-wrap:wrap;justify-content:flex-start}.btn-toolbar .input-group{width:auto}.btn-group>.btn:not(:first-child),.btn-group>.btn-group:not(:first-child){margin-left:-1px}.btn-group>.btn:not(:last-child):not(.dropdown-toggle){border-top-right-radius:0;border-bottom-right-radius:0}.btn-group>.btn:nth-child(n+3),.btn-group>:not(.btn-check)+.btn{border-top-left-radius:0;border-bottom-left-radius:0}.dropdown-toggle-split{padding-right:0.5625rem;padding-left:0.5625rem}.dropdown-toggle-split::after,.dropup .dropdown-toggle-split::after,.dropend .dropdown-toggle-split::after{margin-left:0}.dropstart .dropdown-toggle-split::before{margin-right:0}.btn-sm+.dropdown-toggle-split{padding-right:0.375rem;padding-left:0.375rem}.btn-lg+.dropdown-toggle-split{padding-right:0.75rem;padding-left:0.75rem}.btn-group-vertical{flex-direction:column;align-items:flex-start;justify-content:center}.btn-group-vertical>.btn,.btn-group-vertical>.btn-group{width:100%}.btn-group-vertical>.btn:not(:first-child),.btn-group-vertical>.btn-group:not(:first-child){margin-top:-1px}.btn-group-vertical>.btn:not(:last-child):not(.dropdown-toggle){border-bottom-right-radius:0;border-bottom-left-radius:0}.align-baseline{vertical-align:baseline !important}.align-top{vertical-align:top !important}.align-middle{vertical-align:middle !important}.align-bottom{vertical-align:bottom !important}.align-text-bottom{vertical-align:text-bottom !important}.align-text-top{vertical-align:text-top !important}.float-start{float:left !important}.float-end{float:right !important}.float-none{float:none !important}.opacity-0{opacity:0 !important}.opacity-25{opacity:0.25 !important}.opacity-50{opacity:0.5 !important}.opacity-75{opacity:0.75 !important}.opacity-100{opacity:1 !important}.overflow-auto{overflow:auto !important}.overflow-hidden{overflow:hidden !important}.overflow-visible{overflow:visible !important}.overflow-scroll{overflow:scroll !important}.d-inline{display:inline !important}.d-inline-block{display:inline-block !important}.d-block{display:block !important}.d-grid{display:grid !important}.d-table{display:table !important}.d-table-row{display:table-row !important}.d-table-cell{display:table-cell !important}.d-flex{display:flex !important}.d-inline-flex{display:inline-flex !important}.d-none{display:none !important}.shadow{box-shadow:0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important}.shadow-sm{box-shadow:0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important}.shadow-lg{box-shadow:0 1rem 3rem rgba(0, 0, 0, 0.175) !important}.shadow-none{box-shadow:none !important}.position-static{position:static !important}.position-relative{position:relative !important}.position-absolute{position:absolute !important}.position-fixed{position:fixed !important}.position-sticky{position:sticky !important}.top-0{top:0 !important}.top-50{top:50% !important}.top-100{top:100% !important}.bottom-0{bottom:0 !important}.bottom-50{bottom:50% !important}.bottom-100{bottom:100% !important}.start-0{left:0 !important}.start-50{left:50% !important}.start-100{left:100% !important}.end-0{right:0 !important}.end-50{right:50% !important}.end-100{right:100% !important}.translate-middle{transform:translate(-50%, -50%) !important}.translate-middle-x{transform:translateX(-50%) !important}.translate-middle-y{transform:translateY(-50%) !important}.border{border:1px solid #dee2e6 !important}.border-0{border:0 !important}.border-top{border-top:1px solid #dee2e6 !important}.border-top-0{border-top:0 !important}.border-end{border-right:1px solid #dee2e6 !important}.border-end-0{border-right:0 !important}.border-bottom{border-bottom:1px solid #dee2e6 !important}.border-bottom-0{border-bottom:0 !important}.border-start{border-left:1px solid #dee2e6 !important}.border-start-0{border-left:0 !important}.border-primary{border-color:var(--bs-primary,#07689f) !important}.border-secondary{border-color:var(--bs-secondary,#c9d6df) !important}.border-success{border-color:var(--bs-warning,#ffc107) !important}.border-info{border-color:var(--bs-info,#a2d5f2) !important}.border-warning{border-color:#ffc107 !important}.border-danger{border-color:var(--bs-danger,#f67280) !important}.border-light{border-color:#f8f9fa !important}.border-dark{border-color:#212529 !important}.border-white{border-color:#fff !important}.border-1{border-width:1px !important}.border-2{border-width:2px !important}.border-3{border-width:3px !important}.border-4{border-width:4px !important}.border-5{border-width:5px !important}.w-25{width:25% !important}.w-50{width:50% !important}.w-75{width:75% !important}.w-100{width:100% !important}.w-auto{width:auto !important}.mw-100{max-width:100% !important}.vw-100{width:100vw !important}.min-vw-100{min-width:100vw !important}.h-25{height:25% !important}.h-50{height:50% !important}.h-75{height:75% !important}.h-100{height:100% !important}.h-auto{height:auto !important}.mh-100{max-height:100% !important}.vh-100{height:100vh !important}.min-vh-100{min-height:100vh !important}.flex-fill{flex:1 1 auto !important}.flex-row{flex-direction:row !important}.flex-column{flex-direction:column !important}.flex-row-reverse{flex-direction:row-reverse !important}.flex-column-reverse{flex-direction:column-reverse !important}.flex-grow-0{flex-grow:0 !important}.flex-grow-1{flex-grow:1 !important}.flex-shrink-0{flex-shrink:0 !important}.flex-shrink-1{flex-shrink:1 !important}.flex-wrap{flex-wrap:wrap !important}.flex-nowrap{flex-wrap:nowrap !important}.flex-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-0{gap:0 !important}.gap-1{gap:0.25rem !important}.gap-2{gap:0.5rem !important}.gap-3{gap:1rem !important}.gap-4{gap:1.5rem !important}.gap-5{gap:3rem !important}.justify-content-start{justify-content:flex-start !important}.justify-content-end{justify-content:flex-end !important}.justify-content-center{justify-content:center !important}.justify-content-between{justify-content:space-between !important}.justify-content-around{justify-content:space-around !important}.justify-content-evenly{justify-content:space-evenly !important}.align-items-start{align-items:flex-start !important}.align-items-end{align-items:flex-end !important}.align-items-center{align-items:center !important}.align-items-baseline{align-items:baseline !important}.align-items-stretch{align-items:stretch !important}.align-content-start{align-content:flex-start !important}.align-content-end{align-content:flex-end !important}.align-content-center{align-content:center !important}.align-content-between{align-content:space-between !important}.align-content-around{align-content:space-around !important}.align-content-stretch{align-content:stretch !important}.align-self-auto{align-self:auto !important}.align-self-start{align-self:flex-start !important}.align-self-end{align-self:flex-end !important}.align-self-center{align-self:center !important}.align-self-baseline{align-self:baseline !important}.align-self-stretch{align-self:stretch !important}.order-first{order:-1 !important}.order-0{order:0 !important}.order-1{order:1 !important}.order-2{order:2 !important}.order-3{order:3 !important}.order-4{order:4 !important}.order-5{order:5 !important}.order-last{order:6 !important}.m-0{margin:0 !important}.m-1{margin:0.25rem !important}.m-2{margin:0.5rem !important}.m-3{margin:1rem !important}.m-4{margin:1.5rem !important}.m-5{margin:3rem !important}.m-auto{margin:auto !important}.mx-0{margin-right:0 !important;margin-left:0 !important}.mx-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-3{margin-right:1rem !important;margin-left:1rem !important}.mx-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-5{margin-right:3rem !important;margin-left:3rem !important}.mx-auto{margin-right:auto !important;margin-left:auto !important}.my-0{margin-top:0 !important;margin-bottom:0 !important}.my-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-0{margin-top:0 !important}.mt-1{margin-top:0.25rem !important}.mt-2{margin-top:0.5rem !important}.mt-3{margin-top:1rem !important}.mt-4{margin-top:1.5rem !important}.mt-5{margin-top:3rem !important}.mt-auto{margin-top:auto !important}.me-0{margin-right:0 !important}.me-1{margin-right:0.25rem !important}.me-2{margin-right:0.5rem !important}.me-3{margin-right:1rem !important}.me-4{margin-right:1.5rem !important}.me-5{margin-right:3rem !important}.me-auto{margin-right:auto !important}.mb-0{margin-bottom:0 !important}.mb-1{margin-bottom:0.25rem !important}.mb-2{margin-bottom:0.5rem !important}.mb-3{margin-bottom:1rem !important}.mb-4{margin-bottom:1.5rem !important}.mb-5{margin-bottom:3rem !important}.mb-auto{margin-bottom:auto !important}.ms-0{margin-left:0 !important}.ms-1{margin-left:0.25rem !important}.ms-2{margin-left:0.5rem !important}.ms-3{margin-left:1rem !important}.ms-4{margin-left:1.5rem !important}.ms-5{margin-left:3rem !important}.ms-auto{margin-left:auto !important}.p-0{padding:0 !important}.p-1{padding:0.25rem !important}.p-2{padding:0.5rem !important}.p-3{padding:1rem !important}.p-4{padding:1.5rem !important}.p-5{padding:3rem !important}.px-0{padding-right:0 !important;padding-left:0 !important}.px-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-3{padding-right:1rem !important;padding-left:1rem !important}.px-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-5{padding-right:3rem !important;padding-left:3rem !important}.py-0{padding-top:0 !important;padding-bottom:0 !important}.py-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-0{padding-top:0 !important}.pt-1{padding-top:0.25rem !important}.pt-2{padding-top:0.5rem !important}.pt-3{padding-top:1rem !important}.pt-4{padding-top:1.5rem !important}.pt-5{padding-top:3rem !important}.pe-0{padding-right:0 !important}.pe-1{padding-right:0.25rem !important}.pe-2{padding-right:0.5rem !important}.pe-3{padding-right:1rem !important}.pe-4{padding-right:1.5rem !important}.pe-5{padding-right:3rem !important}.pb-0{padding-bottom:0 !important}.pb-1{padding-bottom:0.25rem !important}.pb-2{padding-bottom:0.5rem !important}.pb-3{padding-bottom:1rem !important}.pb-4{padding-bottom:1.5rem !important}.pb-5{padding-bottom:3rem !important}.ps-0{padding-left:0 !important}.ps-1{padding-left:0.25rem !important}.ps-2{padding-left:0.5rem !important}.ps-3{padding-left:1rem !important}.ps-4{padding-left:1.5rem !important}.ps-5{padding-left:3rem !important}.font-monospace{font-family:var(--bs-font-monospace) !important}.fs-1{font-size:calc(1.375rem + 1.5vw) !important}.fs-2{font-size:calc(1.325rem + 0.9vw) !important}.fs-3{font-size:calc(1.3rem + 0.6vw) !important}.fs-4{font-size:calc(1.275rem + 0.3vw) !important}.fs-5{font-size:1.25rem !important}.fs-6{font-size:1rem !important}.fst-italic{font-style:italic !important}.fst-normal{font-style:normal !important}.fw-light{font-weight:300 !important}.fw-lighter{font-weight:lighter !important}.fw-normal{font-weight:400 !important}.fw-bold{font-weight:700 !important}.fw-bolder{font-weight:bolder !important}.lh-1{line-height:1 !important}.lh-sm{line-height:1.25 !important}.lh-base{line-height:1.5 !important}.lh-lg{line-height:2 !important}.text-start{text-align:left !important}.text-end{text-align:right !important}.text-center{text-align:center !important}.text-decoration-none{text-decoration:none !important}.text-decoration-underline{text-decoration:underline !important}.text-decoration-line-through{text-decoration:line-through !important}.text-lowercase{text-transform:lowercase !important}.text-uppercase{text-transform:uppercase !important}.text-capitalize{text-transform:capitalize !important}.text-wrap{white-space:normal !important}.text-nowrap{white-space:nowrap !important}.text-break{word-wrap:break-word !important;word-break:break-word !important}.text-primary{--bs-text-opacity:1;color:rgba(var(--bs-primary-rgb), var(--bs-text-opacity)) !important}.text-secondary{--bs-text-opacity:1;color:rgba(var(--bs-secondary-rgb), var(--bs-text-opacity)) !important}.text-success{--bs-text-opacity:1;color:rgba(var(--bs-success-rgb), var(--bs-text-opacity)) !important}.text-info{--bs-text-opacity:1;color:rgba(var(--bs-info-rgb), var(--bs-text-opacity)) !important}.text-warning{--bs-text-opacity:1;color:rgba(var(--bs-warning-rgb), var(--bs-text-opacity)) !important}.text-danger{--bs-text-opacity:1;color:rgba(var(--bs-danger-rgb), var(--bs-text-opacity)) !important}.text-light{--bs-text-opacity:1;color:rgba(var(--bs-light-rgb), var(--bs-text-opacity)) !important}.text-dark{--bs-text-opacity:1;color:rgba(var(--bs-dark-rgb), var(--bs-text-opacity)) !important}.text-black{--bs-text-opacity:1;color:rgba(var(--bs-black-rgb), var(--bs-text-opacity)) !important}.text-white{--bs-text-opacity:1;color:rgba(var(--bs-white-rgb), var(--bs-text-opacity)) !important}.text-body{--bs-text-opacity:1;color:rgba(var(--bs-body-color-rgb), var(--bs-text-opacity)) !important}.text-muted{--bs-text-opacity:1;color:var(--bs-secondary,#c9d6df) !important}.text-black-50{--bs-text-opacity:1;color:rgba(0, 0, 0, 0.5) !important}.text-white-50{--bs-text-opacity:1;color:rgba(255, 255, 255, 0.5) !important}.text-reset{--bs-text-opacity:1;color:inherit !important}.text-opacity-25{--bs-text-opacity:0.25}.text-opacity-50{--bs-text-opacity:0.5}.text-opacity-75{--bs-text-opacity:0.75}.text-opacity-100{--bs-text-opacity:1}.bg-primary{--bs-bg-opacity:1;background-color:rgba(var(--bs-primary-rgb), var(--bs-bg-opacity)) !important}.bg-secondary{--bs-bg-opacity:1;background-color:rgba(var(--bs-secondary-rgb), var(--bs-bg-opacity)) !important}.bg-success{--bs-bg-opacity:1;background-color:rgba(var(--bs-success-rgb), var(--bs-bg-opacity)) !important}.bg-info{--bs-bg-opacity:1;background-color:rgba(var(--bs-info-rgb), var(--bs-bg-opacity)) !important}.bg-warning{--bs-bg-opacity:1;background-color:rgba(var(--bs-warning-rgb), var(--bs-bg-opacity)) !important}.bg-danger{--bs-bg-opacity:1;background-color:rgba(var(--bs-danger-rgb), var(--bs-bg-opacity)) !important}.bg-light{--bs-bg-opacity:1;background-color:rgba(var(--bs-light-rgb), var(--bs-bg-opacity)) !important}.bg-dark{--bs-bg-opacity:1;background-color:rgba(var(--bs-dark-rgb), var(--bs-bg-opacity)) !important}.bg-black{--bs-bg-opacity:1;background-color:rgba(var(--bs-black-rgb), var(--bs-bg-opacity)) !important}.bg-white{--bs-bg-opacity:1;background-color:rgba(var(--bs-white-rgb), var(--bs-bg-opacity)) !important}.bg-body{--bs-bg-opacity:1;background-color:rgba(var(--bs-body-bg-rgb), var(--bs-bg-opacity)) !important}.bg-transparent{--bs-bg-opacity:1;background-color:transparent !important}.bg-opacity-10{--bs-bg-opacity:0.1}.bg-opacity-25{--bs-bg-opacity:0.25}.bg-opacity-50{--bs-bg-opacity:0.5}.bg-opacity-75{--bs-bg-opacity:0.75}.bg-opacity-100{--bs-bg-opacity:1}.bg-gradient{background-image:var(--bs-gradient) !important}.user-select-all{user-select:all !important}.user-select-auto{user-select:auto !important}.user-select-none{user-select:none !important}.pe-none{pointer-events:none !important}.pe-auto{pointer-events:auto !important}.rounded{border-radius:0.25rem !important}.rounded-0{border-radius:0 !important}.rounded-1{border-radius:0.2rem !important}.rounded-2{border-radius:0.25rem !important}.rounded-3{border-radius:0.3rem !important}.rounded-circle{border-radius:50% !important}.rounded-pill{border-radius:50rem !important}.rounded-top{border-top-left-radius:0.25rem !important;border-top-right-radius:0.25rem !important}.rounded-end{border-top-right-radius:0.25rem !important;border-bottom-right-radius:0.25rem !important}.rounded-bottom{border-bottom-right-radius:0.25rem !important;border-bottom-left-radius:0.25rem !important}.rounded-start{border-bottom-left-radius:0.25rem !important;border-top-left-radius:0.25rem !important}.visible{visibility:visible !important}.invisible{visibility:hidden !important}@media(min-width: 576px){.float-sm-start{float:left !important}.float-sm-end{float:right !important}.float-sm-none{float:none !important}.d-sm-inline{display:inline !important}.d-sm-inline-block{display:inline-block !important}.d-sm-block{display:block !important}.d-sm-grid{display:grid !important}.d-sm-table{display:table !important}.d-sm-table-row{display:table-row !important}.d-sm-table-cell{display:table-cell !important}.d-sm-flex{display:flex !important}.d-sm-inline-flex{display:inline-flex !important}.d-sm-none{display:none !important}.flex-sm-fill{flex:1 1 auto !important}.flex-sm-row{flex-direction:row !important}.flex-sm-column{flex-direction:column !important}.flex-sm-row-reverse{flex-direction:row-reverse !important}.flex-sm-column-reverse{flex-direction:column-reverse !important}.flex-sm-grow-0{flex-grow:0 !important}.flex-sm-grow-1{flex-grow:1 !important}.flex-sm-shrink-0{flex-shrink:0 !important}.flex-sm-shrink-1{flex-shrink:1 !important}.flex-sm-wrap{flex-wrap:wrap !important}.flex-sm-nowrap{flex-wrap:nowrap !important}.flex-sm-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-sm-0{gap:0 !important}.gap-sm-1{gap:0.25rem !important}.gap-sm-2{gap:0.5rem !important}.gap-sm-3{gap:1rem !important}.gap-sm-4{gap:1.5rem !important}.gap-sm-5{gap:3rem !important}.justify-content-sm-start{justify-content:flex-start !important}.justify-content-sm-end{justify-content:flex-end !important}.justify-content-sm-center{justify-content:center !important}.justify-content-sm-between{justify-content:space-between !important}.justify-content-sm-around{justify-content:space-around !important}.justify-content-sm-evenly{justify-content:space-evenly !important}.align-items-sm-start{align-items:flex-start !important}.align-items-sm-end{align-items:flex-end !important}.align-items-sm-center{align-items:center !important}.align-items-sm-baseline{align-items:baseline !important}.align-items-sm-stretch{align-items:stretch !important}.align-content-sm-start{align-content:flex-start !important}.align-content-sm-end{align-content:flex-end !important}.align-content-sm-center{align-content:center !important}.align-content-sm-between{align-content:space-between !important}.align-content-sm-around{align-content:space-around !important}.align-content-sm-stretch{align-content:stretch !important}.align-self-sm-auto{align-self:auto !important}.align-self-sm-start{align-self:flex-start !important}.align-self-sm-end{align-self:flex-end !important}.align-self-sm-center{align-self:center !important}.align-self-sm-baseline{align-self:baseline !important}.align-self-sm-stretch{align-self:stretch !important}.order-sm-first{order:-1 !important}.order-sm-0{order:0 !important}.order-sm-1{order:1 !important}.order-sm-2{order:2 !important}.order-sm-3{order:3 !important}.order-sm-4{order:4 !important}.order-sm-5{order:5 !important}.order-sm-last{order:6 !important}.m-sm-0{margin:0 !important}.m-sm-1{margin:0.25rem !important}.m-sm-2{margin:0.5rem !important}.m-sm-3{margin:1rem !important}.m-sm-4{margin:1.5rem !important}.m-sm-5{margin:3rem !important}.m-sm-auto{margin:auto !important}.mx-sm-0{margin-right:0 !important;margin-left:0 !important}.mx-sm-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-sm-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-sm-3{margin-right:1rem !important;margin-left:1rem !important}.mx-sm-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-sm-5{margin-right:3rem !important;margin-left:3rem !important}.mx-sm-auto{margin-right:auto !important;margin-left:auto !important}.my-sm-0{margin-top:0 !important;margin-bottom:0 !important}.my-sm-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-sm-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-sm-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-sm-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-sm-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-sm-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-sm-0{margin-top:0 !important}.mt-sm-1{margin-top:0.25rem !important}.mt-sm-2{margin-top:0.5rem !important}.mt-sm-3{margin-top:1rem !important}.mt-sm-4{margin-top:1.5rem !important}.mt-sm-5{margin-top:3rem !important}.mt-sm-auto{margin-top:auto !important}.me-sm-0{margin-right:0 !important}.me-sm-1{margin-right:0.25rem !important}.me-sm-2{margin-right:0.5rem !important}.me-sm-3{margin-right:1rem !important}.me-sm-4{margin-right:1.5rem !important}.me-sm-5{margin-right:3rem !important}.me-sm-auto{margin-right:auto !important}.mb-sm-0{margin-bottom:0 !important}.mb-sm-1{margin-bottom:0.25rem !important}.mb-sm-2{margin-bottom:0.5rem !important}.mb-sm-3{margin-bottom:1rem !important}.mb-sm-4{margin-bottom:1.5rem !important}.mb-sm-5{margin-bottom:3rem !important}.mb-sm-auto{margin-bottom:auto !important}.ms-sm-0{margin-left:0 !important}.ms-sm-1{margin-left:0.25rem !important}.ms-sm-2{margin-left:0.5rem !important}.ms-sm-3{margin-left:1rem !important}.ms-sm-4{margin-left:1.5rem !important}.ms-sm-5{margin-left:3rem !important}.ms-sm-auto{margin-left:auto !important}.p-sm-0{padding:0 !important}.p-sm-1{padding:0.25rem !important}.p-sm-2{padding:0.5rem !important}.p-sm-3{padding:1rem !important}.p-sm-4{padding:1.5rem !important}.p-sm-5{padding:3rem !important}.px-sm-0{padding-right:0 !important;padding-left:0 !important}.px-sm-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-sm-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-sm-3{padding-right:1rem !important;padding-left:1rem !important}.px-sm-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-sm-5{padding-right:3rem !important;padding-left:3rem !important}.py-sm-0{padding-top:0 !important;padding-bottom:0 !important}.py-sm-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-sm-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-sm-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-sm-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-sm-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-sm-0{padding-top:0 !important}.pt-sm-1{padding-top:0.25rem !important}.pt-sm-2{padding-top:0.5rem !important}.pt-sm-3{padding-top:1rem !important}.pt-sm-4{padding-top:1.5rem !important}.pt-sm-5{padding-top:3rem !important}.pe-sm-0{padding-right:0 !important}.pe-sm-1{padding-right:0.25rem !important}.pe-sm-2{padding-right:0.5rem !important}.pe-sm-3{padding-right:1rem !important}.pe-sm-4{padding-right:1.5rem !important}.pe-sm-5{padding-right:3rem !important}.pb-sm-0{padding-bottom:0 !important}.pb-sm-1{padding-bottom:0.25rem !important}.pb-sm-2{padding-bottom:0.5rem !important}.pb-sm-3{padding-bottom:1rem !important}.pb-sm-4{padding-bottom:1.5rem !important}.pb-sm-5{padding-bottom:3rem !important}.ps-sm-0{padding-left:0 !important}.ps-sm-1{padding-left:0.25rem !important}.ps-sm-2{padding-left:0.5rem !important}.ps-sm-3{padding-left:1rem !important}.ps-sm-4{padding-left:1.5rem !important}.ps-sm-5{padding-left:3rem !important}.text-sm-start{text-align:left !important}.text-sm-end{text-align:right !important}.text-sm-center{text-align:center !important}}@media(min-width: 768px){.float-md-start{float:left !important}.float-md-end{float:right !important}.float-md-none{float:none !important}.d-md-inline{display:inline !important}.d-md-inline-block{display:inline-block !important}.d-md-block{display:block !important}.d-md-grid{display:grid !important}.d-md-table{display:table !important}.d-md-table-row{display:table-row !important}.d-md-table-cell{display:table-cell !important}.d-md-flex{display:flex !important}.d-md-inline-flex{display:inline-flex !important}.d-md-none{display:none !important}.flex-md-fill{flex:1 1 auto !important}.flex-md-row{flex-direction:row !important}.flex-md-column{flex-direction:column !important}.flex-md-row-reverse{flex-direction:row-reverse !important}.flex-md-column-reverse{flex-direction:column-reverse !important}.flex-md-grow-0{flex-grow:0 !important}.flex-md-grow-1{flex-grow:1 !important}.flex-md-shrink-0{flex-shrink:0 !important}.flex-md-shrink-1{flex-shrink:1 !important}.flex-md-wrap{flex-wrap:wrap !important}.flex-md-nowrap{flex-wrap:nowrap !important}.flex-md-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-md-0{gap:0 !important}.gap-md-1{gap:0.25rem !important}.gap-md-2{gap:0.5rem !important}.gap-md-3{gap:1rem !important}.gap-md-4{gap:1.5rem !important}.gap-md-5{gap:3rem !important}.justify-content-md-start{justify-content:flex-start !important}.justify-content-md-end{justify-content:flex-end !important}.justify-content-md-center{justify-content:center !important}.justify-content-md-between{justify-content:space-between !important}.justify-content-md-around{justify-content:space-around !important}.justify-content-md-evenly{justify-content:space-evenly !important}.align-items-md-start{align-items:flex-start !important}.align-items-md-end{align-items:flex-end !important}.align-items-md-center{align-items:center !important}.align-items-md-baseline{align-items:baseline !important}.align-items-md-stretch{align-items:stretch !important}.align-content-md-start{align-content:flex-start !important}.align-content-md-end{align-content:flex-end !important}.align-content-md-center{align-content:center !important}.align-content-md-between{align-content:space-between !important}.align-content-md-around{align-content:space-around !important}.align-content-md-stretch{align-content:stretch !important}.align-self-md-auto{align-self:auto !important}.align-self-md-start{align-self:flex-start !important}.align-self-md-end{align-self:flex-end !important}.align-self-md-center{align-self:center !important}.align-self-md-baseline{align-self:baseline !important}.align-self-md-stretch{align-self:stretch !important}.order-md-first{order:-1 !important}.order-md-0{order:0 !important}.order-md-1{order:1 !important}.order-md-2{order:2 !important}.order-md-3{order:3 !important}.order-md-4{order:4 !important}.order-md-5{order:5 !important}.order-md-last{order:6 !important}.m-md-0{margin:0 !important}.m-md-1{margin:0.25rem !important}.m-md-2{margin:0.5rem !important}.m-md-3{margin:1rem !important}.m-md-4{margin:1.5rem !important}.m-md-5{margin:3rem !important}.m-md-auto{margin:auto !important}.mx-md-0{margin-right:0 !important;margin-left:0 !important}.mx-md-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-md-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-md-3{margin-right:1rem !important;margin-left:1rem !important}.mx-md-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-md-5{margin-right:3rem !important;margin-left:3rem !important}.mx-md-auto{margin-right:auto !important;margin-left:auto !important}.my-md-0{margin-top:0 !important;margin-bottom:0 !important}.my-md-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-md-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-md-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-md-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-md-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-md-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-md-0{margin-top:0 !important}.mt-md-1{margin-top:0.25rem !important}.mt-md-2{margin-top:0.5rem !important}.mt-md-3{margin-top:1rem !important}.mt-md-4{margin-top:1.5rem !important}.mt-md-5{margin-top:3rem !important}.mt-md-auto{margin-top:auto !important}.me-md-0{margin-right:0 !important}.me-md-1{margin-right:0.25rem !important}.me-md-2{margin-right:0.5rem !important}.me-md-3{margin-right:1rem !important}.me-md-4{margin-right:1.5rem !important}.me-md-5{margin-right:3rem !important}.me-md-auto{margin-right:auto !important}.mb-md-0{margin-bottom:0 !important}.mb-md-1{margin-bottom:0.25rem !important}.mb-md-2{margin-bottom:0.5rem !important}.mb-md-3{margin-bottom:1rem !important}.mb-md-4{margin-bottom:1.5rem !important}.mb-md-5{margin-bottom:3rem !important}.mb-md-auto{margin-bottom:auto !important}.ms-md-0{margin-left:0 !important}.ms-md-1{margin-left:0.25rem !important}.ms-md-2{margin-left:0.5rem !important}.ms-md-3{margin-left:1rem !important}.ms-md-4{margin-left:1.5rem !important}.ms-md-5{margin-left:3rem !important}.ms-md-auto{margin-left:auto !important}.p-md-0{padding:0 !important}.p-md-1{padding:0.25rem !important}.p-md-2{padding:0.5rem !important}.p-md-3{padding:1rem !important}.p-md-4{padding:1.5rem !important}.p-md-5{padding:3rem !important}.px-md-0{padding-right:0 !important;padding-left:0 !important}.px-md-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-md-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-md-3{padding-right:1rem !important;padding-left:1rem !important}.px-md-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-md-5{padding-right:3rem !important;padding-left:3rem !important}.py-md-0{padding-top:0 !important;padding-bottom:0 !important}.py-md-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-md-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-md-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-md-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-md-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-md-0{padding-top:0 !important}.pt-md-1{padding-top:0.25rem !important}.pt-md-2{padding-top:0.5rem !important}.pt-md-3{padding-top:1rem !important}.pt-md-4{padding-top:1.5rem !important}.pt-md-5{padding-top:3rem !important}.pe-md-0{padding-right:0 !important}.pe-md-1{padding-right:0.25rem !important}.pe-md-2{padding-right:0.5rem !important}.pe-md-3{padding-right:1rem !important}.pe-md-4{padding-right:1.5rem !important}.pe-md-5{padding-right:3rem !important}.pb-md-0{padding-bottom:0 !important}.pb-md-1{padding-bottom:0.25rem !important}.pb-md-2{padding-bottom:0.5rem !important}.pb-md-3{padding-bottom:1rem !important}.pb-md-4{padding-bottom:1.5rem !important}.pb-md-5{padding-bottom:3rem !important}.ps-md-0{padding-left:0 !important}.ps-md-1{padding-left:0.25rem !important}.ps-md-2{padding-left:0.5rem !important}.ps-md-3{padding-left:1rem !important}.ps-md-4{padding-left:1.5rem !important}.ps-md-5{padding-left:3rem !important}.text-md-start{text-align:left !important}.text-md-end{text-align:right !important}.text-md-center{text-align:center !important}}@media(min-width: 992px){.float-lg-start{float:left !important}.float-lg-end{float:right !important}.float-lg-none{float:none !important}.d-lg-inline{display:inline !important}.d-lg-inline-block{display:inline-block !important}.d-lg-block{display:block !important}.d-lg-grid{display:grid !important}.d-lg-table{display:table !important}.d-lg-table-row{display:table-row !important}.d-lg-table-cell{display:table-cell !important}.d-lg-flex{display:flex !important}.d-lg-inline-flex{display:inline-flex !important}.d-lg-none{display:none !important}.flex-lg-fill{flex:1 1 auto !important}.flex-lg-row{flex-direction:row !important}.flex-lg-column{flex-direction:column !important}.flex-lg-row-reverse{flex-direction:row-reverse !important}.flex-lg-column-reverse{flex-direction:column-reverse !important}.flex-lg-grow-0{flex-grow:0 !important}.flex-lg-grow-1{flex-grow:1 !important}.flex-lg-shrink-0{flex-shrink:0 !important}.flex-lg-shrink-1{flex-shrink:1 !important}.flex-lg-wrap{flex-wrap:wrap !important}.flex-lg-nowrap{flex-wrap:nowrap !important}.flex-lg-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-lg-0{gap:0 !important}.gap-lg-1{gap:0.25rem !important}.gap-lg-2{gap:0.5rem !important}.gap-lg-3{gap:1rem !important}.gap-lg-4{gap:1.5rem !important}.gap-lg-5{gap:3rem !important}.justify-content-lg-start{justify-content:flex-start !important}.justify-content-lg-end{justify-content:flex-end !important}.justify-content-lg-center{justify-content:center !important}.justify-content-lg-between{justify-content:space-between !important}.justify-content-lg-around{justify-content:space-around !important}.justify-content-lg-evenly{justify-content:space-evenly !important}.align-items-lg-start{align-items:flex-start !important}.align-items-lg-end{align-items:flex-end !important}.align-items-lg-center{align-items:center !important}.align-items-lg-baseline{align-items:baseline !important}.align-items-lg-stretch{align-items:stretch !important}.align-content-lg-start{align-content:flex-start !important}.align-content-lg-end{align-content:flex-end !important}.align-content-lg-center{align-content:center !important}.align-content-lg-between{align-content:space-between !important}.align-content-lg-around{align-content:space-around !important}.align-content-lg-stretch{align-content:stretch !important}.align-self-lg-auto{align-self:auto !important}.align-self-lg-start{align-self:flex-start !important}.align-self-lg-end{align-self:flex-end !important}.align-self-lg-center{align-self:center !important}.align-self-lg-baseline{align-self:baseline !important}.align-self-lg-stretch{align-self:stretch !important}.order-lg-first{order:-1 !important}.order-lg-0{order:0 !important}.order-lg-1{order:1 !important}.order-lg-2{order:2 !important}.order-lg-3{order:3 !important}.order-lg-4{order:4 !important}.order-lg-5{order:5 !important}.order-lg-last{order:6 !important}.m-lg-0{margin:0 !important}.m-lg-1{margin:0.25rem !important}.m-lg-2{margin:0.5rem !important}.m-lg-3{margin:1rem !important}.m-lg-4{margin:1.5rem !important}.m-lg-5{margin:3rem !important}.m-lg-auto{margin:auto !important}.mx-lg-0{margin-right:0 !important;margin-left:0 !important}.mx-lg-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-lg-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-lg-3{margin-right:1rem !important;margin-left:1rem !important}.mx-lg-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-lg-5{margin-right:3rem !important;margin-left:3rem !important}.mx-lg-auto{margin-right:auto !important;margin-left:auto !important}.my-lg-0{margin-top:0 !important;margin-bottom:0 !important}.my-lg-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-lg-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-lg-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-lg-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-lg-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-lg-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-lg-0{margin-top:0 !important}.mt-lg-1{margin-top:0.25rem !important}.mt-lg-2{margin-top:0.5rem !important}.mt-lg-3{margin-top:1rem !important}.mt-lg-4{margin-top:1.5rem !important}.mt-lg-5{margin-top:3rem !important}.mt-lg-auto{margin-top:auto !important}.me-lg-0{margin-right:0 !important}.me-lg-1{margin-right:0.25rem !important}.me-lg-2{margin-right:0.5rem !important}.me-lg-3{margin-right:1rem !important}.me-lg-4{margin-right:1.5rem !important}.me-lg-5{margin-right:3rem !important}.me-lg-auto{margin-right:auto !important}.mb-lg-0{margin-bottom:0 !important}.mb-lg-1{margin-bottom:0.25rem !important}.mb-lg-2{margin-bottom:0.5rem !important}.mb-lg-3{margin-bottom:1rem !important}.mb-lg-4{margin-bottom:1.5rem !important}.mb-lg-5{margin-bottom:3rem !important}.mb-lg-auto{margin-bottom:auto !important}.ms-lg-0{margin-left:0 !important}.ms-lg-1{margin-left:0.25rem !important}.ms-lg-2{margin-left:0.5rem !important}.ms-lg-3{margin-left:1rem !important}.ms-lg-4{margin-left:1.5rem !important}.ms-lg-5{margin-left:3rem !important}.ms-lg-auto{margin-left:auto !important}.p-lg-0{padding:0 !important}.p-lg-1{padding:0.25rem !important}.p-lg-2{padding:0.5rem !important}.p-lg-3{padding:1rem !important}.p-lg-4{padding:1.5rem !important}.p-lg-5{padding:3rem !important}.px-lg-0{padding-right:0 !important;padding-left:0 !important}.px-lg-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-lg-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-lg-3{padding-right:1rem !important;padding-left:1rem !important}.px-lg-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-lg-5{padding-right:3rem !important;padding-left:3rem !important}.py-lg-0{padding-top:0 !important;padding-bottom:0 !important}.py-lg-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-lg-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-lg-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-lg-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-lg-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-lg-0{padding-top:0 !important}.pt-lg-1{padding-top:0.25rem !important}.pt-lg-2{padding-top:0.5rem !important}.pt-lg-3{padding-top:1rem !important}.pt-lg-4{padding-top:1.5rem !important}.pt-lg-5{padding-top:3rem !important}.pe-lg-0{padding-right:0 !important}.pe-lg-1{padding-right:0.25rem !important}.pe-lg-2{padding-right:0.5rem !important}.pe-lg-3{padding-right:1rem !important}.pe-lg-4{padding-right:1.5rem !important}.pe-lg-5{padding-right:3rem !important}.pb-lg-0{padding-bottom:0 !important}.pb-lg-1{padding-bottom:0.25rem !important}.pb-lg-2{padding-bottom:0.5rem !important}.pb-lg-3{padding-bottom:1rem !important}.pb-lg-4{padding-bottom:1.5rem !important}.pb-lg-5{padding-bottom:3rem !important}.ps-lg-0{padding-left:0 !important}.ps-lg-1{padding-left:0.25rem !important}.ps-lg-2{padding-left:0.5rem !important}.ps-lg-3{padding-left:1rem !important}.ps-lg-4{padding-left:1.5rem !important}.ps-lg-5{padding-left:3rem !important}.text-lg-start{text-align:left !important}.text-lg-end{text-align:right !important}.text-lg-center{text-align:center !important}}@media(min-width: 1200px){.float-xl-start{float:left !important}.float-xl-end{float:right !important}.float-xl-none{float:none !important}.d-xl-inline{display:inline !important}.d-xl-inline-block{display:inline-block !important}.d-xl-block{display:block !important}.d-xl-grid{display:grid !important}.d-xl-table{display:table !important}.d-xl-table-row{display:table-row !important}.d-xl-table-cell{display:table-cell !important}.d-xl-flex{display:flex !important}.d-xl-inline-flex{display:inline-flex !important}.d-xl-none{display:none !important}.flex-xl-fill{flex:1 1 auto !important}.flex-xl-row{flex-direction:row !important}.flex-xl-column{flex-direction:column !important}.flex-xl-row-reverse{flex-direction:row-reverse !important}.flex-xl-column-reverse{flex-direction:column-reverse !important}.flex-xl-grow-0{flex-grow:0 !important}.flex-xl-grow-1{flex-grow:1 !important}.flex-xl-shrink-0{flex-shrink:0 !important}.flex-xl-shrink-1{flex-shrink:1 !important}.flex-xl-wrap{flex-wrap:wrap !important}.flex-xl-nowrap{flex-wrap:nowrap !important}.flex-xl-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-xl-0{gap:0 !important}.gap-xl-1{gap:0.25rem !important}.gap-xl-2{gap:0.5rem !important}.gap-xl-3{gap:1rem !important}.gap-xl-4{gap:1.5rem !important}.gap-xl-5{gap:3rem !important}.justify-content-xl-start{justify-content:flex-start !important}.justify-content-xl-end{justify-content:flex-end !important}.justify-content-xl-center{justify-content:center !important}.justify-content-xl-between{justify-content:space-between !important}.justify-content-xl-around{justify-content:space-around !important}.justify-content-xl-evenly{justify-content:space-evenly !important}.align-items-xl-start{align-items:flex-start !important}.align-items-xl-end{align-items:flex-end !important}.align-items-xl-center{align-items:center !important}.align-items-xl-baseline{align-items:baseline !important}.align-items-xl-stretch{align-items:stretch !important}.align-content-xl-start{align-content:flex-start !important}.align-content-xl-end{align-content:flex-end !important}.align-content-xl-center{align-content:center !important}.align-content-xl-between{align-content:space-between !important}.align-content-xl-around{align-content:space-around !important}.align-content-xl-stretch{align-content:stretch !important}.align-self-xl-auto{align-self:auto !important}.align-self-xl-start{align-self:flex-start !important}.align-self-xl-end{align-self:flex-end !important}.align-self-xl-center{align-self:center !important}.align-self-xl-baseline{align-self:baseline !important}.align-self-xl-stretch{align-self:stretch !important}.order-xl-first{order:-1 !important}.order-xl-0{order:0 !important}.order-xl-1{order:1 !important}.order-xl-2{order:2 !important}.order-xl-3{order:3 !important}.order-xl-4{order:4 !important}.order-xl-5{order:5 !important}.order-xl-last{order:6 !important}.m-xl-0{margin:0 !important}.m-xl-1{margin:0.25rem !important}.m-xl-2{margin:0.5rem !important}.m-xl-3{margin:1rem !important}.m-xl-4{margin:1.5rem !important}.m-xl-5{margin:3rem !important}.m-xl-auto{margin:auto !important}.mx-xl-0{margin-right:0 !important;margin-left:0 !important}.mx-xl-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-xl-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-xl-3{margin-right:1rem !important;margin-left:1rem !important}.mx-xl-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-xl-5{margin-right:3rem !important;margin-left:3rem !important}.mx-xl-auto{margin-right:auto !important;margin-left:auto !important}.my-xl-0{margin-top:0 !important;margin-bottom:0 !important}.my-xl-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-xl-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-xl-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-xl-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-xl-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-xl-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-xl-0{margin-top:0 !important}.mt-xl-1{margin-top:0.25rem !important}.mt-xl-2{margin-top:0.5rem !important}.mt-xl-3{margin-top:1rem !important}.mt-xl-4{margin-top:1.5rem !important}.mt-xl-5{margin-top:3rem !important}.mt-xl-auto{margin-top:auto !important}.me-xl-0{margin-right:0 !important}.me-xl-1{margin-right:0.25rem !important}.me-xl-2{margin-right:0.5rem !important}.me-xl-3{margin-right:1rem !important}.me-xl-4{margin-right:1.5rem !important}.me-xl-5{margin-right:3rem !important}.me-xl-auto{margin-right:auto !important}.mb-xl-0{margin-bottom:0 !important}.mb-xl-1{margin-bottom:0.25rem !important}.mb-xl-2{margin-bottom:0.5rem !important}.mb-xl-3{margin-bottom:1rem !important}.mb-xl-4{margin-bottom:1.5rem !important}.mb-xl-5{margin-bottom:3rem !important}.mb-xl-auto{margin-bottom:auto !important}.ms-xl-0{margin-left:0 !important}.ms-xl-1{margin-left:0.25rem !important}.ms-xl-2{margin-left:0.5rem !important}.ms-xl-3{margin-left:1rem !important}.ms-xl-4{margin-left:1.5rem !important}.ms-xl-5{margin-left:3rem !important}.ms-xl-auto{margin-left:auto !important}.p-xl-0{padding:0 !important}.p-xl-1{padding:0.25rem !important}.p-xl-2{padding:0.5rem !important}.p-xl-3{padding:1rem !important}.p-xl-4{padding:1.5rem !important}.p-xl-5{padding:3rem !important}.px-xl-0{padding-right:0 !important;padding-left:0 !important}.px-xl-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-xl-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-xl-3{padding-right:1rem !important;padding-left:1rem !important}.px-xl-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-xl-5{padding-right:3rem !important;padding-left:3rem !important}.py-xl-0{padding-top:0 !important;padding-bottom:0 !important}.py-xl-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-xl-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-xl-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-xl-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-xl-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-xl-0{padding-top:0 !important}.pt-xl-1{padding-top:0.25rem !important}.pt-xl-2{padding-top:0.5rem !important}.pt-xl-3{padding-top:1rem !important}.pt-xl-4{padding-top:1.5rem !important}.pt-xl-5{padding-top:3rem !important}.pe-xl-0{padding-right:0 !important}.pe-xl-1{padding-right:0.25rem !important}.pe-xl-2{padding-right:0.5rem !important}.pe-xl-3{padding-right:1rem !important}.pe-xl-4{padding-right:1.5rem !important}.pe-xl-5{padding-right:3rem !important}.pb-xl-0{padding-bottom:0 !important}.pb-xl-1{padding-bottom:0.25rem !important}.pb-xl-2{padding-bottom:0.5rem !important}.pb-xl-3{padding-bottom:1rem !important}.pb-xl-4{padding-bottom:1.5rem !important}.pb-xl-5{padding-bottom:3rem !important}.ps-xl-0{padding-left:0 !important}.ps-xl-1{padding-left:0.25rem !important}.ps-xl-2{padding-left:0.5rem !important}.ps-xl-3{padding-left:1rem !important}.ps-xl-4{padding-left:1.5rem !important}.ps-xl-5{padding-left:3rem !important}.text-xl-start{text-align:left !important}.text-xl-end{text-align:right !important}.text-xl-center{text-align:center !important}}@media(min-width: 1400px){.float-xxl-start{float:left !important}.float-xxl-end{float:right !important}.float-xxl-none{float:none !important}.d-xxl-inline{display:inline !important}.d-xxl-inline-block{display:inline-block !important}.d-xxl-block{display:block !important}.d-xxl-grid{display:grid !important}.d-xxl-table{display:table !important}.d-xxl-table-row{display:table-row !important}.d-xxl-table-cell{display:table-cell !important}.d-xxl-flex{display:flex !important}.d-xxl-inline-flex{display:inline-flex !important}.d-xxl-none{display:none !important}.flex-xxl-fill{flex:1 1 auto !important}.flex-xxl-row{flex-direction:row !important}.flex-xxl-column{flex-direction:column !important}.flex-xxl-row-reverse{flex-direction:row-reverse !important}.flex-xxl-column-reverse{flex-direction:column-reverse !important}.flex-xxl-grow-0{flex-grow:0 !important}.flex-xxl-grow-1{flex-grow:1 !important}.flex-xxl-shrink-0{flex-shrink:0 !important}.flex-xxl-shrink-1{flex-shrink:1 !important}.flex-xxl-wrap{flex-wrap:wrap !important}.flex-xxl-nowrap{flex-wrap:nowrap !important}.flex-xxl-wrap-reverse{flex-wrap:wrap-reverse !important}.gap-xxl-0{gap:0 !important}.gap-xxl-1{gap:0.25rem !important}.gap-xxl-2{gap:0.5rem !important}.gap-xxl-3{gap:1rem !important}.gap-xxl-4{gap:1.5rem !important}.gap-xxl-5{gap:3rem !important}.justify-content-xxl-start{justify-content:flex-start !important}.justify-content-xxl-end{justify-content:flex-end !important}.justify-content-xxl-center{justify-content:center !important}.justify-content-xxl-between{justify-content:space-between !important}.justify-content-xxl-around{justify-content:space-around !important}.justify-content-xxl-evenly{justify-content:space-evenly !important}.align-items-xxl-start{align-items:flex-start !important}.align-items-xxl-end{align-items:flex-end !important}.align-items-xxl-center{align-items:center !important}.align-items-xxl-baseline{align-items:baseline !important}.align-items-xxl-stretch{align-items:stretch !important}.align-content-xxl-start{align-content:flex-start !important}.align-content-xxl-end{align-content:flex-end !important}.align-content-xxl-center{align-content:center !important}.align-content-xxl-between{align-content:space-between !important}.align-content-xxl-around{align-content:space-around !important}.align-content-xxl-stretch{align-content:stretch !important}.align-self-xxl-auto{align-self:auto !important}.align-self-xxl-start{align-self:flex-start !important}.align-self-xxl-end{align-self:flex-end !important}.align-self-xxl-center{align-self:center !important}.align-self-xxl-baseline{align-self:baseline !important}.align-self-xxl-stretch{align-self:stretch !important}.order-xxl-first{order:-1 !important}.order-xxl-0{order:0 !important}.order-xxl-1{order:1 !important}.order-xxl-2{order:2 !important}.order-xxl-3{order:3 !important}.order-xxl-4{order:4 !important}.order-xxl-5{order:5 !important}.order-xxl-last{order:6 !important}.m-xxl-0{margin:0 !important}.m-xxl-1{margin:0.25rem !important}.m-xxl-2{margin:0.5rem !important}.m-xxl-3{margin:1rem !important}.m-xxl-4{margin:1.5rem !important}.m-xxl-5{margin:3rem !important}.m-xxl-auto{margin:auto !important}.mx-xxl-0{margin-right:0 !important;margin-left:0 !important}.mx-xxl-1{margin-right:0.25rem !important;margin-left:0.25rem !important}.mx-xxl-2{margin-right:0.5rem !important;margin-left:0.5rem !important}.mx-xxl-3{margin-right:1rem !important;margin-left:1rem !important}.mx-xxl-4{margin-right:1.5rem !important;margin-left:1.5rem !important}.mx-xxl-5{margin-right:3rem !important;margin-left:3rem !important}.mx-xxl-auto{margin-right:auto !important;margin-left:auto !important}.my-xxl-0{margin-top:0 !important;margin-bottom:0 !important}.my-xxl-1{margin-top:0.25rem !important;margin-bottom:0.25rem !important}.my-xxl-2{margin-top:0.5rem !important;margin-bottom:0.5rem !important}.my-xxl-3{margin-top:1rem !important;margin-bottom:1rem !important}.my-xxl-4{margin-top:1.5rem !important;margin-bottom:1.5rem !important}.my-xxl-5{margin-top:3rem !important;margin-bottom:3rem !important}.my-xxl-auto{margin-top:auto !important;margin-bottom:auto !important}.mt-xxl-0{margin-top:0 !important}.mt-xxl-1{margin-top:0.25rem !important}.mt-xxl-2{margin-top:0.5rem !important}.mt-xxl-3{margin-top:1rem !important}.mt-xxl-4{margin-top:1.5rem !important}.mt-xxl-5{margin-top:3rem !important}.mt-xxl-auto{margin-top:auto !important}.me-xxl-0{margin-right:0 !important}.me-xxl-1{margin-right:0.25rem !important}.me-xxl-2{margin-right:0.5rem !important}.me-xxl-3{margin-right:1rem !important}.me-xxl-4{margin-right:1.5rem !important}.me-xxl-5{margin-right:3rem !important}.me-xxl-auto{margin-right:auto !important}.mb-xxl-0{margin-bottom:0 !important}.mb-xxl-1{margin-bottom:0.25rem !important}.mb-xxl-2{margin-bottom:0.5rem !important}.mb-xxl-3{margin-bottom:1rem !important}.mb-xxl-4{margin-bottom:1.5rem !important}.mb-xxl-5{margin-bottom:3rem !important}.mb-xxl-auto{margin-bottom:auto !important}.ms-xxl-0{margin-left:0 !important}.ms-xxl-1{margin-left:0.25rem !important}.ms-xxl-2{margin-left:0.5rem !important}.ms-xxl-3{margin-left:1rem !important}.ms-xxl-4{margin-left:1.5rem !important}.ms-xxl-5{margin-left:3rem !important}.ms-xxl-auto{margin-left:auto !important}.p-xxl-0{padding:0 !important}.p-xxl-1{padding:0.25rem !important}.p-xxl-2{padding:0.5rem !important}.p-xxl-3{padding:1rem !important}.p-xxl-4{padding:1.5rem !important}.p-xxl-5{padding:3rem !important}.px-xxl-0{padding-right:0 !important;padding-left:0 !important}.px-xxl-1{padding-right:0.25rem !important;padding-left:0.25rem !important}.px-xxl-2{padding-right:0.5rem !important;padding-left:0.5rem !important}.px-xxl-3{padding-right:1rem !important;padding-left:1rem !important}.px-xxl-4{padding-right:1.5rem !important;padding-left:1.5rem !important}.px-xxl-5{padding-right:3rem !important;padding-left:3rem !important}.py-xxl-0{padding-top:0 !important;padding-bottom:0 !important}.py-xxl-1{padding-top:0.25rem !important;padding-bottom:0.25rem !important}.py-xxl-2{padding-top:0.5rem !important;padding-bottom:0.5rem !important}.py-xxl-3{padding-top:1rem !important;padding-bottom:1rem !important}.py-xxl-4{padding-top:1.5rem !important;padding-bottom:1.5rem !important}.py-xxl-5{padding-top:3rem !important;padding-bottom:3rem !important}.pt-xxl-0{padding-top:0 !important}.pt-xxl-1{padding-top:0.25rem !important}.pt-xxl-2{padding-top:0.5rem !important}.pt-xxl-3{padding-top:1rem !important}.pt-xxl-4{padding-top:1.5rem !important}.pt-xxl-5{padding-top:3rem !important}.pe-xxl-0{padding-right:0 !important}.pe-xxl-1{padding-right:0.25rem !important}.pe-xxl-2{padding-right:0.5rem !important}.pe-xxl-3{padding-right:1rem !important}.pe-xxl-4{padding-right:1.5rem !important}.pe-xxl-5{padding-right:3rem !important}.pb-xxl-0{padding-bottom:0 !important}.pb-xxl-1{padding-bottom:0.25rem !important}.pb-xxl-2{padding-bottom:0.5rem !important}.pb-xxl-3{padding-bottom:1rem !important}.pb-xxl-4{padding-bottom:1.5rem !important}.pb-xxl-5{padding-bottom:3rem !important}.ps-xxl-0{padding-left:0 !important}.ps-xxl-1{padding-left:0.25rem !important}.ps-xxl-2{padding-left:0.5rem !important}.ps-xxl-3{padding-left:1rem !important}.ps-xxl-4{padding-left:1.5rem !important}.ps-xxl-5{padding-left:3rem !important}.text-xxl-start{text-align:left !important}.text-xxl-end{text-align:right !important}.text-xxl-center{text-align:center !important}}@media(min-width: 1200px){.fs-1{font-size:2.5rem !important}.fs-2{font-size:2rem !important}.fs-3{font-size:1.75rem !important}.fs-4{font-size:1.5rem !important}}@media print{.d-print-inline{display:inline !important}.d-print-inline-block{display:inline-block !important}.d-print-block{display:block !important}.d-print-grid{display:grid !important}.d-print-table{display:table !important}.d-print-table-row{display:table-row !important}.d-print-table-cell{display:table-cell !important}.d-print-flex{display:flex !important}.d-print-inline-flex{display:inline-flex !important}.d-print-none{display:none !important}}#webcomponent{font-family:\"Roboto\", sans-serif}"),
				this.shadowRoot.appendChild(r),
				B(
					this,
					{ target: this.shadowRoot, props: h(this.attributes), customElement: !0 },
					jr,
					_r,
					i,
					{
						style: 43,
						id: 42,
						externalfilter: 0,
						rows: 1,
						size: 2,
						page: 3,
						pages: 4,
						headers: 5,
						actions: 6,
						selectactions: 7,
						selectrow: 8,
						enableselect: 11,
						disablepagination: 9,
						add_item: 10
					},
					null,
					[-1, -1, -1, -1]
				),
				t && (t.target && l(t.target, this, t.anchor), t.props && (this.$set(t.props), O()));
		}
		static get observedAttributes() {
			return [
				'style',
				'id',
				'externalfilter',
				'rows',
				'size',
				'page',
				'pages',
				'headers',
				'actions',
				'selectactions',
				'selectrow',
				'enableselect',
				'disablepagination',
				'add_item'
			];
		}
		get style() {
			return this.$$.ctx[43];
		}
		set style(t) {
			this.$$set({ style: t }), O();
		}
		get id() {
			return this.$$.ctx[42];
		}
		set id(t) {
			this.$$set({ id: t }), O();
		}
		get externalfilter() {
			return this.$$.ctx[0];
		}
		set externalfilter(t) {
			this.$$set({ externalfilter: t }), O();
		}
		get rows() {
			return this.$$.ctx[1];
		}
		set rows(t) {
			this.$$set({ rows: t }), O();
		}
		get size() {
			return this.$$.ctx[2];
		}
		set size(t) {
			this.$$set({ size: t }), O();
		}
		get page() {
			return this.$$.ctx[3];
		}
		set page(t) {
			this.$$set({ page: t }), O();
		}
		get pages() {
			return this.$$.ctx[4];
		}
		set pages(t) {
			this.$$set({ pages: t }), O();
		}
		get headers() {
			return this.$$.ctx[5];
		}
		set headers(t) {
			this.$$set({ headers: t }), O();
		}
		get actions() {
			return this.$$.ctx[6];
		}
		set actions(t) {
			this.$$set({ actions: t }), O();
		}
		get selectactions() {
			return this.$$.ctx[7];
		}
		set selectactions(t) {
			this.$$set({ selectactions: t }), O();
		}
		get selectrow() {
			return this.$$.ctx[8];
		}
		set selectrow(t) {
			this.$$set({ selectrow: t }), O();
		}
		get enableselect() {
			return this.$$.ctx[11];
		}
		set enableselect(t) {
			this.$$set({ enableselect: t }), O();
		}
		get disablepagination() {
			return this.$$.ctx[9];
		}
		set disablepagination(t) {
			this.$$set({ disablepagination: t }), O();
		}
		get add_item() {
			return this.$$.ctx[10];
		}
		set add_item(t) {
			this.$$set({ add_item: t }), O();
		}
	}
	return customElements.define('hb-table', zr), zr;
})();
//# sourceMappingURL=release.js.map
