import { readable, writable, type Readable, type Writable } from 'svelte/store';
import type { StreamingServer } from '../lib/streams';
import type { TStreamCustomList } from '$lib/interfaces';

export const records: Writable<
	Array<{
		name: string;
		server: string;
		segments: { start: string; duration?: string }[];
	}>
> = writable([]);

export const streams: Writable<Array<TStreamCustomList>> = writable([]);

export const servers: Writable<Array<StreamingServer>> = writable([]);

export const localStorageServersKey: Readable<string> = readable('local_servers');
