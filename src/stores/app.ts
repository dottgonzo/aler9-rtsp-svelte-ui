import { readable, writable, type Readable, type Writable } from 'svelte/store';

export const componentsVersion: Readable<string> = readable('0.20.0');
