import type { TAler9PathItem } from 'aler9-server-manager';

export type TStreamItem = TAler9PathItem & {
	uri: string;
	record: boolean;
	segments: { start: string }[];
};
export type TStreamCustomList = {
	serverName: string;
	streamUpdate: Date;
	items: TStreamItem[];
	pageCount: number;
};
