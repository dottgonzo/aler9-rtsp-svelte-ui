import { get, type Writable, type Readable } from 'svelte/store';

import StreamsConfigurator, {
	type TAler9Configuration,
	type TMediaMTXRecordingListItem
} from 'aler9-server-manager';
import { localStorageServersKey, records } from '../stores/server';
import type { TStreamCustomList } from './interfaces';

export function getAllStreams(
	servers: Writable<Array<StreamingServer>>,
	setter: Writable<Array<TStreamCustomList>>
) {
	const newStreams: Array<TStreamCustomList> = [];
	for (const s of get(servers)) {
		try {
			const streamsConfigurator = new StreamsConfigurator({
				uri: s.apiUri,
				auth: s.auth
			});
			streamsConfigurator
				.getPaths()
				.then((paths) => {
					const allServers = get(servers);
					allServers.find((f) => f.name === s.name).updatedAt = new Date();
					servers.set(allServers);
					console.info('update server paths', s.name, paths);
					streamsConfigurator
						.getPathsConfigs()
						.then((pathsConfigs) => {
							const customPathList = paths as unknown as TStreamCustomList;
							// const allRecords = get(records);
							customPathList.items.forEach((p) => {
								const pathConfig = pathsConfigs.items.find((f) => f.name === p.name);
								if (pathConfig) {
									if (pathConfig.runOnInit.includes('://')) {
										const sourceUriProtocol =
											pathConfig.runOnInit.split('://')[0].split(' ')[
												pathConfig.runOnInit.split('://')[0].split(' ').length - 1
											] + '://';

										const sourceUriDest = pathConfig.runOnInit.split('://')[1].split(' ')[0];

										p.uri = sourceUriProtocol + sourceUriDest;
									} else {
										p.uri = pathConfig.source;
									}
									p.record = pathConfig.record;
								}

								// const pathSegments = allRecords.find((f) => f.name === p.name)?.segments;

								// p.segments = pathSegments;
							});
							const newPath = {
								...customPathList,
								serverName: s.name,
								streamUpdate: new Date()
							};

							newStreams.push(newPath);

							const old = get(setter);
							if (old.find((f) => f.serverName === s.name)) {
								const oldStreamIndex = old.findIndex((f) => f.serverName === s.name);
								old[oldStreamIndex] = newPath;
							} else {
								old.push(newPath);
							}
							setter.set(old);
						})
						.catch((err) => {
							console.error(err);
						});
				})
				.catch((err) => {
					console.error(err);
				});
		} catch (err) {
			console.error('error on ' + s.name + ' streaming fetch', err);
		}
		// if (currentStreams.find((f) => f.serverName === s.name)) {
		// } else {
		// 	streamsToAdd.push({
		// 		...paths,
		// 		serverName: s.name,
		// 		streamUpdate: new Date()
		// 	});
		// }

		// streams.set(paths);
		// streamUpdate.set(new Date());
	}
}

export async function setAllRecordings(
	servers: Writable<Array<StreamingServer>>,
	streams: Readable<Array<TStreamCustomList>>,
	records: Writable<
		Array<
			TMediaMTXRecordingListItem & {
				server: string;
				segments: { start: string; duration?: string }[];
			}
		>
	>
) {
	const recordings: Array<
		TMediaMTXRecordingListItem & {
			server: string;
			segments: { start: string; duration?: string }[];
		}
	> = [];
	const allServers = get(servers);
	const allStreams = get(streams);
	for (const serverStreams of allStreams) {
		const server = allServers.find((f) => f.name === serverStreams.serverName);
		let streamsConfigurator = new StreamsConfigurator({
			uri: server.apiUri,
			auth: server.auth,
			playbackProxyUri: server.playBackProxyUri
		});
		if (!server.config) {
			try {
				server.config = await streamsConfigurator.getConfig();
				server.updatedAt = new Date();
				const playbackProtocol = server.apiUri.split(':')[0];
				const serverUri = playbackProtocol + ':' + server.apiUri.split(':')[1];
				if (server.config.playback && server.config.playbackAddress) {
					server.playBackUri = serverUri + ':' + server.config.playbackAddress.split(':')[1];
				}
				if (server.config.hls && server.config.hlsAddress) {
					server.hlsUri = serverUri + ':' + server.config.hlsAddress.split(':')[1];
				}
				if (server.config.webrtc && server.config.webrtcAddress) {
					server.webrtcUri = serverUri + ':' + server.config.webrtcAddress.split(':')[1];
				}
				servers.set(allServers);
			} catch (err) {
				console.error('unable to get config', err);
			}
		}
		for (const stream of serverStreams.items.filter((f) => f.record)) {
			try {
				const recs = await streamsConfigurator.getPathRecordings(stream.name);

				try {
					const recsFromList = await streamsConfigurator.getRecordingsPlaybackList4Path(
						stream.name
					);
					recs.segments = recsFromList;
					recordings.push({ ...recs, server: server.name });
				} catch (err) {
					recordings.push({ ...recs, server: server.name });
				}
			} catch (err) {
				console.error('error on ' + stream.name + ' recording fetch', err);
			}
		}
	}
	console.info('setAllRecordings', recordings);
	records.set(recordings);
}

export type StreamingServer = {
	source: 'config' | 'local';
	name: string;
	apiUri: string;

	webrtcUri?: string;
	webrtcProxyUri?: string;
	hlsUri?: string;
	hlsProxyUri?: string;
	playBackUri?: string;
	playBackProxyUri?: string;

	auth?: {
		username: string;
		password: string;
		type: 'basic';
	};

	updatedAt: Date;

	config?: TAler9Configuration;
};

let servers: StreamingServer[];

export default async function getStreamsConfigurator(): Promise<StreamingServer[]> {
	if (servers) return servers;
	let newServers: StreamingServer[] = [];
	const localServersString = localStorage.getItem(get(localStorageServersKey));
	if (localServersString) {
		newServers = JSON.parse(localServersString);
	}

	try {
		const serverJsonLocation =
			location.href.split('://')[0] +
			'://' +
			location.href.split('://')[1].split('/')[0] +
			'/servers.json';
		const cfg = await fetch(serverJsonLocation);

		if (cfg.ok) {
			const fetchedServers = await cfg.json();
			fetchedServers.forEach((server) => {
				if (!server.source) {
					server.source = 'config';
				}
			});

			for (const f of fetchedServers) {
				if (!newServers.find((n) => n.name === f.name)) {
					newServers.push(f);
				} else {
					const oldServerIndex = newServers.findIndex((n) => n.name === f.name);
					newServers[oldServerIndex] = f;
				}
			}

			console.log(newServers, 'loadcfgJSON');
		} else {
			console.error('no servers.json', cfg);
		}
	} catch (error) {
		console.error('error fetch servers.json', error);
	}
	const localDefaultUri = `http://127.0.0.1`;
	const localDefaultApiUri = `${localDefaultUri}:9997`;
	try {
		if (!newServers.find((n) => n.name === 'local')) {
			const localDefaultApi = new StreamsConfigurator({
				uri: localDefaultApiUri
			});

			const theconfig = await localDefaultApi.getConfig();
			console.info('localDefaultApi', theconfig);
			const newServer: StreamingServer = {
				source: 'config',
				name: 'local',
				apiUri: localDefaultApiUri,
				updatedAt: new Date()
			};
			if (theconfig.playback && theconfig.playbackAddress) {
				newServer.playBackUri = localDefaultUri + ':' + theconfig.playbackAddress.split(':')[1];
			}
			if (theconfig.hls && theconfig.hlsAddress) {
				newServer.hlsUri = localDefaultUri + ':' + theconfig.hlsAddress.split(':')[1];
			}
			if (theconfig.webrtc && theconfig.webrtcAddress) {
				newServer.webrtcUri = localDefaultUri + ':' + theconfig.webrtcAddress.split(':')[1];
			}
			newServers.push(newServer);
		}
	} catch (err) {
		console.info(`no ${localDefaultApiUri} found`);
	}
	return newServers;
}
