FROM node:20-alpine as builder
WORKDIR /app
COPY ./package.json ./
COPY ./package-lock.json ./
RUN npm install

COPY ./src ./src
COPY ./static ./static
COPY ./svelte.config.js ./
COPY ./tsconfig.json ./
COPY ./vite.config.js ./
RUN npm run build


FROM dottgonzo/docker-nginx-htmlsite:withouttry
COPY --from=builder /app/build /usr/share/nginx/html